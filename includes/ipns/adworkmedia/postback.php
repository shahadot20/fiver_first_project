<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include('../../../includes/config.php');

$IP = get_user_ip();	

if ($IP=="67.227.230.75" || $IP=="67.227.230.76" || $IP=="2607:fad0:3704:2::" || $IP=="2607:fad0:3704:2::1" || $IP=="2607:fad0:3704:2::2" || $IP=="2607:fad0:3704:2::3") {
 
// get variables and prevent against SQL injections
$campaign_id = mysqli_real_escape_string($conn, $_GET['campaign_id']);			// Campaign ID
$campaign_name = mysqli_real_escape_string($conn, $_GET['campaign_name']);		// Campaign Name		
$subid = mysqli_real_escape_string($conn, $_GET['sid']);	 					// Primary SubID
$subid2 = mysqli_real_escape_string($conn, $_GET['sid2']);  				    // Secondary SubID2
$subid3 = mysqli_real_escape_string($conn, $_GET['sid3']);  					// Secondary SubID3
$commission = mysqli_real_escape_string($conn, $_GET['commission']);			// Commission		
$status = mysqli_real_escape_string($conn, $_GET['status']);  					// Status: 1 (Credited) or 2 (Reversed)
$ip = mysqli_real_escape_string($conn, $_GET['ip']);  							// User IP Address
$reversal_reason = mysqli_real_escape_string($conn, $_GET['reversal_reason']);  // Reversal Reason (usually not sent)
$vc_value = mysqli_real_escape_string($conn, $_GET['vc_value']);  				// Virtual Currency Only - VC Ratio * Commission	
	
if ($status==1) {	
	
$query = mysqli_query($conn, "SELECT u.* FROM users u WHERE u.username='{$subid}' LIMIT 1") or die(mysqli_error($conn));
$u = mysqli_fetch_array($query, MYSQLI_ASSOC);

$query2 = mysqli_query($conn, "SELECT * FROM networks WHERE network_name='Adwork Media' LIMIT 1") or die(mysqli_error($conn));
$network = mysqli_fetch_array($query2);

// add chat message
$text = $campaign_name;
$type = "Offer";
$provider = $network['network_name'];
mysqli_query($conn, "INSERT INTO `activity_history`(history_id, user_id, history_type, history_provider, history_text, history_points, history_date) VALUES(NULL, '{$u['user_id']}', '{$type}', '{$provider}', '{$text}', '{$vc_value}', NOW())") or die(mysqli_error($conn));
mysqli_query($conn, "UPDATE `users` SET `points`=`points`+'".$vc_value."', `daily_offers`=`daily_offers`+'1' WHERE `user_id`='{$u['user_id']}'") or die(mysqli_error($conn));

if($vc_value >= $config['site_daily_offers_points_minimum']) {
mysqli_query($conn, "UPDATE `users` SET `daily_bonus_offers`=`daily_bonus_offers`+'1' WHERE `user_id`='{$u['user_id']}'") or die(mysqli_error($conn));
}


// add chat message
$username = $config['site_bot_username'];
$text = "<strong>{$u['username']}</strong> has just completed {$text} for {$vc_value} {$config['site_currency']} via <a href=\"".$config['site_url']."earn/adwork-media\" target=\"_blank\">Adwork Media</a>";
$date = date('h:i a', time());
$chat_ip = mysqli_real_escape_string($conn, get_user_ip());
mysqli_query($conn, "INSERT INTO chat (id, username, message, ip_address) VALUES (NULL, '$username', '$text', '$chat_ip')") or die(mysqli_error($conn));

// add affiliate earnings
$affiliate_sql= mysqli_query($conn, "SELECT referral,username FROM `users` WHERE `user_id`='{$u['user_id']}'") or die(mysqli_error($conn));
$affiliate = mysqli_fetch_array($affiliate_sql);
if($affiliate['referral'] > 0){
$bonus_points = sprintf($vc_value * (($config['site_referral_bonus']) / 100));	
mysqli_query($conn, "UPDATE `users` SET `points`=`points`+'".$bonus_points."' WHERE `user_id`='{$affiliate['referral']}' LIMIT 1") or die(mysqli_error($conn));
}

// add admin profits
$amount = sprintf($vc_value * (($network['network_conversion_ratio']) / 100));
$earnings2 = $vc_value-$amount;
$earnings = convert($earnings2);
add_admin_profit($u['user_id'], $provider, $campaign_id, $campaign_name, $earnings);


echo "OK.";

} elseif($status==2) { 
	
$query = mysqli_query($conn, "SELECT u.* FROM users u WHERE u.username='{$subid}'") or die(mysqli_error($conn));
$u = mysqli_fetch_array($query, MYSQLI_ASSOC);

$query2 = mysqli_query($conn, "SELECT * FROM networks WHERE network_name='Adwork Media'") or die(mysqli_error($conn));
$network = mysqli_fetch_array($query2);

// add activity history
$text = $campaign_name;
$type = "Reversal";
$provider = "Adwork Media";
mysqli_query($conn, "INSERT INTO `activity_history`(history_id, user_id, history_type, history_provider, history_text, history_points, history_date) VALUES(NULL, '{$u['user_id']}', '{$type}', '{$provider}', '{$text}', '{$vc_value}', NOW())") or die(mysqli_error($conn));
mysqli_query($conn, "UPDATE `users` SET `points`=`points`-'".$vc_value."', `daily_offers`=`daily_offers`-'1' WHERE `user_id`='{$u['user_id']}'") or die(mysqli_error($conn));

// remove affiliate earnings
$affiliate_sql= mysqli_query($conn, "SELECT referral,username FROM `users` WHERE `user_id`='{$u['user_id']}'") or die(mysqli_error($conn));
$affiliate = mysqli_fetch_array($affiliate_sql);
if($affiliate['referral'] > 0){
$bonus_points = sprintf($vc_value * (($config['site_referral_bonus']) / 100));	
mysqli_query($conn, "UPDATE `users` SET `points`=`points`-'".$bonus_points."' WHERE `user_id`='{$affiliate['referral']}' LIMIT 1") or die(mysqli_error($conn));
}

// check reversal ban
$get_offer_reversals = mysqli_query($conn, "SELECT * FROM `activity_history` WHERE `user_id`='{$u['user_id']}'") or die(mysqli_error($conn));

if(mysqli_num_rows($get_offer_reversals) >= $config['site_reversal_limit']) {
mysqli_query($conn, "UPDATE `users` SET `banned`='1', `banned_reason`='Offer Reversals' WHERE `user_id`='{$u['user_id']}'") or die(mysqli_error($conn));

$get_bans_sql = mysqli_query($conn, "SELECT * FROM banned_ips WHERE ban_ip_address='{$u['ip']}'") or die(mysqli_error($conn));

if(mysqli_num_rows($get_bans_sql) == 0) {
mysqli_query($conn, "INSERT INTO `banned_ips`(ban_id, ban_ip_address, ban_reason, ban_banner, ban_date_added) VALUES(NULL, '{$u['ip']}', 'Offer Reversals', 'System', NOW())") or die(mysqli_error($conn));
}
}

// remove admin profits
$amount = sprintf($vc_value * (($network['network_conversion_ratio']) / 100));
$earnings2 = $vc_value-$amount;
$earnings = convert($earnings2);
remove_admin_profit($u['user_id'], $provider, $campaign_id, $campaign_name, $earnings);

}
}

?>