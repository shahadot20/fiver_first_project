<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include('../../../includes/config.php');

// If Receive Post Request
if ($_POST) {

    // Get User IP
    $request_ip = get_user_ip();

    // Define Whitelisted IPs
    $ip_whitelist = array("108.170.27.234", "108.170.27.238", "198.15.95.74", "198.15.113.58");

    // Process Incoming Variables and Sanitize
    $notify_id = mysqli_real_escape_string($conn, intval($_POST["notify_id"]));
    $app_id = mysqli_real_escape_string($conn, intval($_POST["app_id"]));
    $user_id = mysqli_real_escape_string($conn, $_POST["user_id"]);
    $create_time = mysqli_real_escape_string($conn, intval($_POST["create_time"]));
    $real_amount = mysqli_real_escape_string($conn, floatval($_POST["real_amount"]));
    $credit_amount = mysqli_real_escape_string($conn, floatval($_POST["credit_amount"]));
    $retry = mysqli_real_escape_string($conn, intval($_POST["retry"]);)
    $verify_code = mysqli_real_escape_string($conn, preg_replace("/[^a-zA-Z0-9]/", "", $_POST["verify_code"]));
    $more_info = mysqli_real_escape_string($conn, preg_replace("/[^a-zA-Z0-9]/", "", $_POST["more_info"]));
    $campaign_name = "Minute Staff Offer #{$notify_id}";
    $oid = $notify_id;
    // Check to see if postback post is coming from MinuteStaff Servers
    if (in_array($request_ip, $ip_whitelist)) {

        // Define Notify Code - Grab from Panel
        $notify_code = "";

        // Generate the Verification Code by MD5 Hash
        $generated_verify_code = md5($notify_code . $app_id . $user_id . $notify_id);

        // Insert Entry into Data (for Logging, Duplicate Checking)

        // Verify Notification Postback (Optional)
        if ($verify_code == $generated_verify_code) {

            // Check Database for Duplicate based on notify_id - Make Sure to Use a Variable Fetch Prior to Insert Entry

            // Credit User Based on credit_amount
            if (($credit_amount > 0) && ($real_amount > 0)) {

	   $query = mysqli_query($conn, "SELECT u.* FROM users u WHERE u.username='{$user_id}' LIMIT 1");
	   $u= mysqli_fetch_array($query);
	   
	   $query2 = mysqli_query($conn, "SELECT * FROM networks WHERE network_name='MinuteStaff' LIMIT ") or die(mysqli_error($conn));
$network = mysqli_fetch_array($query2);
	
	$text = $campaign_name;
	$type = "Offer";
	$provider = $network['network_name'];
	mysqli_query($conn, "INSERT INTO `activity_history`(history_id, user_id, history_type, history_provider, history_text, history_points, history_date) VALUES(NULL, '{$u['user_id']}', '{$type}', '{$provider}', '{$text}', '{$credit_amount}', NOW())");
	mysqli_query($conn, "UPDATE `users` SET `points`=`points`+'".$credit_amount."', `daily_offers`=`daily_offers`+'1' WHERE `user_id`='{$u['user_id']}'");
	
	if($credit_amount >= $config['site_daily_offers_points_minimum']) {
mysqli_query($conn, "UPDATE `users` SET `daily_bonus_offers`=`daily_bonus_offers`+'1' WHERE `user_id`='{$u['user_id']}'");
}

$username = $config['site_bot_username'];
$text = "<strong>{$u['username']}</strong> has just completed {$campaign_name} for {$credit_amount} {$config['site_currency']} via <a href=\"".$config['site_url']."earn/minutestaff\" target=\"_blank\">Minutestaff</a>";
$date = date('h:i a', time());
$chat_ip = mysqli_real_escape_string($conn, get_user_ip());
mysqli_query($conn, "INSERT INTO chat (id, username, message, ip_address) VALUES (NULL, '$username', '$text', '$chat_ip')");

$affiliate_sql= mysqli_query($conn, "SELECT referral,username FROM `users` WHERE `user_id`='{$u['user_id']}'");
$affiliate = mysqli_fetch_array($affiliate_sql);
if($affiliate['referral'] > 0){
$bonus_points = sprintf($credit_amount * (($config['site_referral_bonus']) / 100));	
mysqli_query($conn, "UPDATE `users` SET `points`=`points`+'".$bonus_points."' WHERE `user_id`='{$affiliate['referral']}' LIMIT 1");
}

// add admin profits
$amount = sprintf($credit_amount * (($network['network_conversion_ratio']) / 100));
$earnings2 = $credit_amount-$amount;
$earnings = convert($earnings2);
add_admin_profit($u['user_id'], $provider, $oid, $campaign_name, $earnings);


            // Deduct Earning From User
            } else if (($credit_amount < 0) && ($real_amount < 0)) {


	   $query = mysqli_query($conn, "SELECT u.* FROM users u WHERE u.username='{$user_id}'");
	   $u= mysqli_fetch_array($query);
	   $query2 = mysqli_query($conn, "SELECT * FROM networks WHERE network_name='MinuteStaff'") or die(mysqli_error($conn));
$network = mysqli_fetch_array($query2);
	
	$text = $campaign_name;
	$type = "Reversal";
	$provider = "Minute Staff";
	mysqli_query($conn, "INSERT INTO `activity_history`(history_id, user_id, history_type, history_provider, history_text, history_points, history_date) VALUES(NULL, '{$u['user_id']}', '{$type}', '{$provider}', '{$text}', '{$credit_amount}', NOW())");
	mysqli_query($conn, "UPDATE `users` SET `points`=`points`-'".$credit_amount."' WHERE `user_id`='{$u['user_id']}'");

$affiliate_sql= mysqli_query($conn, "SELECT referral,username FROM `users` WHERE `user_id`='{$u['user_id']}'");
$affiliate = mysqli_fetch_array($affiliate_sql);
if($affiliate['referral'] > 0){
$bonus_points = sprintf($credit_amount * (($config['site_referral_bonus']) / 100));	
mysqli_query($conn, "UPDATE `users` SET `points`=`points`-'".$bonus_points."' WHERE `user_id`='{$affiliate['referral']}' LIMIT 1");
}

// check reversal ban
$get_offer_reversals = mysqli_query($conn, "SELECT * FROM `activity_history` WHERE `user_id`='{$u['user_id']}'") or die(mysqli_error($conn));

if(mysqli_num_rows($get_offer_reversals) >= $config['site_reversal_limit']) {
mysqli_query($conn, "UPDATE `users` SET `banned`='1', `banned_reason`='Offer Reversals' WHERE `user_id`='{$u['user_id']}'") or die(mysqli_error($conn));

$get_bans_sql = mysqli_query($conn, "SELECT * FROM banned_ips WHERE ban_ip_address='{$u['ip']}'") or die(mysqli_error($conn));

if(mysqli_num_rows($get_bans_sql) == 0) {
mysqli_query($conn, "INSERT INTO `banned_ips`(ban_id, ban_ip_address, ban_reason, ban_banner, ban_date_added) VALUES(NULL, '{$u['ip']}', 'Offer Reversals', 'System', NOW())") or die(mysqli_error($conn));
}
}

// remove admin profits
$amount = sprintf($credit_amount * (($network['network_conversion_ratio']) / 100));
$earnings2 = $credit_amount-$amount;
$earnings = convert($earnings2);
remove_admin_profit($u['user_id'], $provider, $oid, $campaign_name, $earnings);



            // System Notification (Warning), Send User a Message with "extra_detail"
            } else if (($credit_amount == 0.00000) && ($real_amount == 0.00000)) {

            }

        // Error Out, Bad Verification Code
        } else {

            // Notify Admin Here of Possible Hijack - Bad Verification Code

        }

    // Error Out, Bad User IP Address
    } else {

        // Notify Admin Here of Possible Hijack (Non-Whitelisted IP)

    }

}