<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include('../../../includes/config.php');

$publisher_hash = "";
$publisher_secret_key = "";

$id =         mysqli_real_escape_string($conn, $_GET['placement_id']);
$subid =            mysqli_real_escape_string($conn, $_GET['user_id']);
$campaign_name =    mysqli_real_escape_string($conn, $_GET['offer_name']);
$vc_value = mysqli_real_escape_string($conn, $_GET['amount']);


$signature = $_REQUEST['pub_signature'];
$sig = md5($subid.':'.$publisher_hash.':'.$publisher_secret_key);

if (!empty($signature) && isset($signature) && ($signature != $sig)) { 
die();
  exit;
} else {

	
	   $query = mysqli_query($conn, "SELECT u.* FROM users u WHERE u.username='".$subid."' LIMIT 1") or die(mysqli_error($conn));
	   $u = mysqli_fetch_array($query);
	   
	$query2 = mysqli_query($conn, "SELECT * FROM networks WHERE network_name='Personaly'") or die(mysqli_error($conn));
$network = mysqli_fetch_array($query2);

	$text = $campaign_name;
	$type = "Offer";
	$provider = $network['network_name'];
	mysqli_query($conn, "INSERT INTO `activity_history`(history_id, user_id, history_type, history_provider, history_text, history_points, history_date) VALUES(NULL, '{$u['user_id']}', '{$type}', '{$provider}', '{$text}', '{$vc_value}', NOW())") or die(mysqli_error($conn));
	mysqli_query($conn, "UPDATE `users` SET `points`=`points`+'".$vc_value."', `daily_offers`=`daily_offers`+'1' WHERE `user_id`='{$u['user_id']}'") or die(mysqli_error($conn));

if($vc_value >= $config['site_daily_offers_points_minimum']) {
mysqli_query($conn, "UPDATE `users` SET `daily_bonus_offers`=`daily_bonus_offers`+'1' WHERE `user_id`='".$u['user_id']."'") or die(mysqli_error($conn));
}

$username = $config['site_bot_username'];
$chat_text = "<strong>{$u['username']}</strong> has just completed {$campaign_name} for {$vc_value} points via <a href=\"{$config['site_url']}earn/personaly\" target=\"_blank\">Personaly</a>";
$date = date('h:i a', time());
$chat_ip = mysqli_real_escape_string($conn, get_user_ip());
mysqli_query($conn, "INSERT INTO chat (id, username, message, ip_address) VALUES (NULL, '$username', '$chat_text', '$chat_ip')") or die(mysqli_error($conn));

$affiliate_sql= mysqli_query($conn, "SELECT referral,username FROM `users` WHERE `user_id`='{$u['user_id']}'") or die(mysqli_error($conn));
$affiliate = mysqli_fetch_array($affiliate_sql);
if($affiliate['referral'] > 0){
$bonus_points = sprintf($vc_value * (($config['site_referral_bonus']) / 100));	
mysqli_query($conn, "UPDATE `users` SET `points`=`points`+'".$bonus_points."' WHERE `user_id`='{$affiliate['referral']}' LIMIT 1") or die(mysqli_error($conn));
}

// add admin profits
$amount = sprintf($vc_value * (($network['network_conversion_ratio']) / 100));
$earnings2 = $vc_value-$amount;
$earnings = convert($earnings2);
add_admin_profit($u['user_id'], $provider, $id, $campaign_name, $earnings);


}
?>