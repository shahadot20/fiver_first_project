<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include('../../../includes/config.php');

$app_key = ""; 

$transaction_id = mysqli_real_escape_string($conn, urldecode($_REQUEST['transaction_id'])); 
$offer_id = mysqli_real_escape_string($conn, intval($_REQUEST['offer_id']));
$campaign_name = mysqli_real_escape_string($conn, urldecode($_REQUEST['offer_name']));
$subid = mysqli_real_escape_string($conn, urldecode($_REQUEST['subid1']));
$vc_value = mysqli_real_escape_string($conn, urldecode($_REQUEST['amount']));

// Get Postback variables, for a complete list of list of variables, please go to https://www.offerdaddy.com/docs Postback tab

//Check the signature
$my_signature = md5($transaction_id.'/'.$offer_id.'/'.$app_key);
$signature = urldecode($_REQUEST['signature']);


if(isset($signature) && $my_signature != $signature) {
die();
exit;
} else {

$query = mysqli_query($conn, "SELECT u.* FROM users u WHERE u.username='".$subid."' LIMIT 1");
$u = mysqli_fetch_array($query);

$query2 = mysqli_query($conn, "SELECT * FROM networks WHERE network_name='Offer Daddy'") or die(mysqli_error($conn));
$network = mysqli_fetch_array($query2);
	
$text = $campaign_name;
$type = "Offer";
$provider = $network['network_name'];
mysqli_query($conn, "INSERT INTO `activity_history`(history_id, user_id, history_type, history_provider, history_text, history_points, history_date) VALUES(NULL, '{$u['user_id']}', '{$type}', '{$provider}', '{$text}', '{$vc_value}', NOW())");
mysqli_query($conn, "UPDATE `users` SET `points`=`points`+'".$vc_value."', `daily_offers`=`daily_offers`+'1' WHERE `user_id`='{$u['user_id']}'");

if($vc_value >= $config['site_daily_offers_points_minimum']) {
mysqli_query($conn, "UPDATE `users` SET `daily_bonus_offers`=`daily_bonus_offers`+'1' WHERE `user_id`='{$u['user_id']}'");
}

$username = $config['site_bot_username'];
$chat_text = "<strong>{$u['username']}</strong> has just completed {$campaign_name} for {$vc_value} points via <a href=\"".$config['site_url']."earn/offerdaddy\" target=\"_blank\">Offer Daddy</a>";
$date = date('h:i a', time());
$ip = mysqli_real_escape_string($conn, get_user_ip());
mysqli_query($conn, "INSERT INTO chat (id, username, message, ip_address) VALUES (NULL, '$username', '$chat_text', '$ip')");

$affiliate_sql= mysqli_query($conn, "SELECT referral,username FROM `users` WHERE `user_id`='{$u['user_id']}'");
$affiliate = mysqli_fetch_array($affiliate_sql);
if($affiliate['referral'] > 0) {
$bonus_points = sprintf($vc_value * (($config['site_referral_bonus']) / 100));	
mysqli_query($conn, "UPDATE `users` SET `points`=`points`+'".$bonus_points."' WHERE `user_id`='{$affiliate['referral']}' LIMIT 1");
}

// add admin profits
$amount = sprintf($vc_value * (($network['network_conversion_ratio']) / 100));
$earnings2 = $vc_value-$amount;
$earnings = convert($earnings2);
add_admin_profit($u['user_id'], $provider, $offer_id, $campaign_name, $earnings);

echo "1";

} 

?>