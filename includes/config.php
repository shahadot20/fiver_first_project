<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '0');

$servername = "localhost";
$dbusername = "root";
$dbpassword = "";
$dbname = "dbdata";

// Create connection
$conn = new mysqli($servername, $dbusername, $dbpassword, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

mysqli_query($conn, "SET NAMES utf8");

include_once("functions.php");

if(isset($_SESSION['username'])){
    
$_SESSION['username'] = filter_var(trim($_SESSION['username']),FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
$user = mysqli_fetch_array(mysqli_query($conn, "SELECT *,UNIX_TIMESTAMP(`online`) AS `online` FROM `users` WHERE `username`='".mysqli_real_escape_string($conn, $_SESSION['username'])."' LIMIT 1"));

$user_ip = get_user_ip();
$get_ips = mysqli_query($conn, "SELECT * FROM users_ip_logs WHERE user_id='".mysqli_real_escape_string($conn, $user['user_id'])."' AND ip_address='".mysqli_real_escape_string($conn, $user_ip)."' LIMIT 1") or die(mysqli_error($conn));
if(mysqli_num_rows($get_ips) == 0) {
    mysqli_query($conn, "INSERT INTO `users_ip_logs`(id, user_id,ip_address,date_added) values(NULL, '".mysqli_real_escape_string($conn, $user['user_id'])."','".mysqli_real_escape_string($conn, $user_ip)."',NOW())") or die(mysqli_error($conn));
}
}

  
$config = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM `config`"));

date_default_timezone_set($config['site_timezone']);

?>