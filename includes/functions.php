<?php

/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

function html_advertisement($id) {
global $conn;
$get_advertisements_sql = mysqli_query($conn, "SELECT * FROM advertisements WHERE ad_id='".mysqli_real_escape_string($conn, intval($id))."' AND ad_active='1' LIMIT 1") or die(mysqli_error($conn));
$advertisement = mysqli_fetch_array($get_advertisements_sql);
echo html_entity_decode($advertisement['ad_html_code']);
}

function points_formatter($num, $precision = 4){
    return floor($num).substr($num-floor($num),1,$precision+1);
}

function str_clean($str) {
	$str = trim(htmlentities(strip_tags($str)));
	
	if (get_magic_quotes_gpc())
		$str = stripslashes($str);
	
	return $str;
}

function truncate($str, $length, $trailing='...') {
$length-=mb_strlen($trailing);
if (mb_strlen($str)> $length) {
return mb_substr($str,0,$length).$trailing;
}
else
{
$s = $str;
}
return $s;
} 

function clean($string) {
return preg_replace('/[^A-Za-z0-9\-]/', ' ', $string); // Removes special chars.
}

function add_offer_history($user_id, $type, $text, $points, $provider) {
global $conn; 

$user_id = mysqli_real_escape_string($conn, intval($user_id));
$type = mysqli_real_escape_string($conn, $type);
$points = mysqli_real_escape_string($conn, $points);
$provider = mysqli_real_escape_string($conn, $provider);
mysqli_query($conn, "INSERT INTO `activity_history` (history_id, user_id, history_type, history_provider, history_text, history
_points, history_date) VALUES(NULL, '{$user_id}', '{$type}', '{$provider}', '{$text}', '{$points}', NOW() )") or die(mysqli_error($conn));
}

function add_admin_profit($user_id, $network, $offer_id, $offer_name, $amount) {
global $conn; 

$user_id = mysqli_real_escape_string($conn, intval($user_id));
$network = mysqli_real_escape_string($conn, $network);
$offer_id = mysqli_real_escape_string($conn, intval($offer_id));
$offer_name = mysqli_real_escape_string($conn, $offer_name);
$amount = mysqli_real_escape_string($conn, $amount);

mysqli_query($conn, "INSERT INTO `admin_earnings` (id,user_id,network,offer_id,offer_name,amount,date_added) VALUES(NULL, '{$user_id}', '{$network}', '{$offer_id}', '{$offer_name}', '{$amount}', NOW() )") or die(mysqli_error($conn));
}

function remove_admin_profit($user_id, $network, $offer_id, $offer_name, $amount) {
global $conn; 

$user_id = mysqli_real_escape_string($conn, intval($user_id));
$network = mysqli_real_escape_string($conn, $network);
$offer_id = mysqli_real_escape_string($conn, intval($offer_id));
$offer_name = mysqli_real_escape_string($conn, $offer_name);
$amount = mysqli_real_escape_string($conn, $amount);

mysqli_query($conn, "DELETE FROM admin_earnings WHERE user_id='{$user_id}' AND network='{$network}' AND offer_id='{$offer_id}' AND amount='{$amount}' LIMIT 1") or die(mysqli_error($conn));
}

function get_user_ip() {
    $ipaddress = '';
    if ($_SERVER['HTTP_CLIENT_IP'])
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if($_SERVER['HTTP_X_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if($_SERVER['HTTP_X_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if($_SERVER['HTTP_FORWARDED_FOR'])
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if($_SERVER['HTTP_FORWARDED'])
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if($_SERVER['REMOTE_ADDR'])
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function convert($p) {
    global $config;
$p2 = $p/$config['site_point_conversion'];
return $p2;
}

function userid_to_username($userid) {
global $conn;

$userid = mysqli_real_escape_string($conn, intval($userid));
$userw = mysqli_fetch_array(mysqli_query($conn, "SELECT username FROM `users` WHERE `user_id`='{$userid}' LIMIT 1"));

if($userw['username'] == "") {
echo "N/A";
} else {
return $userw['username'];
}
}

function coupon_code_generator($length = 10) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function add_transaction($user_id, $package, $money, $fee, $paypal_email) {
global $conn;
$user_id = mysqli_real_escape_string($conn, intval($user_id));
$money = mysqli_real_escape_string($conn, $money);
$fee = mysqli_real_escape_string($conn, intval($fee));
$package = mysqli_real_escape_string($conn, $package);
$paypal_email = mysqli_real_escape_string($conn, $paypal_email);

$add_transaction = "INSERT INTO `users_transactions` (id, user_id, package, money, fee, paypal_email, date_added) VALUES(NULL, '{$user_id}', '{$package}', '{$money}', '{$fee}', '{$paypal_email}', NOW())";
$result = mysqli_query($conn, $add_transaction) or die(mysqli_error($conn));
}

function daily_bonus_percentage($x,$y) {
 
$percentage = ($x/$y) * 100;
return abs($percentage);
}
?>