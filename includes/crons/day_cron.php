<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include('../config.php');

$secret = '2ti20tgj239gf2j9';
if($_SERVER["argv"][1] != $secret)
{
    die("Invalid Access Denied");
}

mysqli_query($conn, "UPDATE users SET daily_offers=0 WHERE daily_offers > 0") or die(mysqli_error($conn));
mysqli_query($conn, "UPDATE users SET daily_bonus_offers=0 WHERE daily_bonus_offers > 0") or die(mysqli_error($conn));
mysqli_query($conn, "UPDATE users SET daily_bonus=0 WHERE daily_bonus > 0") or die(mysqli_error($conn));

$get_ptc_ads = mysqli_query($conn, "select * from ptc_ads")  or die(mysqli_error($conn));

if(mysqli_num_rows($get_ptc_ads) > 0) {
mysqli_query($conn, "UPDATE ptc_ads SET ptc_ad_today_views=0 WHERE ptc_ad_today_views > 0") or die(mysqli_error($conn));
}

$get_ptc_ads_history = mysqli_query($conn, "select * from ptc_ads_history")  or die(mysqli_error($conn));

if(mysqli_num_rows($get_ptc_ads_history) > 0) {
mysqli_query($conn, "TRUNCATE ptc_ads_history") or die(mysqli_error($conn));
}

if($config['site_daily_active'] == 1) {
$username = $config['site_bot_username'];
$text = "<div class=\"alert alert-success\">Daily offers feature has been reset! Complete {$config['site_daily_offers']} offers worth at least {$config['site_daily_offers_points_minimum']} points to be rewarded {$config['site_daily_offers_points']} points!</div>";
$date = date('h:i a', time());
$chat_ip = mysqli_real_escape_string($conn, get_user_ip());
mysqli_query($conn, "INSERT INTO chat (id, username, message, ip_address) VALUES (NULL, '$username', '$text', '$chat_ip')") or die(mysqli_error($conn));
}

?>