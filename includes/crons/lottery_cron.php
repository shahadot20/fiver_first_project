<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include('../config.php');

$secret = '2ti20tgj239gf2j9';
if($_SERVER["argv"][1] != $secret)
{
    die("Invalid Access Denied");
}


$total_lottery_players = mysqli_query($conn, "SELECT * FROM `lottery`") or die(mysqli_error($conn));

if(mysqli_num_rows($total_lottery_players)>0) {
// calculate the lottery jackpot
$total_lottery_points = mysqli_fetch_array(mysqli_query($conn, "SELECT SUM(`points`) AS `points` FROM `lottery`"));

// choose random winner
$lottery_result = mysqli_query($conn, "SELECT * FROM `lottery` ORDER BY RAND()") or die(mysqli_error($conn));
$row = mysqli_fetch_array($lottery_result);

// get lottery winner
$get_lottery_users = mysqli_query($conn, "SELECT * FROM `users` WHERE `user_id`='{$row['user_id']}' LIMIT 1") or die(mysqli_error($conn));
$lottery_user = mysqli_fetch_array($get_lottery_users);

// update lottery winner's points
mysqli_query($conn, "UPDATE `users` SET `points` = `points` + '".$total_lottery_points['points']."' WHERE `user_id`='".$lottery_user['user_id']."' LIMIT 1") or die(mysqli_error($conn));

// insert into `lottery_winners` table
mysqli_query($conn, "INSERT INTO `lottery_winners`(id, user_id, points, date) VALUES(NULL, '{$lottery_user['user_id']}', '{$total_lottery_points['points']}',NOW())") or die(mysqli_error($conn));

$username = 'GPT-BOT';
$text = "<div class=\"alert alert-success\"><strong>".userid_to_username($lottery_user['user_id'])."</strong> won the <a class=\"alert-link\" href=\"{$config['site_url']}lottery\" target=\"_blank\">lottery</a> and earned {$total_lottery_points['points']} points!</div>";
$date = date('h:i a', time());
$chat_ip = get_user_ip();
mysqli_query($conn, "INSERT INTO chat (id, username, message, ip_address) VALUES (NULL, '$username', '$text', '$chat_ip')") or die(mysqli_error($conn));
		
		
		
// insert into `activey_history` table
$text = "Congrats! You won ".$total_lottery_points['points']." points from the lottery!!!";
$type = "Winnings";
$provider = "Lottery";
mysqli_query($conn, "INSERT INTO `activity_history`(history_id, user_id, history_type, history_provider, history_text, history_points, history_date) VALUES(NULL, '{$lottery_user['user_id']}', '{$type}', '{$provider}', '{$text}', '{$total_lottery_points['points']}', NOW())") or die(mysqli_error($conn));


// delete all rows in table `lottery`
$result2 = mysqli_query($conn, "DELETE FROM `lottery`") or die(mysqli_error($conn));
}
?>