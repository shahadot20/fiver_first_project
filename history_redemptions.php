<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/
session_start();


include_once("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['username'])){
    echo "<script>document.location.href='".$config['base_url']."login'</script>";
    exit;
}

$adjacents = 5;

    $query = mysqli_query($conn, "select COUNT(*) as num from `redemption_history` WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."'") or die(mysqli_error($conn));
    $total_pages = mysqli_fetch_array($query, MYSQLI_ASSOC);
    $total_pages = $total_pages['num'];

    $limit = $config['site_page_limit'];                                //how many items to show per page

    $page = abs((int) $_GET['page']);

    if($page) 
        $start = ($page - 1) * $limit;          //first item to display on this page
    else
        $start = 0;                             //if no page var is given, set start to 
    /* Get data. */
    $result = mysqli_query($conn, "select * from `redemption_history` WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."' ORDER BY redemption_date_added DESC LIMIT $start,$limit") or die(mysqli_error($conn));

    /* Setup page vars for display. */
    if ($page == 0) $page = 1;                  //if no page var is given, default to 1.
    $prev = $page - 1;                          //previous page is page - 1
    $next = $page + 1;                          //next page is page + 1
    $lastpage = ceil($total_pages/$limit);      //lastpage is = total pages / items per page, rounded up.
    $lpm1 = $lastpage - 1;                      //last page minus 1

    $pagination = "";
    if($lastpage > 1) {   
        $pagination .= "<ul class=\"pager\">";
        //previous button
        if ($page > 1) 
            $pagination.= "<li class=\"previous\"><a href=\"".$config['base_url']."history/redemptions/$prev\">&laquo; previous</a></li>";
        else
            $pagination.= "<li class=\"previous disabled\"><a>&laquo; previous</a></li>"; 

        //next button
        if ($page < $lastpage) 
            $pagination.= "<li class=\"next\"><a href=\"".$config['base_url']."history/redemptions/$next\">next &raquo;</a></li>";
        else
            $pagination.= "<li class=\"next disabled\"><a>next &raquo;</a></li>";
        $pagination.= "</ul>\n";       
    }

?>

<div class="col-lg-9">

<h2 class="page-header">Redemption History</h2>
<p>All your redemptions will be displayed here.</p>

<?php if(mysqli_num_rows($result) == 0) { ?>
<div class="alert alert-danger">You haven't made a redemption yet. Once you successfully make a redemption they will be displayed here.</div>
<?php } else { ?>
<table class="table table-responsive">
<tr>
<th>Provider</th>
<th><?php echo $config['site_currency']; ?></th>
<th>Fee</th>
<th>Total</th>
<th>Date Added</th>
<th>Date Paid</th>
<th>Action</th>
</tr>
<?php while($row = mysqli_fetch_assoc($result)) {

if($row['redemption_fee'] > 0) {
$total_points2 = $row['redemption_points'] * "0.".$row['redemption_fee'];
$total_points = $row['redemption_points'] - $total_points2;
//$total_fee = $row['redemption_points'] * "0.".$row['redemption_fee'];
$total_fee = ($total_points / 100) * $row['redemption_fee'];
} else {
$total_points = $row['redemption_points'];
$total_fee = 0;
}

$tpte = ceil($total_points + $row['redemption_fee']) - $row['redemption_fee'];

if($row['redemption_paid'] == 0) { 
$action = "<span class=\"label label-default\">Under Review</span>";
} else { 
$action = "<span class=\"label label-success\">Paid!</span>";
}

if(empty($row['redemption_date_paid'])) {
$redemption_date_paid = "N/A";
} else {
$redemption_date_paid = $row['redemption_date_paid'];
}
?>
<tr>
<td><?php echo $row['redemption_provider']; ?></td>
<td><?php echo $row['redemption_points']; ?></td>
<td><span class="text text-danger"><?php echo $row['redemption_fee']."%"; ?></span></td>
<td><span class="text text-success strong"><?php echo "$".number_format(convert($tpte),2); ?></span></td>
<td><?php echo $row['redemption_date_added']; ?></td>
<td><?php echo $redemption_date_paid; ?></td>
<td><?php echo $action; ?></td>

</tr>
<?php } ?>
</table>
<?php } ?>

<?php echo $pagination; ?>

</div>

<?php include_once("footer.php"); ?>