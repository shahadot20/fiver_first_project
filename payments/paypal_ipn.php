<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include('../includes/config.php');

// CONFIG: Enable debug mode. This means we'll log requests into 'ipn.log' in the same directory.
// Especially useful if you encounter network errors or other intermittent problems with IPN (validation).
// Set this to 0 once you go live or don't require logging.
define("DEBUG", 0);

// Set to 0 once you're ready to go live
define("USE_SANDBOX", 0);


define("LOG_FILE", "./ipn.log");


// Read POST data
// reading posted data directly from $_POST causes serialization
// issues with array data in POST. Reading raw POST data from input stream instead.
$raw_post_data = file_get_contents('php://input');
$raw_post_array = explode('&', $raw_post_data);
$myPost = array();
foreach ($raw_post_array as $keyval) {
	$keyval = explode ('=', $keyval);
	if (count($keyval) == 2)
		$myPost[$keyval[0]] = urldecode($keyval[1]);
}
// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';
if(function_exists('get_magic_quotes_gpc')) {
	$get_magic_quotes_exists = true;
}
foreach ($myPost as $key => $value) {
	if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
		$value = urlencode(stripslashes($value));
	} else {
		$value = urlencode($value);
	}
	$req .= "&$key=$value";
}

// Post IPN data back to PayPal to validate the IPN data is genuine
// Without this step anyone can fake IPN data

if(USE_SANDBOX == true) {
	$paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
} else {
	$paypal_url = "https://www.paypal.com/cgi-bin/webscr";
}

$ch = curl_init($paypal_url);
if ($ch == FALSE) {
	return FALSE;
}

curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);

if(DEBUG == true) {
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
}

// CONFIG: Optional proxy configuration
//curl_setopt($ch, CURLOPT_PROXY, $proxy);
//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);

// Set TCP timeout to 30 seconds
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

// CONFIG: Please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path
// of the certificate as shown below. Ensure the file is readable by the webserver.
// This is mandatory for some environments.

//$cert = __DIR__ . "./cacert.pem";
//curl_setopt($ch, CURLOPT_CAINFO, $cert);

$res = curl_exec($ch);
if (curl_errno($ch) != 0) // cURL error
	{

	curl_close($ch);
	exit;

} else {
		// Log the entire HTTP response if debug is switched on.
		curl_close($ch);
}

// Inspect IPN validation result and act accordingly

// Split response headers and payload, a better way for strcmp
$tokens = explode("\r\n\r\n", trim($res));
$res = trim(end($tokens));



if (strcmp ($res, "VERIFIED") == 0) {

$item_name = mysqli_real_escape_string($conn, $_POST['item_name']);
$item_number = mysqli_real_escape_string($conn, $_POST['item_number']);
$payment_status = mysqli_real_escape_string($conn, $_POST['payment_status']);
$payment_amount = mysqli_real_escape_string($conn, $_POST['mc_gross']);
$payment_fee = mysqli_real_escape_string($conn, $_POST['mc_fee']);
$payment_currency = mysqli_real_escape_string($conn, $_POST['mc_currency']);
$txn_id = mysqli_real_escape_string($conn, $_POST['txn_id']);
$receiver_email = mysqli_real_escape_string($conn, $_POST['receiver_email']);
$payer_email = mysqli_real_escape_string($conn, $_POST['payer_email']);
$custom = mysqli_real_escape_string($conn, $_POST['custom']);
    
$pack_sql = "SELECT * FROM `ptc_adpacks` WHERE `ptc_adpack_name`='".$item_name."' AND `ptc_adpack_id`='".$item_number."' LIMIT 1";
$result = mysqli_query($conn, $pack_sql) or die(mysqli_error($conn));
$pack = mysqli_fetch_array($result, MYSQLI_ASSOC);

$buyer_sql = "SELECT * FROM `users` WHERE `user_id`='".$custom."' LIMIT 1";
$result2 = mysqli_query($conn, $buyer_sql) or die(mysqli_error($conn));
$buyer = mysqli_fetch_array($result2, MYSQLI_ASSOC);


if($item_name == $pack['ptc_adpack_name'] && $item_number == $pack['ptc_adpack_id'] && $payment_amount == $pack['ptc_adpack_price'] && $receiver_email == $config['site_paypal_email']) {
$add_points_sql = "UPDATE `users` SET `advertiser_points`=`advertiser_points`+'".$pack['ptc_adpack_points']."' WHERE `user_id`='".$custom."' LIMIT 1";
mysqli_query($conn, $add_points_sql) or die(mysqli_error($conn));

// add transaction
$package_name = $item_name;
$package_money = $payment_amount;
$buyer_email = $payer_email;
$package_fee = $payment_fee;
add_transaction($custom, $package_name, $package_money, $package_fee, $buyer_email);

}

} else if (strcmp ($res, "INVALID") == 0) {

}

?>

?>