<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include_once("header.php");

if(isset($_POST['send_message'])) {

$name = mysqli_real_escape_string($conn, str_clean($_POST['name']));
$email = mysqli_real_escape_string($conn, str_clean($_POST['email']));
$subject = str_replace(array("\n"),array("<br />"),strip_tags(str_clean($_POST['subject'])));
$msg = str_replace(array("\n"),array("<br />"),strip_tags($_POST['msg']));

if(!filter_var($email, FILTER_VALIDATE_EMAIL)) { 
$message = "<div class=\"alert alert-danger\">Invalid email format. Valid: <i>admin@example.com</i></div>";
} else if(empty($_POST['email']) || $_POST['email'] == "") {
$message = "<div class=\"alert alert-danger\">Email is empty, please try again.</div>";
} else if(empty($_POST['subject']) || $_POST['subject'] == "") {
$message = "<div class=\"alert alert-danger\">Subject is empty, please try again.</div>";
} else if(empty($_POST['msg']) || $_POST['msg'] == "") {
$message = "<div class=\"alert alert-danger\">Message is empty, please try again.</div>";
} else if(empty($_POST['name']) || $_POST['name'] == "") {
$message = "<div class=\"alert alert-danger\">Name is empty, please try again.</div>";
} else {
    
    $msg = str_replace("\n\n", "<br />", $msg);
$msg = str_replace("\n", "", $msg);

	mail($config['site_email'],"{$config['site_name']} - {$subject}","New message from: {$email} (via ".$config['site_url']."contact)

Message:

".strip_tags(htmlspecialchars_decode($msg))."

","From: {$name} <{$email}>");

$message = "<div class=\"alert alert-success\">Your message was successfully  sent to our team! Please allow up to 48 hours (usually less) for a response. Don't forget to check your spam/bulk folders periodically for our mails. If our emails are in your spam folder please click on <strong>NOT SPAM</strong>.</div>";

} 
}
?>

<?php if(isset($user['username'])) { ?>
<div class="col-lg-9">
<?php } else { ?>
<div class="container-fluid">

<div class="row row-centered">
<div class="col-lg-5 col-centered">
<?php } ?>

<form class="form-signin" role="form" method="post" action="">
        <h2 class="form-signin-heading text-center">Send a message</h2>
<div class="alert alert-info">Contact us using the form below.</div>

<?php if(isset($message)) echo $message; ?>

<div class="form-group">
        <label for="inputEmail" class="sr-only">Name</label>
        <input type="text" name="name" id="name" class="form-control" placeholder="Full name" required autofocus>
        </div>
        
<div class="form-group">
        <label for="inputEmail" class="sr-only">Email</label>
        <input type="email" name="email" id="email" class="form-control" placeholder="Email Address" required autofocus>
        </div>
        
        <div class="form-group">
        <label for="inputPassword" class="sr-only">Subject</label>
        <input type="text" name="subject" id="subject" class="form-control" placeholder="Enter subject of message.." required>
        </div>
        
        <div class="form-group">
        <label for="inputPassword" class="sr-only">Message</label>
        <textarea name="msg" id="msg" class="form-control" placeholder="Type a message.." rows="3" required></textarea>
        </div>
<div class="form-group">
        <button class="btn btn-lg btn-primary btn-block" name="send_message" type="submit">Send Message</button>
        </div>
      </form>

</div>
</div>

</div>

<?php include_once("footer.php"); ?>