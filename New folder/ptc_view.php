<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include('header_ptc.php');

if(!isset($_SESSION['username'])) { echo "<script>document.location.href='".$config['base_url']."'</script>"; exit; }

$id = intval($_GET['id']);

$sql = "SELECT * FROM `ptc_ads` WHERE `ptc_ad_id`='".mysqli_real_escape_string($conn, $id)."' AND `ptc_ad_active`='1' LIMIT 1";
$result = mysqli_query($conn,$sql) or die(mysqli_error($conn));
$x = mysqli_fetch_array($result);

?>

<script>

function alert() { // plays a sound when timer reaches 0
	alarmDiv = document.createElement("div");
	alarmDiv.innerHTML = "<embed src=\"<?php echo $config['base_url']; ?>assets/swf/sound.swf\" autostart=true hidden=true>";
	pbDiv = document.getElementById("contentContainer");
	document.body.insertBefore(alarmDiv, pbDiv);
}

var calcHeight = function() {
        $('iframe').height($(window).height() - 0);
    }
    
    $(window).resize(function() {
        calcHeight();
    }).load(function() {
        calcHeight();
    });


var counterName='countDown';

function start(){ displayCountdown(<?php echo $x['ptc_ad_timer']; ?>);}

$(document).ready(function() {
loaded(start);
});
var pageLoaded=0;

window.onload=function(){pageLoaded=1;}

function loaded(functionName){
	if(document.getElementById && document.getElementById(counterName) != null){
		functionName();
	}else{
		if(!pageLoaded){
			setTimeout('loaded('+functionName+')',100);
		}
	}
}
function displayCountdown(counter){
	if(counter <= 0){
		$("#countOutcome").html('<div class="alert alert-warning">Verifying click...</div>');
            var siteid = '<?php echo $x['ptc_ad_id']; ?>';
            var userid = "<?php echo $user['user_id']; ?>";
            var timer = "<?php echo $x['ptc_ad_timer']; ?>";
            var points = "<?php echo $x['ptc_ad_points']; ?>";

		$.ajax({
			type: "POST",
			url: "<?php echo $config['base_url']; ?>ajax/confirm_ptc_view.php",
			data: "site_id=" + siteid + "&user_id=" + userid + "&timer=" + <?php echo $x['ptc_ad_timer']; ?> + "&points=" + <?php echo $x['ptc_ad_points']; ?>, 
			success: function(msg){
				$("#countOutcome").html(msg);
alert();
			}
		});
	}else{
		document.getElementById(counterName).innerHTML=counter;
		setTimeout('displayCountdown('+(counter-1)+');',1000);
	}
}
</script>

<div class="surf_header">
<?php
if(!mysqli_num_rows($result)){
echo "<script>document.location.href='".$config['base_url']."ptc.php'</script>"; exit;
} else {
?>
<div class="text-center" id="countOutcome">
<div class="alert alert-info alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <i class="fa fa-spinner fa-pulse fa-fw"></i> Please wait <span id="countDown">0</span> seconds
</div></div>
<?php } ?>
</div>

<iframe id="iframe" src="<?php echo $x['ptc_ad_url']; ?>" SCROLLING="auto" frameborder="0" style="margin-top:0px;width:100%; height:100%; overflow-x:hidden;" vspace="0" hspace="0"></iframe>

