<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/
session_start();


include_once("header.php");

// redirect user to login to access this page
if(!isset($user['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}

?>

<div class="col-lg-9">

<h2 class="page-header">Daily Bonus</h2>
<p>Each day we reward users who complete offers, this is our way of saying thanks for being active!</p>

<?php
switch($_GET['action'])
{
case 'claim':
x_claim();
break;

default:
x_index();
break;
}
function x_index()
{
global $conn, $user, $config;
?>

<?php if($config['site_daily_active'] == 0) { ?>
<div class="alert alert-danger">The daily bonus feature is currently disabled.</div>
<?php } else { ?>
<?php if($user['daily_bonus'] == 0) { ?>
<?php if($user['daily_bonus_offers'] >= $config['site_daily_offers']) { ?>
<div class="alert alert-success">You completed <?php echo $user['daily_bonus_offers']; ?> offers today - well done! Click the button below to claim your bonus <?php echo $config['site_currency']; ?>!</div>
<a class="btn btn-md btn-success" href="<?php echo $config['base_url']; ?>daily_bonus.php?action=claim">Click to Claim Daily Bonus!</a>
<?php } else { ?>
<div class="alert alert-danger">You need to complete <?php echo $config['site_daily_offers'];?> offers worth at least <?php echo $config['site_daily_offers_points_minimum']; ?> <?php echo $config['site_currency']; ?> to claim <?php echo $config['site_daily_offers_points']; ?> bonus <?php echo $config['site_currency']; ?>!</div>
<?php } ?>
<?php } else { ?>
<div class="alert alert-danger">You already claimed the daily bonus today. Please try for the daily bonus tomorrow!</div>

<?php } ?>
<?php } ?>
<?php } ?>

<?php
function x_claim () {
global $conn, $user, $config;

if($config['site_daily_active'] == 0) { 
print "<div class=\"alert alert-danger\">The daily bonus feature is currently disabled.s</div>";
}

else if($user['daily_bonus'] == 0 && $user['daily_bonus_offers'] >= $config['site_daily_offers']) {

$username = $config['site_bot_username'];
$text = "<strong>{$user['username']}</strong> earned {$config['site_daily_offers_points']} {$config['site_currency']} for claiming the <a target=\"_blank\" href=\"".$config['base_url']."daily_bonus.php\">daily bonus</a>!";
$date = date('h:i a', time());
$chat_ip = get_user_ip();
mysqli_query($conn, "INSERT INTO chat (username, message, ip_address)
		VALUES ('".mysqli_real_escape_string($conn, $username)."', '$text', '$chat_ip')");
		
		mysqli_query($conn, "UPDATE `users` SET `daily_bonus`='1', `points`=`points`+'".mysqli_real_escape_string($conn, $config['site_daily_offers_points'])."' WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."' LIMIT 1") or die(mysqli_error($conn));

$insert_sql = "INSERT INTO `dailybonus_history` (dailybonus_id, dailybonus_user_id, dailybonus_points, dailybonus_offers, dailybonus_date_added) VALUES(NULL, '".mysqli_real_escape_string($conn, $user['user_id'])."', '".mysqli_real_escape_string($conn, $config['site_daily_offers_points'])."', '".mysqli_real_escape_string($conn, $config['site_daily_offers'])."', NOW())";
$result = mysqli_query($conn, $insert_sql) or die(mysqli_error($conn));

print "<div class=\"alert alert-success\">You earned ".$config['site_daily_offers_points']." bonus {$config['site_currency']}! Come back again tomorrow to claim the daily bonus again!</div>";
} else {
print "<div class=\"alert alert-danger\">You either already claimed the daily bonus for today, or you don't meet the requirement.</div>";
}
}
?>

</div>

<?php include_once("footer.php"); ?>