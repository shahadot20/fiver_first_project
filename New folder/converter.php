<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}

if(isset($_POST['convert'])) {

$convert_points = intval(str_clean($_POST['convert_points']));


if($convert_points == "" || empty($convert_points)) {
$error .= "<li>You need to enter the amount of ".$config['site_currency']." you want to convert to advertiser ".$config['site_currency'].".</li>";
}

if(!is_numeric($convert_points)) {
$error .= "<li>".$config['site_currency']." are only numeric values (0-9).</li>";
}

if($user['points'] < $convert_points) {
$error .= "<li>You don't have enough ".$config['site_currency']." to make this conversion.</li>";
}

if(!$error) {

$convert_points = intval(str_clean($_POST['convert_points']));

$total_points = $convert_points;

mysqli_query($conn, "INSERT INTO `conversion_history`(conversion_id, user_id,conversion_points,conversion_advertiser_points,conversion_date_added) values(NULL,'".mysqli_real_escape_string($conn, $user['user_id'])."','".mysqli_real_escape_string($conn, $total_points)."','".mysqli_real_escape_string($conn, $total_points)."',NOW())") or die(mysqli_error($conn));
 
mysqli_query($conn, "UPDATE `users` SET `points`=`points`-'".mysqli_real_escape_string($conn, $total_points)."',`advertiser_points`=`advertiser_points`+'".mysqli_real_escape_string($conn, $total_points)."' WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."' LIMIT 1");

    $success = "You successfully converted <strong>".$convert_points."</strong> ".$config['site_currency']." into ".$total_points." advertiser ".$config['site_currency'].".";
}
}
?>

<div class="col-lg-9">

<h2 class="page-header"><?php echo $config['site_currency']; ?> Converter</h2>
<p>This feature allows you to convert your <?php echo $config['site_currency']; ?> into advertiser <?php echo $config['site_currency']; ?>. Advertiser <?php echo $config['site_currency']; ?> are used for PTC ads and other on-site advertising payments.</p>

<?php if(isset($error)) { echo "<div class=\"alert alert-danger\"><h3>Errors:</h3><ul>".$error."</ul></div>"; } ?>
<?php if(isset($success)) { echo "<div class=\"alert alert-success\">".$success."</div>"; } ?>

<form class="form-horizontal" role="form" method="post">

<div class="form-group">
<label class="control-label col-sm-2" for="<?php echo $config['site_currency']; ?>"><?php echo $config['site_currency']; ?></label>
<div class="col-sm-10">
<input class="form-control" name="convert_points" id="convert_points" type="text" value="<?php if(isset($convert_points)) { echo $convert_points; } ?>" required/>
<span class="help-block">Input the number of <?php echo $config['site_currency']; ?> you want to convert into advertiser points.</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="convert" type="submit" value="Use Converter"/>
</div>
</div>
</form>

</div>

<?php include("footer.php"); ?>