<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}


// update payment email
if(isset($_POST['update_payment_email'])) {

$payment_email = str_clean($_POST['payment_email']);

if(empty($payment_email) || $payment_email== "") {
$error2 = "Please enter your payment email.<br />"; // throw error if payment email is blank, or empty
}

if(!filter_var($payment_email, FILTER_VALIDATE_EMAIL)) { 
$error2 .= "<div class=\"alert alert-danger\">Invalid email format. Valid: <i>admin@example.com</i></div>";  //throw error if payment email is not valid
}

if(!$error2) {
$payment_email = str_clean($_POST['payment_email']);
mysqli_query($conn, "UPDATE `users` SET 
`payment_email`='".mysqli_real_escape_string($conn, $payment_email)."' WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."'") or die(mysqli_error($conn)); // update user table with payment email
$success2 = "Payment Email updated!";
}
}


// change the user password
if(isset($_POST['update_password'])) {

$password = md5($_POST['password']);
$new_password = md5($_POST['new_password']);

if(empty($password) || $password == "") {
$error = "Please enter your password.<br />"; // throw error if password is blank, or empty
}

if(empty($new_password) || $new_password == "") {
$error .= "Please enter your new password.<br />"; // throw error if new password is blank, or empty
}

if($new_password == $user['password']) { 
$error .= "No use in using the same password..<br />"; // throw error if new password is same as old password
}

if($user['password'] != $password) {
$error .= "Wrong password.. try again.<br />"; // throw error if users inputs incorrect current password
}

if(!$error) {
$update_password = md5($new_password);
mysqli_query($conn, "UPDATE `users` SET 
`password`='".mysqli_real_escape_string($conn, $new_password)."' WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."'") or die(mysqli_error($conn)); // update user table with password
$success = "Account Password updated!";
}
}

// change the site theme
if(isset($_POST['change_theme'])) {

$theme = str_clean($_POST['theme']);

if($theme != "default" && $theme != "cerulean" && $theme != "cosmo" && $theme != "cyborg" && $theme != "darkly" && $theme != "flatly" && $theme != "journal" && $theme != "lumen" && $theme != "paper" && $theme != "readable" && $theme != "sandstone" && $theme != "simplex" && $theme != "slate" && $theme != "spacelab" && $theme != "superhero" && $theme != "united" && $theme != "yeti") {
$error3 = "This theme doesn't exist, please choose a theme from the options below.";

}

if(!$error3) {
$theme = str_clean($_POST['theme']);
mysqli_query($conn, "UPDATE `users` SET 
`site_theme`='".mysqli_real_escape_string($conn, $theme)."' WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."'") or die(mysqli_error($conn)); // update user table with theme
$success3 = "Site theme updated! The new theme will appear on next click or refresh.";
}
}


// change the chat status
if(isset($_POST['update_chat_status'])) {

$chat_status = intval(str_clean($_POST['chat_status']));

if($chat_status != "0" && $chat_status != "1") {
$error4 = "Invalid chat status. Please select a chat status below.";

}

if(!$error4) {
$chat_status = str_clean($_POST['chat_status']);
mysqli_query($conn, "UPDATE `users` SET 
`chat_status`='".mysqli_real_escape_string($conn, $chat_status)."' WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."'") or die(mysqli_error($conn)); // update user table with chat status
$success4 = "Chat status updated!";
}
}

?>

<div class="col-lg-9">

<!-- Email form -->

<form class="form-horizontal" role="form" method="post">

<h2>Update your Payment Email</h2>

<?php if(isset($error2)) { ?><div class="alert alert-danger"><?php echo $error2; ?></div><?php } ?>
<?php if(isset($success2)) { ?><div class="alert alert-success"><?php echo $success2; ?></div><?php } ?>

<div class="form-group">
<label class="control-label col-sm-2" for="payment_email">Email</label>
<div class="col-sm-10">
<div class="input-group">
  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope"></i></span>
  <input type="text" class="form-control" name="payment_email" id="payment_email" value="<?php echo $user['payment_email']; ?>">
</div>
<span class="help-block">Your Payment email.</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="update_payment_email" type="submit" value="Update Payment Email"/>
</div>
</div>

</form>
<!-- / Email form -->


<form class="form-horizontal" role="form" method="post">

<h2>Update your Password</h2>

<div class="alert alert-warning">Strict passwords have 8 characters and more!</div>

<?php if(isset($error)) { ?><div class="alert alert-danger"><?php echo $error; ?></div><?php } ?>
<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<div class="form-group">
<label class="control-label col-sm-2" for="password">Password</label>
<div class="col-sm-10">
<div class="input-group">
  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-key"></i></span>
<input class="form-control" name="password" id="password" type="text" value=""/>
</div>
<span class="help-block">Your account's current password.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="new_password">New Password</label>
<div class="col-sm-10">
<div class="input-group">
  <span class="input-group-addon" id="basic-addon1"><i class="fa fa-key"></i></span>
<input class="form-control" name="new_password" id="new_password" type="text" value="" />
</div>
<span class="help-block">Your account's new password.</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="update_password" type="submit" value="Update Password"/>
</div>
</div>

</form>

<form class="form-horizontal" role="form" method="post">

<h2>Change Site Theme</h2>

<?php if(isset($error3)) { ?><div class="alert alert-danger"><?php echo $error3; ?></div><?php } ?>
<?php if(isset($success3)) { ?><div class="alert alert-success"><?php echo $success3; ?></div><?php } ?>

<div class="form-group">
<label class="control-label col-sm-2" for="theme">Theme Switcher</label>
<div class="col-sm-10">
<div class="input-group">
<select name="theme" class="form-control">
<option value="default">Default</option>
<option value="cerulean">Cerulean</option>
<option value="cosmo">Cosmo</option>
<option value="cyborg">Cyborg</option>
<option value="darkly">Darkly</option>
<option value="flatly">Flatly</option>
<option value="journal">Journal</option>
<option value="lumen">Lumen</option>
<option value="paper">Paper</option>
<option value="readable">Readable</option>
<option value="sandstone">Sandstone</option>
<option value="simplex">Simplex</option>
<option value="slate">Slate</option>
<option value="spacelab">Spacelab</option>
<option value="superhero">Superhero</option>
<option value="united">United</option>
<option value="yeti">Yeti</option>
</select>
</div>
<span class="help-block">Choose a theme to use on your account.</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="change_theme" type="submit" value="Change Site Theme"/>
</div>
</div>

</form>

<form class="form-horizontal" role="form" method="post">

<h2>Change Chat Status</h2>

<div class="alert alert-info">The chat displays chat, daily bonus alerts, and offer completion alerts. If you don't want the chat box on your screen you can disable it here.</div>

<?php if(isset($error4)) { ?><div class="alert alert-danger"><?php echo $error4; ?></div><?php } ?>
<?php if(isset($success4)) { ?><div class="alert alert-success"><?php echo $success4; ?></div><?php } ?>

<div class="form-group">
<label class="control-label col-sm-2" for="chat_status">Chat Status</label>
<div class="col-sm-10">
<div class="input-group">
<select name="chat_status" class="form-control">
<option value="1"<?php if($user['chat_status'] == 1) { ?> selected<?php } ?>>Enabled</option>
<option value="0"<?php if($user['chat_status'] == 0) { ?> selected<?php } ?>>Disabled</option>
</select>
</div>
<span class="help-block">Do you want the chat box enabled or disabled?</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="update_chat_status" type="submit" value="Update Chat Status"/>
</div>
</div>

</form>

</div>

<?php include("footer.php"); ?>