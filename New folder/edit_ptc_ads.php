<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/
session_start();


include_once("header.php");

// redirect user to login to access this page
if(!isset($user['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}

$ptc_id = intval(str_clean(filter_var($_GET['ptc_id'], FILTER_VALIDATE_INT)));

$get_ptcad = mysqli_query($conn, "SELECT * FROM `ptc_ads` WHERE `ptc_ad_id`='".$ptc_id."' AND `ptc_ad_userid`='".mysqli_real_escape_string($conn, $user['user_id'])."' LIMIT 1");

if(mysqli_num_rows($get_ptcad)) {

$row = mysqli_fetch_array($get_ptcad);

if(isset($_POST['edit_ptc_ad'])) {

$title = str_clean($_POST['ptc_ad_title']);
$description = str_clean($_POST['ptc_ad_description']);
$points = str_clean($_POST['ptc_ad_points']);
$url = str_clean($_POST['ptc_ad_url']);
$timer = str_clean($_POST['ptc_ad_timer']);

if(empty($title) || $title == "") {
$message = "<div class=\"alert alert-danger\">You never entered a title for your PTC Ad. Please try again.</div>";
} else if(strlen($title) > 60) {
$message = "<div class=\"alert alert-danger\">The PTC ad title character maximum is 60 characters. Please try again.</div>";
} else if(empty($description) || $description == "") {
$message = "<div class=\"alert alert-danger\">You never entered a description for your PTC Ad. Please try again.</div>";
} else if(strlen($description) > 140) {
$message = "<div class=\"alert alert-danger\">The PTC ad description character maximum is 140 characters. Please try again.</div>";
} else if(empty($url) || $url == "") {
$message = "<div class=\"alert alert-danger\">You never entered a URL for your PTC Ad. Please try again.</div>";
} elseif(!preg_match('/^(http|https):\/\/[a-z0-9_]+([\-\.]{1}[a-z_0-9]+)*\.[_a-z]{2,5}'.'((:[0-9]{1,5})?\/.*)?$/i', $url)){
$message = "<div class=\"alert alert-danger\">You need to add a valid website URL!</div>";
} elseif(substr($url, -4) == '.exe') {
$message = "<div class=\"alert alert-danger\">This URL is not allowed, don't add .exe files!</div>";
} else if($points > 1) {
$message = "<div class=\"alert alert-danger\">The maximum CPC is 1.</div>";
} else if($points < 0.001) {
$message = "<div class=\"alert alert-danger\">The minimum CPC is 0.001</div>";
} else if($points > $user['advertiser_points']) {
$message = "<div class=\"alert alert-danger\">You don't have this many advertiser {$config['site_currency']}.</div>";
} else if(!is_numeric($points)) { 
$message = "<div class=\"alert alert-danger\">{$config['site_currency']} must be a valid number!</div>";
} else if(!is_numeric($timer)) {
$message = "<div class=\"alert alert-danger\">Timer must be a valid number!</div>";
} else {


$insert_sql = "UPDATE ptc_ads SET ptc_ad_title='".$title."', ptc_ad_description='".$description."', ptc_ad_points='".$points."', ptc_ad_url='".$url."', ptc_ad_timer='".$timer."', ptc_ad_active='".$status."' WHERE ptc_ad_id='".$ptc_id."' AND ptc_ad_userid='".$user['user_id']."' LIMIT 1";
$result = mysqli_query($conn, $insert_sql) or die(mysqli_error($conn));

$message = "<div class=\"alert alert-success\">The PTC Ad <strong>{$title}</strong> has been successfully edited.</div>";
}
}

?>

<div class="col-lg-9">

<h2 class="page-header">Editing PTC Ad: <?php echo $row['ptc_ad_title']; ?></h2>

<form class="form-horizontal" role="form" method="post">
    
    <?php if(isset($message)) echo $message; ?>

<div class="form-group">
<label class="control-label col-sm-2" for="ptc_ad_title">PTC Ad Title</label>
<div class="col-sm-10">
<input class="form-control" type="text" name="ptc_ad_title" id="ptc_ad_title" placeholder="<?php echo $row['ptc_ad_title']; ?>" required/>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ptc_ad_description">PTC Ad Description</label>
<div class="col-sm-10">
<textarea class="form-control" rows="3" name="ptc_ad_description" id="ptc_ad_description" placeholder="<?php echo $row['ptc_ad_description']; ?>" required/ ></textarea>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ptc_ad_points">PTC Ad CPC</label>
<div class="col-sm-10">
<input class="form-control" type="text" name="ptc_ad_points" id="ptc_ad_points" placeholder="<?php echo $row['ptc_ad_points']; ?>"  required/>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ptc_ad_url">PTC Ad URL</label>
<div class="col-sm-10">
<input class="form-control" type="text" name="ptc_ad_url" id="ptc_ad_url" placeholder="<?php echo $row['ptc_ad_url']; ?>" required/>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ptc_ad_timer">PTC Ad Timer</label>
<div class="col-sm-10">
<input class="form-control" type="number" name="ptc_ad_timer" id="ptc_ad_timer" max="10" min="1" value="<?php echo $row['ptc_ad_timer']; ?>" required/>
<span class="help-block">Timer is in seconds. 1 = 1 second, 10 = 10 seconds</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="edit_ptc_ad"></label>
<div class="col-sm-10">
<button type="submit" name="edit_ptc_ad" class="btn btn-success">Edit PTC Ad</button>
</div>
</div>
</form>

<?php } else { ?>
<div class="alert alert-danger">This PTC ad doesn't exist. Please try again.</div>
<?php } ?>

</div>

<?php include_once("footer.php"); ?>