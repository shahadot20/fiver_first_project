<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include_once("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}

if(isset($_POST['create'])) {

$code = coupon_code_generator();
$points = intval(str_clean($_POST['points']));
$uses = intval(str_clean($_POST['uses']));

$points_total = $points * $uses;


$coupon_sql = "SELECT * FROM `coupons` WHERE `code`='".mysqli_real_escape_string($conn, $code)."'";
$result = mysqli_query($conn,$coupon_sql) or die(mysqli_error($conn));
$coupon_exists = mysqli_num_rows($result);

if($coupon_exists > 0) {
$message = "<div class=\"alert alert-danger\">There is already a coupon with this code. Please try another code.</div>";
} else if(empty($code) || $code == "") {
$message = "<div class=\"alert alert-danger\">You never entered a coupon code. Please try again.</div>";
} else if($points > $user['points']) {
$message = "<div class=\"alert alert-danger\">You don't have this many ".$config['site_currency'].".</div>";
} else if(!is_numeric($points)) { 
$message = "<div class=\"alert alert-danger\">".$config['site_currency']." must be a valid number!</div>";
} else if($points_total > $user['points']) { 
$message = "<div class=\"alert alert-danger\">You don't have this many ".$config['site_currency']." to use with the coupon!</div>";
} else if(!is_numeric($uses)) {
$message = "<div class=\"alert alert-danger\">Invalid uses value!</div>";
} else {

$points_total = $points * $uses;

$insert_sql = "INSERT INTO `coupons` (coupon_id, user_id, code, points, uses, date) VALUES(NULL, '{$user['user_id']}', '".mysqli_real_escape_string($conn, $code)."', '".mysqli_real_escape_string($conn, $points)."', '".mysqli_real_escape_string($conn, $uses)."', NOW())";
$result = mysqli_query($conn,$insert_sql) or die(mysqli_error($conn));

if($result) {
$remove_points_sql = "UPDATE `users` SET `points`=`points`-'".mysqli_real_escape_string($conn, $points_total)."' WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."' LIMIT 1";
mysqli_query($conn, $remove_points_sql) or die(mysqli_error($conn));
}

$message = "<div class=\"alert alert-success\">The coupon <strong>{$code}</strong> has been successfully created.</div>";
}
}
?>

<div class="col-lg-9">

<h2 class="page-header">Create Coupon</h2>

<?php if(isset($message)) echo $message; ?>

<p>You can create coupons to gain referrals, or generally for anything! When adding a coupon you use your own points!</p>

<form class="form-horizontal" role="form" method="post">

<div class="form-group">
<label class="control-label col-sm-2" for="coupon_code">Coupon Code</label>
<div class="col-sm-10">
<input class="form-control" type="text" name="code" id="code" placeholder="<?php echo coupon_code_generator(); ?>" disabled/>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="coupon_points">Coupon <?php echo $config['site_currency']; ?></label>
<div class="col-sm-10">
<input class="form-control" type="number" name="points" id="points" max="<?php echo $user['points']; ?>" min="1" value="1" required/>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="coupon_type">Coupon Uses</label>
<div class="col-sm-10">
<input class="form-control" type="number" name="uses" id="uses" value="1" min="1" max="100" required/>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="coupon_create"></label>
<div class="col-sm-10">
<button type="submit" name="create" class="btn btn-success">Create</button>
</div>
</div>
</form>

</div>

<?php include('footer.php'); ?>