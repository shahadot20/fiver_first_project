<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/
session_start();

include('header.php');

// redirect user to login to access this page
if(isset($_SESSION['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}

if(isset($_POST['user_signup'])) {

$username = mysqli_real_escape_string($conn, str_clean(strtolower($_POST['username'])));
$email = mysqli_real_escape_string($conn, $_POST['email']);
$get_ip = mysqli_real_escape_string($conn, get_user_ip());

$check_username = mysqli_query($conn, "SELECT username FROM `users` WHERE `username`='".$username."'") or die(mysqli_error($conn));
$username_exists = mysqli_num_rows($check_username);

$check_email = mysqli_query($conn, "SELECT email FROM `users` WHERE `email`='".$email."'") or die(mysqli_error($conn));
$email_exists = mysqli_num_rows($check_email);

$check_ip = mysqli_query($conn, "SELECT ip FROM `users` WHERE `ip`='".$get_ip."'") or die(mysqli_error($conn));
$ip_exists = mysqli_num_rows($check_ip);

$check_ban_ip = mysqli_query($conn, "SELECT * FROM `banned_ips` WHERE  `ban_ip_address`='".$get_ip."'") or die(mysqli_error($conn));
$ip_ban_exists = mysqli_num_rows($check_ban_ip);

if($username_exists == 1) {
$message = "<div class=\"alert alert-danger\">That username is not available.</div>";
} else if($email_exists == 1) {
$message = "<div class=\"alert alert-danger\">That email is already registered.</div>";
} else if($ip_exists == 1) {
$message = "<div class=\"alert alert-danger\">That ip is already registered to an account, you can only have  1 account.</div>";
} else if($ip_ban_exists == 1) {
$message = "<div class=\"alert alert-danger\">That ip is banned, and can no longer be registered with an account.</div>";
} else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) { 
$message = "<div class=\"alert alert-danger\">Invalid email format. Valid: <i>admin@example.com</i></div>";
} else if(!preg_match("/^[a-z0-9]+$/i", $_POST['username'])) {
$message = "<div class=\"alert alert-danger\">Username must contain only letters and numbers.</div>";
} else if(empty($_POST['email']) || $_POST['email'] == "") {
$message = "<div class=\"alert alert-danger\">Email is empty, try again.</div>";
} else if(empty($_POST['username']) || $_POST['username'] == "") {
$message = "<div class=\"alert alert-danger\">Username is empty, try again.</div>";
} else if(empty($_POST['password']) || $_POST['password'] == "") {
$message = "<div class=\"alert alert-danger\">Password is empty, try again.</div>";
} else if(strlen($_POST['username']) > 20) {
$message = "<div class=\"alert alert-danger\">Username must be shorter than 20 characters.</div>";
} else if(strlen($_POST['username']) < 6) {
$message = "<div class=\"alert alert-danger\">Username must be greater than 5 characters.</div>";
} else {

$username = mysqli_real_escape_string($conn, str_clean(strtolower($_POST['username'])));
$email = mysqli_real_escape_string($conn, $_POST['email']);
$password = mysqli_real_escape_string($conn, md5($_POST['password']));
$ip = mysqli_real_escape_string($conn, get_user_ip());

$ref_id = "";
if(isset($_COOKIE['user_referral'])) {
$username = mysqli_real_escape_string($conn, str_clean(strtolower($_POST['username'])));
$ref_ip = mysqli_real_escape_string($conn, get_user_ip());
$ref_id = mysqli_real_escape_string($conn, intval(str_clean($_COOKIE['user_referral'])));
$referral_sql = mysqli_query($conn, "SELECT * FROM `users` WHERE `user_id`='{$ref_id}'") or die(mysqli_error($conn));
$referral = mysqli_fetch_array($referral_sql, MYSQLI_ASSOC);

$get_referrals_sql = mysqli_query($conn, "SELECT * FROM `users_referrals` WHERE `referral_referral` = '".$username."'") or die(mysqli_error($conn));
if(mysqli_num_rows($get_referrals_sql) == 0) {
mysqli_query($conn, "INSERT INTO `users_referrals`(id, referral_username,referral_referral,referral_ip,referral_date_added) values(NULL, '{$referral['username']}','{$username}','{$ref_ip}',NOW())") or die(mysqli_error($conn));


if($config['site_referral_point_bonus'] > 0) {
$site_referral_point_bonus = $config['site_referral_point_bonus'];
} else { 
$site_referral_point_bonus = 0;
}

$text = "You referred {$username}";
	$type = "Referral";
	$provider = "{$config['site_name']}";
	mysqli_query($conn, "INSERT INTO `activity_history`(history_id, user_id, history_type, history_provider, history_text, history_points, history_date) VALUES(NULL, '{$referral['user_id']}', '{$type}', '{$provider}', '{$text}', '{$site_referral_point_bonus}', NOW())") or die(mysqli_error($conn));

mysqli_query($conn, "UPDATE `users` SET `points`=`points`+'{$site_referral_point_bonus}' WHERE `user_id`='{$referral['user_id']}' LIMIT 1") or die(mysqli_error($conn));

$bot_username = $config['site_bot_username'];
$text = "<strong>{$referral['username']}</strong> earned {$config['site_referral_point_bonus']} {$config['site_currency']} for referring <strong>{$username}</strong>! <a href=\"{$config['base_url']}refer.php\" target=\"_blank\">Click here to refer users to {$config['site_name']}.</a>";
$date = date('h:i a', time());
$chat_ip = '127.0.0.1';
mysqli_query($conn, "INSERT INTO chat (id, username, message, ip_address) VALUES (NULL, '$bot_username', '$text', '$chat_ip')") or die(mysqli_error($conn));
}
}

if($config['site_signup_bonus'] > 0) {
$signup_point_bonus = $config['site_signup_bonus'];
} else { 
$signup_point_bonus = 0;
}

$signup_points = $signup_point_bonus;

mysqli_query($conn, "INSERT INTO `users`(user_id, email,username,ip,password,signup,online,points,referral) values(NULL,'{$email}','{$username}','{$ip}','{$password}',NOW(),NOW(),'{$signup_points}','{$ref_id}')") or die(mysqli_error($conn));

$bot_username = $config['site_bot_username'];
$text = "<strong>{$username}</strong> has just signed up!";
$date = date('h:i a', time());
$chat_ip = mysqli_real_escape_string($conn, get_user_ip());
mysqli_query($conn, "INSERT INTO chat (id, username, message, ip_address) VALUES (NULL, '$bot_username', '$text', '$chat_ip')") or die(mysqli_error($conn));

$_SESSION['username'] = $username;
echo "<script>document.location.href='".$config['base_url']."dashboard.php'</script>";

} 
}

?>

<div class="container">

<div class="row row-centered">
<div class="col-lg-5 col-centered">
      <form class="form-signin" role="form" method="post" action="">
        <h2 class="form-signin-heading text-center">Signup</h2>

<?php if(isset($message)) echo $message; ?>

<div class="form-group">
        <label for="inputEmail" class="sr-only">Username</label>
        <input type="email" name="email" id="email" class="form-control" placeholder="Email" required autofocus>
        </div>
        
<div class="form-group">
        <label for="inputEmail" class="sr-only">Username</label>
        <input type="text" name="username" id="inputUsername" class="form-control" placeholder="Username" required autofocus>
        </div>
        
        <div class="form-group">
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
        </div>
<div class="form-group">
        <button class="btn btn-lg btn-primary btn-block" name="user_signup" type="submit">Sign up</button>
                <hr class="sep"></hr>
        <a class="btn btn-lg btn-success btn-block" href="<?php echo $config['base_url']; ?>login.php">Login</a>
        </div>
      </form>

    </div>
    </div>
    </div> <!-- /container -->


<?php include('footer.php'); ?>