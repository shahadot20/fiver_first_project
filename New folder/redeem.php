<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}

$get_provider = str_clean($_GET['provider']);

$provider_sql = mysqli_query($conn, "SELECT * FROM `payment_providers` WHERE `provider_seo_name`='".mysqli_real_escape_string($conn, $get_provider)."'") or die(mysqli_error($conn));
$row = mysqli_fetch_array($provider_sql, MYSQLI_ASSOC);

$check_provider = mysqli_query($conn, "SELECT * FROM `payment_providers` WHERE `provider_seo_name`='".mysqli_real_escape_string($conn, $get_provider)."'") or die(mysqli_error($conn));
$provider_exists = mysqli_num_rows($check_provider);


if(isset($_POST['add_redemption'])) {

$redemption_points = str_clean($_POST['redemption_points']);

if($provider_exists == 0) {
$error = "<li>This payment provider doesn't exist.</li>";
}

if($redemption_points == "" || empty($redemption_points)) {
$error .= "<li>You need to enter the amount of ".$config['site_currency']." you want to redeem for cash.</li>";
}

if(!is_numeric($redemption_points)) {
$error .= "<li>".$config['site_currency']." are only numeric values (0-9).</li>";
}

if($user['points'] < $redemption_points) {
$error .= "<li>You don't have enough ".$config['site_currency']." to make this redemption.</li>";
}

if($redemption_points < $row['provider_minimum']) {
$error .= "<li>The minimum you can withdraw is <strong>".$row['provider_minimum']."</strong> ".$config['site_currency'].".</li>";
}

if($user['payment_email'] == "" || empty($user['payment_email'])) { 
$error .= "<li>You need to enter your payment email via settings.</li>";
}

if(!$error) {

$redemption_points = str_clean($_POST['redemption_points']);

if($row['provider_fee'] > 0) {
$total_points2 = sprintf($redemption_points * (($row['provider_fee']) / 100));
$total_points = $redemption_points - $total_point2;
} else {
$total_points = $redemption_points;
}

mysqli_query($conn, "INSERT INTO `redemption_history`(redemption_id,user_id,redemption_email,redemption_provider,redemption_points,redemption_fee,redemption_date_added,redemption_date_paid,redemption_paid) values(NULL,'".mysqli_real_escape_string($conn, $user['user_id'])."','".mysqli_real_escape_string($conn, $user['payment_email'])."','".mysqli_real_escape_string($conn, $row['provider_name'])."','".mysqli_real_escape_string($conn, $total_points)."','".mysqli_real_escape_string($conn, $row['provider_fee'])."',NOW(),'','0')") or die(mysqli_error($conn));
 
mysqli_query($conn, "UPDATE `users` SET `points`=`points`-'".mysqli_real_escape_string($conn, $total_points)."' WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."' LIMIT 1") or die(mysqli_error($conn));

    $success = "You successfully redeemed <strong>".$total_points."</strong> ".$config['site_currency'].". Please allow up to 48 hours for us to process your redemption.";
}
}
?>

<div class="col-lg-9">

<?php if($provider_exists == 0) { ?>
<h2 class="page-header">Error!</h2>
<div class="alert alert-danger">You never specified a payment provider to redeem with.</div>
<?php } else { ?>
<h2 class="page-header"><?php echo $row['provider_name']; ?></h2>
<p><?php echo $row['provider_description']; ?></p>

<?php if(isset($error)) { echo "<div class=\"alert alert-danger\"><h3>Errors:</h3><ul>".$error."</ul></div>"; } ?>
<?php if(isset($success)) { echo "<div class=\"alert alert-success\">".$success."</div>"; } ?>

<form class="form-horizontal" role="form" method="post">

<div class="form-group">
<label class="control-label col-sm-2" for="<?php echo $config['site_currency']; ?>
"><?php echo $config['site_currency']; ?>
</label>
<div class="col-sm-10">
<input class="form-control" name="redemption_points" id="redemption_points" type="text" value="<?php if(isset($redemption_points)) { echo $redemption_points; } ?>" required/>
<span class="help-block">Input the number of <?php echo $config['site_currency']; ?>
 you want to redeem for cash.</span>
</div>
</div>

<?php if($row['provider_fee'] > 0) { ?>
<div class="form-group">
<label class="control-label col-sm-2" for="network_fee">Fee</label>
<div class="col-sm-10">
<div class="input-group">
  <input type="text" class="form-control" name="provider_fee" id="provider_fee" value="<?php if(isset($row['provider_fee'])) { echo $row['provider_fee']; } ?>" disabled />
  <span class="input-group-addon">%</span>
  </div>
</div>
</div>
<?php } ?>

<?php if($row['provider_referral_url'] != "") { ?>
<div class="form-group">
<label class="control-label col-sm-2" for="network_referral_url_blank"></label>
<div class="col-sm-10">
<strong><a target="_blank" href="<?php if(isset($row['provider_referral_url'])) { echo $row['provider_referral_url']; } ?>">If you don't have an account with <?php echo $row['provider_name']; ?>, click here to sign up!</a></strong>
</div>
</div>
<?php } ?>


<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="add_redemption" type="submit" value="Redeem"/>
</div>
</div>
</form>
<?php } ?>

</div>

<?php include("footer.php"); ?>