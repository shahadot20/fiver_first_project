<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include_once("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}

$adjacents = 5;

    $query = mysqli_query($conn, "select COUNT(*) as num from `ptc_ads` WHERE `ptc_ad_userid`='".mysqli_real_escape_string($conn, $user['user_id'])."'") or die(mysqli_error($conn));
    $total_pages = mysqli_fetch_array($query, MYSQLI_ASSOC);
    $total_pages = $total_pages['num'];

    $limit = $config['site_page_limit'];                                //how many items to show per page

    $page = abs((int) $_GET['page']);

    if($page) 
        $start = ($page - 1) * $limit;          //first item to display on this page
    else
        $start = 0;                             //if no page var is given, set start to 
    /* Get data. */
    $result = mysqli_query($conn, "select * from `ptc_ads` WHERE `ptc_ad_userid`='".mysqli_real_escape_string($conn, $user['user_id'])."' ORDER BY ptc_ad_points DESC LIMIT $start,$limit") or die(mysqli_error($conn));

    /* Setup page vars for display. */
    if ($page == 0) $page = 1;                  //if no page var is given, default to 1.
    $prev = $page - 1;                          //previous page is page - 1
    $next = $page + 1;                          //next page is page + 1
    $lastpage = ceil($total_pages/$limit);      //lastpage is = total pages / items per page, rounded up.
    $lpm1 = $lastpage - 1;                      //last page minus 1

    $pagination = "";
    if($lastpage > 1) {   
        $pagination .= "<ul class=\"pager\">";
        //previous button
        if ($page > 1) 
            $pagination.= "<li class=\"previous\"><a href=\"".$config['base_url']."my_ptc_ads.php?page=$prev\">&laquo; previous</a></li>";
        else
            $pagination.= "<li class=\"previous disabled\"><a>&laquo; previous</a></li>"; 

        //next button
        if ($page < $lastpage) 
            $pagination.= "<li class=\"next\"><a href=\"".$config['base_url']."my_ptc_ads.php?page=$next\">next &raquo;</a></li>";
        else
            $pagination.= "<li class=\"next disabled\"><a>next &raquo;</a></li>";
        $pagination.= "</ul>\n";       
    }

?>

<div class="col-lg-9">

<h2 class="page-header">My PTC Ads</h2>
<p>Displaying all your PTC ads.</p>

<?php if(mysqli_num_rows($result) == 0) { ?>
<div class="alert alert-danger">You currently don't have any PTC ads at this time.</div>
<?php } else { ?>
<table class="table table-condensed">
<tr>
<th>Title</th>
<th><?php echo $config['site_currency']; ?></th>
<th>Timer</th>
<th>Status</th>
<th>Clicks Today</th>
<th>Clicks Total</th>
<th>Date Added</th>
<th></th>
</tr>
<?php while($row = mysqli_fetch_assoc($result)) { ?>
<tr>
<td><?php echo $row['ptc_ad_title']; ?></td>
<td><?php echo $row['ptc_ad_points']; ?></td>
<td><?php echo $row['ptc_ad_timer']; ?>s</td>
<td><?php if($row['ptc_ad_active'] == 1) { ?><div class="label label-success">Active</div><?php } else { ?><div class="label label-warning">Paused</div><?php } ?>
<td><?php echo $row['ptc_ad_today_views']; ?></td>
<td><?php echo $row['ptc_ad_total_views']; ?></td>
<td><?php echo $row['ptc_ad_date_added']; ?></td>
<td><a class="btn btn-xs btn-primary" href="<?php echo $config['base_url']; ?>edit/ptc_ads/<?php echo $row['ptc_ad_id']; ?>">Edit</a>&nbsp;<a class="btn btn-xs btn-danger" href="<?php echo $config['base_url']; ?>delete/ptc_ads/<?php echo $row['ptc_ad_id']; ?>" onClick="if ( !confirm('Are you SURE you wish to delete this PTC Ad?\n\nAll data associated with this PTC Ad will be deleted as well, and this action can NOT be reversed!\n') ) { return false; }">Delete</a></td>
</tr>
<?php } ?>
</table>
<?php } ?>

<?php echo $pagination; ?>

</div>

<?php include_once("footer.php"); ?>