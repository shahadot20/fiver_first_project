<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/
session_start();


include_once("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}

$id = intval(str_clean(filter_var($_GET['id'], FILTER_VALIDATE_INT)));

	switch($_GET['action']) {
		case "delete": delete_coupon($id); break;
		default:  view_coupons(); break;
	}
	
function view_coupons() { 
global $conn, $config, $user;
$adjacents = 5;

    $query = mysqli_query($conn, "select COUNT(*) as num from `coupons` WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."'") or die(mysqli_error($conn));
    $total_pages = mysqli_fetch_array($query, MYSQLI_ASSOC);
    $total_pages = $total_pages['num'];

    $limit = $config['site_page_limit'];                                //how many items to show per page

    $page = abs((int) $_GET['page']);

    if($page) 
        $start = ($page - 1) * $limit;          //first item to display on this page
    else
        $start = 0;                             //if no page var is given, set start to 
    /* Get data. */
    $result = mysqli_query($conn, "select * from `coupons` WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."' ORDER BY date DESC LIMIT $start,$limit");

    /* Setup page vars for display. */
    if ($page == 0) $page = 1;                  //if no page var is given, default to 1.
    $prev = $page - 1;                          //previous page is page - 1
    $next = $page + 1;                          //next page is page + 1
    $lastpage = ceil($total_pages/$limit);      //lastpage is = total pages / items per page, rounded up.
    $lpm1 = $lastpage - 1;                      //last page minus 1

    $pagination = "";
    if($lastpage > 1) {   
        $pagination .= "<ul class=\"pager\">";
        //previous button
        if ($page > 1) 
            $pagination.= "<li class=\"previous\"><a href=\"".$config['base_url']."coupon_manage.php/$prev\">&laquo; previous</a></li>";
        else
            $pagination.= "<li class=\"previous disabled\"><a>&laquo; previous</a></li>"; 

        //next button
        if ($page < $lastpage) 
            $pagination.= "<li class=\"next\"><a href=\"".$config['base_url']."coupon_manage.php/$next\">next &raquo;</a></li>";
        else
            $pagination.= "<li class=\"next disabled\"><a>next &raquo;</a></li>";
        $pagination.= "</ul>\n";       
    }

?>

<div class="col-lg-9">

<h2 class="page-header">Manage Coupons</h2>

<?php if(isset($message)) echo $message; ?>
 
<p>All your coupons will be displayed here.</p>

<?php if(mysqli_num_rows($result) == 0) { ?>
<div class="alert alert-danger">You haven't created any coupons yet.</div>
<?php } else { ?>
<table class="table">
<tr>
<th>Code</th>
<th><?php echo $config['site_currency']; ?></th>
<th>Uses Left</th>
<th>Date Created</th>
<th>&nbsp;</th>
</tr>
<?php while($row = mysqli_fetch_array($result)) {  ?>
<tr>
<td><?php echo $row['code']; ?></td>
<td><?php echo $row['points']; ?></td>
<td><?php echo $row['uses']; ?></td>
<td><?php echo $row['date']; ?></td>
<td><a href="<?php echo $config['base_url']; ?>coupon/manage?action=delete&id=<?php echo $row['coupon_id']; ?>">Delete</a></td>

</tr>
<?php } ?>
</table>
<?php } ?>

<?php echo $pagination; ?>

<?php } ?>

<?php

$id = intval(str_clean(filter_var($_GET['id'], FILTER_VALIDATE_INT)));

function delete_coupon($id) {
global $conn, $config, $user;

if(isset($id)) {
    
$id = str_clean(filter_var($_GET['id'], FILTER_VALIDATE_INT));

$get_coupon = mysqli_query($conn, "SELECT * FROM `coupons` WHERE `coupon_id`='".mysqli_real_escape_string($conn, $id)."' AND `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."'") or die(mysqli_error($conn));
$coupon = mysqli_fetch_array($get_coupon);

$points_total = $coupon['points'] * $coupon['uses'];

if(mysqli_num_rows($get_coupon) == 0) {
$message = "<div class=\"alert alert-danger\">The coupon has already been deleted or doesn't currently exist.</div>";

} else {
    
mysqli_query($conn, "UPDATE `users` SET points=points+'".mysqli_real_escape_string($conn, $points_total)."' WHERE user_id='".mysqli_real_escape_string($conn, $user['user_id'])."' LIMIT 1") or die(mysqli_error($conn));

mysqli_query($conn, "DELETE FROM `coupons` WHERE `coupon_id`='".mysqli_real_escape_string($conn, $id)."' AND `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."'") or die(mysqli_error($conn));
$message = "<div class=\"alert alert-success\">The coupon has been deleted.</div>";
}

echo "<div class=\"col-lg-9\">
<h2 class=\"page-header\">Delete Coupon</h2>";
if(isset($message)) echo $message;
echo "<a class=\"btn btn-primary\" href=\"".$config['base_url']."coupon/manage\">Back to Coupons</a>";
echo "</div>";
}
}
?>

</div>

<?php include_once("footer.php"); ?>