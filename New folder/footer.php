<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

if (strpos($_SERVER['PHP_SELF'], "footer.php") !== false) {
echo "<script>document.location.href='".$config['base_url']."'</script>";
exit;
}


if(isset($_SESSION['username'])) {
$get_offers_sql = mysqli_query($conn, "SELECT * FROM `activity_history` WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."'") or die(mysqli_error($conn))
;
$offers_count = mysqli_num_rows($get_offers_sql);

$get_referrals_sql = mysqli_query($conn, "SELECT * FROM `users_referrals` WHERE `referral_username`='".mysqli_real_escape_string($conn, $user['username'])."'") or die(mysqli_error($conn))
;
$referrals_count = mysqli_num_rows($get_referrals_sql);
}

?>

<?php if(isset($_SESSION['username'])) { ?>
<div class="col-md-3">

<?php if($config['site_daily_active'] == 1) { ?>
<div class="panel panel-default">
  <div class="panel-body">
  <div class="page-header text-center no-top-margin"><h4>Daily Bonus</h4></div>
  
  <?php if($user['daily_bonus'] == 0) { ?>
  <p>Complete <?php echo $config['site_daily_offers']; ?> offers worth at least <?php echo $config['site_daily_offers_points_minimum']; ?> <?php echo $config['site_currency']; ?> to be rewarded <?php echo $config['site_daily_offers_points']; ?> <?php echo $config['site_currency']; ?>.</p>
<div class="progress">
  <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="<?php echo $user['daily_bonus_offers']; ?>" aria-valuemin="0" aria-valuemax="<?php echo $config['site_daily_offers']; ?>" style="min-width: <?php echo daily_bonus_percentage($user['daily_bonus_offers'],$config['site_daily_offers']); ?>%;">
  </div>
</div>

<?php if($user['daily_bonus_offers'] >= $config['site_daily_offers'] && $user['daily_bonus'] == 0) { ?>
<a class="btn btn-xs btn-success btn-block" href="<?php echo $config['base_url']; ?>daily_bonus.php">Claim Daily Bonus</a>
<?php } ?>
<?php } else { ?>
<div class="alert alert-danger">You already claimed the daily bonus today.</div>
<?php } ?>
</div>
</div>
<?php } ?>

<div class="panel panel-default">
<div class="panel-heading">
  <h3 class="panel-title"><i class="fa fa-university fa-panel" aria-hidden="true"></i> <?php echo $config['site_currency']; ?> Balance</h3>
  </div>
  <div class="panel-body">
    <div class="points-balance"><?php echo number_format($user['points'],3); ?></div>
    <div class="text-center"><span class="badge badge-info"><?php echo $config['site_point_conversion']; ?> <?php echo $config['site_currency']; ?> = $1</span></div>
  </div>
</div>

<div class="panel panel-default">
<div class="panel-heading">
  <h3 class="panel-title"><i class="fa fa-usd fa-panel" aria-hidden="true"></i> Cash Balance</h3>
  </div>
  <div class="panel-body">
    <div class="points-balance"><?php echo "$".number_format(convert($user['points']),2); ?></div>
  </div>
</div>

<div class="panel panel-default">
<div class="panel-heading">
  <h3 class="panel-title"><i class="fa fa-university fa-panel" aria-hidden="true"></i> Advertiser Balance</h3>
  </div>
  <div class="panel-body">
    <div class="points-balance"><?php echo number_format($user['advertiser_points'],3); ?></div>
    <a class="btn btn-xs btn-block btn-success" href="<?php echo $config['base_url']; ?>store/ptc_adpacks">Get more advertiser balance</a>
  </div>
</div>

<?php if($user['chat_status'] == 1) { ?>

  <?php
  $get_chatban_users = mysqli_query($conn, "SELECT * FROM chat_bans WHERE ban_username='".mysqli_real_escape_string($conn, $user['username'])."' LIMIT 1");
  if(mysqli_num_rows($get_chatban_users) == 0) { 
  ?>
<div class="panel panel-default">
<div class="panel-heading">
  <h3 class="panel-title"><i class="fa fa-comments fa-panel" aria-hidden="true"></i> Chat</h3>
  </div>
  <div class="panel-body">
 <iframe src="<?php echo $config['base_url']; ?>chat/index.php" height="380" width="100%"></iframe> 
  </div>
</div>
<?php } ?>
<?php } ?>

</div>
<?php } ?>

<div class="clear"></div>

<!-- Footer content -->
<footer class="footer">
      <div class="container">
      <div class="row">
          
          <ul class="list-unstyled footer-links">
              <?php
              $get_pages = mysqli_query($conn, "SELECT * FROM `pages` WHERE `page_active`='1'"); ?>
              <?php while($page = mysqli_fetch_assoc($get_pages)) { ?>
                  <li><a href="<?php echo $config['site_url']; ?>page/<?php echo $page['page_seo_name']; ?>"><?php echo $page['page_name']; ?></a></li>
              <?php } ?>
              <li><a href="<?php echo $config['site_url']; ?>contact-us">Contact Us</a></li>
                            <li class="pull-right"><a href="#top">Back to top</a></li>
          </ul>
          
          <div class="clear"></div>
          <div class="footer-copyright">
        <p class="text-muted go-down">Copyright &copy; <?php echo $config['site_name']; ?>. All rights reserved. Powered by <a target="_blank" href="https://www.scriptbucks.com/item/gpt-reward-php-script"><strong>GPT Reward PHP Script <i class="fa fa-link" aria-hidden="true"></i></strong></a></p>
        </div>
      </div>
      </div>
</footer>
<!-- End of Footer content -->

<!-- Le Javascript -->

<!-- Placed at bottom so pages load faster. -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script><!-- jQuery always has to load first for Bootstrap -->
<script src="<?php echo $config['base_url']; ?>assets/js/bootstrap.min.js"></script>

<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
<!-- End of Javascript -->

</body>
</html>