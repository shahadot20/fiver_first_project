<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include_once("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}

?>

<div class="col-lg-9">

<div class="row-centered">
<h2 class="page-header">Referral Benefits</h2>

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">
          <i class="fa fa-gift fa-icon-lg"></i>
          <h2>Offer Bonus</h2>
          <p>Earn an additional <?php echo$config['site_referral_bonus']."%"; ?> <?php echo $config['site_currency']; ?> for each offer your referral completes.</p>

        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <i class="fa fa-user fa-icon-lg"></i>
          <h2>Referral Bonus</h2>
          <p>For each referral that signs up under your link, you'll earn <?php echo $config['site_referral_point_bonus']; ?> <?php echo $config['site_currency']; ?>.</p>
         
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <i class="fa fa-money fa-icon-lg"></i>
          <h2>Unlimited Earnings</h2>
          <p>Unlike other sites that cap your referral earnings, we dont! Take part of our unlimited referral earnings feature.
          </p>
          
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->

</div>

<h2 class="page-header">Referral Link</h2>
<p>Share your referral link with your friends, on social media or other websites. <i>Please do not spam our links!</i></p>
<div class="form-horizontal">
<div class="form-group">
<label class="control-label col-sm-2" for="referral_link">Referral Link</label>
<div class="col-sm-10">
<div class="input-group">
<span class="input-group-addon" id="basic-addon1"><i class="fa fa-link"></i></span>
<input type="text" class="form-control" name="referral_link" id="referral_link" value="<?php echo $config['base_url']; ?>?ref=<?php echo $user['user_id']; ?>">
</div>
</div>
</div>

</div>
</div>

<?php include_once("footer.php"); ?>