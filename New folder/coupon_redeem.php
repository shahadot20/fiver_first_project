<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include_once("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}

if(isset($_POST['redeem'])) {

$code = str_clean($_POST['code']);

$coupon_sql = "SELECT * FROM `coupons` WHERE `code`='".mysqli_real_escape_string($conn, $code)."'";
$result = mysqli_query($conn,$coupon_sql);
$coupon = mysqli_fetch_array($result);
$coupon_exists = mysqli_num_rows($result);

if($coupon_exists == 0) {
$message = "<div class=\"alert alert-danger\">This coupon doesn't exist.</div>";
} else if(empty($code) || $code == "") {
$message = "<div class=\"alert alert-danger\">You never entered a coupon code. Please try again.</div>";
} else if($coupon['user_id'] == $user['user_id']) {
$message = "<div class=\"alert alert-danger\">You can't redeem your own coupon.</div>";
} else if($coupon['uses'] == 0) {
$message = "<div class=\"alert alert-danger\">This coupon doesn't have any uses left.</div>";
} else {

$coupon_history_sql = "SELECT * FROM `coupons_history` WHERE `coupon_code`='".mysqli_real_escape_string($conn, $code)."' AND `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."'";
$result2 = mysqli_query($conn,$coupon_history_sql);
$coupon_exists = mysqli_num_rows($result2);

if($coupon_exists == 0) {
$insert_sql = "INSERT INTO `coupons_history` (id, user_id, coupon_code, points, date) VALUES(NULL, '{$user['user_id']}', '".mysqli_real_escape_string($conn, $code)."', '".mysqli_real_escape_string($conn, $coupon['points'])."', NOW())";
$result = mysqli_query($conn,$insert_sql);


$remove_points_sql = "UPDATE `users` SET `points`=`points`+'".mysqli_real_escape_string($conn, $coupon['points'])."' WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."' LIMIT 1";
mysqli_query($conn, $remove_points_sql);
$adjust_coupon_use_sql = "UPDATE `coupons` SET `uses`=`uses`-'1' WHERE `coupon_id`='".mysqli_real_escape_string($conn, $coupon['coupon_id'])."' LIMIT 1";


$message = "<div class=\"alert alert-success\">You successfully redeemed coupon <strong>{$code}</strong> for {$coupon['points']} {$$config['site_currency']}!</div>";
} else {
$message = "<div class=\"alert alert-danger\">You already redeemed this coupon.</div>";
}
}
}
?>

<div class="col-lg-9">

<h2 class="page-header">Redeem Coupon</h2>


<?php if(isset($message)) echo $message; ?>

<p>Do you have a coupon code? You can redeem coupon codes for <?php echo $config['site_currency']; ?>!</p>


<form class="form-horizontal" role="form" method="post">

<div class="form-group">
<label class="control-label col-sm-2" for="coupon_code">Coupon Code</label>
<div class="col-sm-10">
<input class="form-control" type="text" name="code" id="code" placeholder="Coupon code" required/>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="coupon_redeem"></label>
<div class="col-sm-10">
<button type="submit" name="redeem" class="btn btn-success">Redeem</button>
</div>
</div>
</form>

</div>

<?php include('footer.php'); ?>