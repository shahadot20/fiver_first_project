<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include_once("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}

$adjacents = 5;

    $query = mysqli_query($conn, "select COUNT(*) as num from `ptc_ads` WHERE `ptc_ad_active`='1' AND (SELECT `advertiser_points` FROM `users` WHERE `user_id` = `ptc_ads`.`ptc_ad_userid` ) >= `ptc_ad_points` AND `ptc_ad_id` NOT IN (SELECT `ptc_ad_siteid` FROM `ptc_ads_history` WHERE `ptc_ad_userid`='{$user['user_id']}') AND `ptc_ad_userid` != '".mysqli_real_escape_string($conn, $user['user_id'])."' ORDER BY ptc_ad_points DESC") or die(mysqli_error($conn));
    $total_pages = mysqli_fetch_array($query, MYSQLI_ASSOC);
    $total_pages = $total_pages['num'];

    $limit = $config['site_page_limit'];                                //how many items to show per page

    $page = abs((int) $_GET['page']);

    if($page) 
        $start = ($page - 1) * $limit;          //first item to display on this page
    else
        $start = 0;                             //if no page var is given, set start to 
    /* Get data. */
    $result = mysqli_query($conn, "select * from `ptc_ads` WHERE `ptc_ad_active`='1' AND (SELECT `advertiser_points` FROM `users` WHERE `user_id` = `ptc_ads`.`ptc_ad_userid` ) >= `ptc_ad_points` AND `ptc_ad_id` NOT IN (SELECT `ptc_ad_siteid` FROM `ptc_ads_history` WHERE `ptc_ad_userid`='{$user['user_id']}') AND `ptc_ad_userid` != '".mysqli_real_escape_string($conn, $user['user_id'])."' ORDER BY ptc_ad_points DESC LIMIT $start,$limit") or die(mysqli_error($conn));

    /* Setup page vars for display. */
    if ($page == 0) $page = 1;                  //if no page var is given, default to 1.
    $prev = $page - 1;                          //previous page is page - 1
    $next = $page + 1;                          //next page is page + 1
    $lastpage = ceil($total_pages/$limit);      //lastpage is = total pages / items per page, rounded up.
    $lpm1 = $lastpage - 1;                      //last page minus 1

    $pagination = "";
    if($lastpage > 1) {   
        $pagination .= "<ul class=\"pager\">";
        //previous button
        if ($page > 1) 
            $pagination.= "<li class=\"previous\"><a href=\"".$config['base_url']."ptc.php?page=$prev\">&laquo; previous</a></li>";
        else
            $pagination.= "<li class=\"previous disabled\"><a>&laquo; previous</a></li>"; 

        //next button
        if ($page < $lastpage) 
            $pagination.= "<li class=\"next\"><a href=\"".$config['base_url']."ptc.php?page=$next\">next &raquo;</a></li>";
        else
            $pagination.= "<li class=\"next disabled\"><a>next &raquo;</a></li>";
        $pagination.= "</ul>\n";       
    }

?>

<div class="col-lg-9">

<h2 class="page-header">PTC Ads</h2>
<p>Earn <?php echo $config['site_currency']; ?>
 easily by clicking on member's PTC ads.</p>

<?php if(mysqli_num_rows($result) == 0) { ?>
<div class="alert alert-danger">There are currently no available ads at this time. Please try again later. </div>
<?php } else { ?>
<table class="table table-condensed">
<tr>
<th>Title</th>
<th>Description</th>
<th><?php echo $config['site_currency']; ?></th>
<th>Timer</th>
<th>Date Added</th>
<th></th>
</tr>
<?php while($row = mysqli_fetch_assoc($result)) { ?>
<tr>
<td><?php echo $row['ptc_ad_title']; ?></td>
<td><?php echo $row['ptc_ad_description']; ?></td>
<td><?php echo $row['ptc_ad_points']; ?></td>
<td><?php echo $row['ptc_ad_timer']; ?> seconds</td>
<td><?php echo $row['ptc_ad_date_added']; ?></td>
<td><a class="btn btn-xs btn-success" href="<?php echo $config['base_url']; ?>ptc_view.php/<?php echo $row['ptc_ad_id']; ?>" target="_blank">View Ad</a></td>

</tr>
<?php } ?>
</table>
<?php } ?>

<?php echo $pagination; ?>

</div>

<?php include_once("footer.php"); ?>