<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include_once("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}

$get_network = str_clean($_GET['network']);

$network_sql = mysqli_query($conn, "SELECT * FROM `networks` WHERE `network_seo_name`='".mysqli_real_escape_string($conn, $get_network)."' LIMIT 1");
$row = mysqli_fetch_array($network_sql, MYSQLI_ASSOC);

$check_network = mysqli_query($conn, "SELECT * FROM `networks` WHERE `network_seo_name`='".mysqli_real_escape_string($conn, $get_network)."' LIMIT 1");
$network_exists = mysqli_num_rows($check_network);

$network_html_code = str_replace('-USERNAME-',$user['username'],$row['network_html_code']);

?>

<div class="col-lg-9">

<?php if($network_exists == 0) { ?>
<h2 class="page-header">Error!</h2>
<div class="alert alert-danger">This network doesn't exist.</div>
<?php } else { ?>
<h2 class="page-header"><?php echo $row['network_name']; ?></h2>
<p><?php echo $row['network_description']; ?></p>

<?php echo html_entity_decode($network_html_code); ?>

<?php } ?>

</div>

<?php include_once("footer.php"); ?>