<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}

$check_lottery = mysqli_query($conn, "SELECT * FROM `lottery` WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."'") or die(mysqli_error($conn));
$lottery_count = mysqli_num_rows($check_lottery);


if(isset($_GET['action']) && $_GET['action'] == "enter") {
if(isset($_POST['add_entry'])) {

$entry_points = str_clean($_POST['entry_points']);

if($lottery_count >= $config['lottery_limit']) {
$error = "<li>You can only enter the lottery ".$config['lottery_limit']." times per day. Try again tomorrow!</li>";
}

if($entry_points == "" || empty($entry_points)) {
$error .= "<li>You never entered any ".$config['site_currency']." for the lottery entry.</li>";
}

if(!is_numeric($entry_points)) {
$error .= "<li>".$config['site_currency']." are only numeric values (0-9).</li>";
}

if($user['points'] < $entry_points) {
$error .= "<li>You don't have enough ".$config['site_currency']." to make this entry.</li>";
}

if($entry_points < $config['lottery_minimum']) {
$error .= "<li>The minimum entry fee is <strong>".$config['lottery_minimum']."</strong> ".$config['site_currency'].".</li>";
}


if(!$error) {

$entry_points = str_clean($_POST['entry_points']);
$entry_fee = $config['lottery_fee'];

if($config['lottery_fee'] > 0) {

$total_points2 = ($entry_fee / 100) * $entry_points;
$total_points = $entry_points - $total_points2;

} else {
$total_points = $entry_points;
}

mysqli_query($conn, "INSERT INTO `lottery`(id, user_id,points) values(NULL, '".mysqli_real_escape_string($conn, $user['user_id'])."','".mysqli_real_escape_string($conn, $total_points)."')") or die(mysqli_error($conn));
 
mysqli_query($conn, "UPDATE `users` SET `points`=`points`-'".mysqli_real_escape_string($conn, $entry_points)."' WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."' LIMIT 1") or die(mysqli_error($conn));

    $success = "You successfully entered the daily lottery. Good luck!";
}
}

?>

<div class="col-lg-9">

<h2 class="page-header">Enter the Lottery <a class="pull-right btn btn-success" href="<?php echo $config['base_url']; ?>lottery.php">Back to Lottery</a></h2>
<p>Try your chance at winning the Daily Lottery. The more you enter the higher chance you have of winning the lottery!</p>

<p>Note: The entry fee is <?php echo $config['lottery_fee']."%"; ?> of the <?php echo $config['site_currency']; ?>
 per entry/ticket! Once you enter you can not get the <?php echo $config['site_currency']; ?>
 back, so buy a ticket at your own risk!</p>

<?php if(isset($error)) { echo "<div class=\"alert alert-danger\"><h3>Errors:</h3><ul>".$error."</ul></div>"; } ?>
<?php if(isset($success)) { echo "<div class=\"alert alert-success\">".$success."</div>"; } ?>

<form class="form-horizontal" role="form" method="post">

<div class="form-group">
<label class="control-label col-sm-2" for="<?php echo $config['site_currency']; ?>
">Entry <?php echo $config['site_currency']; ?>
</label>
<div class="col-sm-10">
<input class="form-control" name="entry_points" id="entry_points" type="text" value="<?php if(isset($entry_points)) { echo $entry_points; } ?>" required/>
<span class="help-block">Input the number of <?php echo $config['site_currency']; ?> you want to enter the lottery with.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="network_fee">Entry Fee</label>
<div class="col-sm-10">
<div class="input-group">
  <input type="text" class="form-control" name="entry_fee" id="entry_fee" value="<?php if(isset($config['lottery_fee'])) { echo $config['lottery_fee']; } else { echo "0"; } ?>" disabled />
  <span class="input-group-addon">%</span>
  </div>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="add_entry" type="submit" value="Buy Lottery Ticket!"/>
</div>
</div>
</form>

</div>
<?php }  else { ?>
<div class="col-lg-9">

<h2 class="page-header">The Lottery <a class="pull-right btn btn-success" href="<?php echo $config['base_url']; ?>lottery.php?action=enter">Enter Lottery</a></h2>
<p>Each day we run a lottery. At the end of the day a random winner is chosen. If you win the lottery, you will win all the <?php echo $config['site_currency']; ?>
 that is currently in the lottery.</p>

<h2 class="page-header">Lottery Stats</h2>
<p>Here are the current lottery stats.</p>

<?php

$result = mysqli_query($conn, "select * from `lottery`");

$entree__sql = mysqli_query($conn, "SELECT * FROM `lottery`") or die(mysqli_error($conn));
$total_lottery_entrees = mysqli_num_rows($entree__sql);

$total_lottery_points = mysqli_fetch_array(mysqli_query($conn, "SELECT SUM(`points`) AS `points` FROM `lottery`"));

if(mysqli_num_rows($result) == 0) { ?>
<div class="alert alert-danger">No user entered today's lottery, <a class="alert-link" href="<?php echo $config['base_url']; ?>lottery.php?action=enter">be the first click here!</a></div>
<?php } else { ?>
<table class="table">
<tr>
<th>Lottery Entrees</th>
<th>Total <?php echo $config['site_currency']; ?>
</th>
<th>$ Value</th>
</tr>
<tr>
<td><?php echo $total_lottery_entrees; ?></td>
<td><?php echo $total_lottery_points['points']; ?></td>
<td><?php echo "$".convert($total_lottery_points['points']); ?></td>
</tr>
</table>
<?php } ?>


<h2 class="page-header">Lottery Winner History</h2>
<p>Displays the last 30 lottery winners.</p>

<?php

$result = mysqli_query($conn, "SELECT * FROM `lottery_winners` ORDER BY `date` DESC LIMIT 30") or die(mysqli_error($conn));

if(mysqli_num_rows($result) == 0) { ?>
<div class="alert alert-danger">There is no history of any lottery winner yet.</div>
<?php } else { ?>
<table class="table">
<tr>
<th>User</th>
<th><?php echo $config['site_currency']; ?></th>
<th>$ Value</th>
<th>Date</th>
</tr>
<?php while($row = mysqli_fetch_assoc($result)) { ?>
<tr>
<td><?php echo userid_to_username($row['user_id']); ?></td>
<td><?php echo $row['points']; ?></td>
<td><?php echo "$".convert($row['points']); ?></td>
<td><?php echo $row['date']; ?></td>
</tr>
<?php } ?>
</table>
<?php } ?>

</div>
<?php } ?>

<?php include("footer.php"); ?>