<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include_once("includes/config.php");

if (strpos($_SERVER['PHP_SELF'], "header.php") !== false) {
echo "<script>document.location.href='".$config['base_url']."'</script>";
exit;
}


if($user['banned'] > 0) {
session_destroy();
echo "<script>document.location.href='".$config['base_url']."'</script>";
exit;
}

if(isset($_GET['ref'])) {

    $ref_id = intval(str_clean($_GET['ref']));
    $ip = mysqli_real_escape_string($conn, str_clean(get_user_ip()));
    
    $ref = mysqli_fetch_array(mysqli_query($conn, "SELECT ip FROM `users` WHERE `user_id`='".mysqli_real_escape_string($conn, $ref_id)."'"));

    if(!isset($ref['ip'])) {
        setcookie("user_referral", $ref_id, time()+ (365 * 24 * 60 * 60));
    } else if($ref['ip'] != $ip) {
        setcookie("user_referral", $ref_id, time()+ (365 * 24 * 60 * 60));
    }
}

if(isset($user['username'])) {

$get_offers_sql = mysqli_query($conn, "SELECT * FROM `activity_history` WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."' AND `history_type`='Offer'") or die(mysqli_error($conn));
$offers_count = mysqli_num_rows($get_offers_sql);

$get_referrals_sql = mysqli_query($conn, "SELECT * FROM `users_referrals` WHERE `referral_username`='".mysqli_real_escape_string($conn, $user['username'])."'") or die(mysqli_error($conn));
$referrals_count = mysqli_num_rows($get_referrals_sql);

$get_redemptions_sql = mysqli_query($conn, "SELECT * FROM `redemption_history` WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."'") or die(mysqli_error($conn));
$redemptions_count = mysqli_num_rows($get_redemptions_sql);

}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    
    <title><?php if(isset($title)) { echo $title; } else { echo $config['site_name']; } ?></title>
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php if(isset($description)) { echo $description; } else { echo $config['site_description']; } ?>">
    <meta name="keywords" content="<?php if(isset($keywords)) { echo $keywords; } else { echo $config['site_keywords']; } ?>">
    <meta name="author" content="Scriptbucks.com">

    <!-- Le styles -->
    <link href="<?php echo $config['base_url']; ?>assets/css/bootstrap.<?php if(empty($user['site_theme']) || $user['site_theme'] == "") { echo $config['site_theme']; } else { echo $user['site_theme']; } ?>.min.css" rel="stylesheet">
    <link href="<?php echo $config['base_url']; ?>assets/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <link href="<?php echo $config['base_url']; ?>assets/css/style.css" rel="stylesheet">
   
  </head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php if(!isset($user['username'])) { ?><?php echo $config['base_url']; ?><?php } else { ?><?php echo $config['base_url']; ?>dashboard<?php } ?>"><?php echo $config['site_name']; ?></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-1">
                <ul class="nav navbar-nav">
                <?php if(isset($user['username'])) { ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true"><i class="fa fa-usd"></i> Earn <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo $config['base_url']; ?>ptc.php">PTC Ads</a></li>
                        <h5 class="dropdown-header">Offerwalls</h5>
                        <?php 
$select_networks= mysqli_query($conn, "SELECT * FROM `networks` WHERE `network_active`='1' ORDER BY network_popular DESC, network_name ASC") or die(mysqli_error($conn));
while($net_row = mysqli_fetch_array($select_networks)):

$popular = '';
if($net_row['network_popular'] == 1) {
    $popular = '<span class="label label-danger label-sm label-popular"><i class="fa fa-fire fa-fw" aria-hidden="true"></i></span>';
}
?>
<li><a href="<?php echo $config['base_url']; ?>earn.php/<?php echo $net_row['network_seo_name']; ?>"><?php echo $net_row['network_name']; ?> <?php echo $popular; ?></a></li>
<li class="clear"></li>
<?php endwhile; ?>
                          
                        </ul>
                      </li>
                      <?php if($config['site_daily_active'] == 1 || $config['lottery_active'] == 1) { ?>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true"><i class="fa fa-gamepad"></i> Games <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                        <?php if($config['site_daily_active'] == 1) { ?><li><a href="<?php echo $config['base_url']; ?>daily_bonus.php">Daily Bonus</a></li><?php } ?>
                        
<?php if($config['lottery_active'] == 1) { ?><li><a href="<?php echo $config['base_url']; ?>lottery.php">Lottery</a></li><?php } ?>
                        </ul>
                      </li>
                      <?php } ?>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true"><i class="fa fa-link"></i> Promote <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
<li><a href="<?php echo $config['base_url']; ?>refer.php">Referral Link</a></li>
                        </ul>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true"><i class="fa fa-money"></i> Redeem <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                      <h5 class="dropdown-header">Cash Rewards</h5>

                        <?php 
$select_providers= mysqli_query($conn, "SELECT * FROM `payment_providers` WHERE `provider_active`='1'") or die(mysqli_error($conn));
while($pro_row = mysqli_fetch_array($select_providers)): ?>
<li><a href="<?php echo $config['base_url']; ?>redeem/<?php echo $pro_row['provider_seo_name']; ?>"><?php echo $pro_row['provider_name']; ?></a></li>
<?php endwhile; ?>
                       <h5 class="dropdown-header">Cryptocurrencies</h5>
                        <?php 
$select_cryptocurrency_providers= mysqli_query($conn, "SELECT * FROM `cryptocurrency_providers` WHERE `provider_active`='1'") or die(mysqli_error($conn));
while($cry_row = mysqli_fetch_array($select_cryptocurrency_providers)): ?>
<li><a href="<?php echo $config['base_url']; ?>redeem_crypto/<?php echo $cry_row['provider_seo_name']; ?>"><?php echo $cry_row['provider_name']; ?></a></li>
<?php endwhile; ?>
                        </ul>
                      </li>
                      
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true"><i class="fa fa-ticket"></i> Coupon <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
<li><a href="<?php echo $config['base_url']; ?>coupon_create.php">Create Coupon</a></li>
<li><a href="<?php echo $config['base_url']; ?>coupon_manage.php">Manage Coupons</a></li>
<li><a href="<?php echo $config['base_url']; ?>coupon_redeem.php">Redeem Coupon</a></li>
                        </ul>
                      </li>
                      
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true"><i class="fa fa-shopping-cart"></i> Store <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
<li><a href="<?php echo $config['base_url']; ?>ptc_adpacks.php">PTC Adpacks</a></li>
                        </ul>
                      </li>
                      <?php } ?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                   <?php if(!isset($_SESSION['username'])) { ?>
                    <li><a href="<?php echo $config['base_url']; ?>signup.php">Signup</a></li>
                    <li><a href="<?php echo $config['base_url']; ?>login.php">Login</a></li>
                    <?php } else { ?>
                    <li><a href="<?php echo $config['base_url']; ?>history_points.php"><i class="fa fa-bank"></i> <?php echo number_format($user['points']); ?> <?php echo $config['site_currency']; ?></a></li>
                    <li><a href="<?php echo $config['base_url']; ?>hstory_redemptions.php"><i class="fa fa-credit-card"></i> <?php echo number_format($redemptions_count); ?></a></li>
                    <li><a href="<?php echo $config['base_url']; ?>history_referrals.php"><i class="fa fa-user"></i> <?php echo number_format($referrals_count); ?></a></li>
                    <li><a href="<?php echo $config['base_url']; ?>history_points.php"><i class="fa fa-check"></i> <?php echo number_format($offers_count); ?></a></li>
                    <li class="dropdown">
                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true"><?php echo $user['username']; ?> <span class="caret"></span></a>
                       <ul class="dropdown-menu" role="menu">
                       <li><a href="<?php echo $config['base_url']; ?>dashboard.php"><?php if($user['news_count']) { ?><span class="label label-danger pull-right label-sm"><?php echo $user['news_count']; ?></span><?php } ?><span>Dashboard</span> </a></li>
                       <h5 class="dropdown-header">History</h5>
                       <li><a href="<?php echo $config['base_url']; ?>history_points.php"><?php echo $config['site_currency']; ?> History</a></li>
                       <li><a href="<?php echo $config['base_url']; ?>history_redemptions.php">Redeem History</a></li>
                       <li><a href="<?php echo $config['base_url']; ?>history_referrals.php">Referral History</a></li>
                       <li><a href="<?php echo $config['base_url']; ?>history_store.php">Store History</a></li>
                       <h5 class="dropdown-header">Advertiser</h5>
                       <li><a href="<?php echo $config['base_url']; ?>add_ptc_ads.php">Add PTC Ads</a></li>
                       <li><a href="<?php echo $config['base_url']; ?>my_ptc_ads.php">My PTC Ads</a></li>
                       <li><a href="<?php echo $config['base_url']; ?>converter.php">Balance Converter</a></li>
                       <h5 class="dropdown-header">Settings</h5>
                       <li><a href="<?php echo $config['base_url']; ?>settings.php">Settings</a></li>
                       <li><a href="<?php echo $config['base_url']; ?>logout.php">Log off</a></li>
                       </ul>
                    </li>
                    
                    <?php } ?>
                    </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>