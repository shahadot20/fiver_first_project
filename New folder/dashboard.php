<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/
session_start();


include_once("header.php");

// redirect user to login to access this page
if(!isset($user['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}

$get_offers_sql = mysqli_query($conn, "SELECT * FROM `activity_history` WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."' AND `history_type`='Offer'") or die(mysqli_error($conn));
$offers_count = mysqli_num_rows($get_offers_sql);

$get_referrals_sql = mysqli_query($conn, "SELECT * FROM `users_referrals` WHERE `referral_username`='".mysqli_real_escape_string($conn, $user['username'])."'") or die(mysqli_error($conn));
$referrals_count = mysqli_num_rows($get_referrals_sql);

$get_redemptions_sql = mysqli_query($conn, "SELECT SUM(redemption_points) FROM redemption_history WHERE user_id = '".mysqli_real_escape_string($conn, $user['user_id'])."' AND `redemption_paid`='1'") or die(mysqli_error($conn));
while($row = mysqli_fetch_array($get_redemptions_sql)){
$redemptions_count = $row['SUM(redemption_points)'];
}
?>

<style>
.stats-list {
padding: 10px;
padding-bottom: 0px !important;
}
</style>

<div class="col-lg-9">

<h2 class="page-header">Stats</h2>

<div class="row">

<div class="col-lg-3">
<div class="panel panel-default">
  <div class="panel-body text-center">
  
  <i class="fa fa-calendar-check-o fa-3x" aria-hidden="true"></i>
    <p class="stats-list text-success fa-2x"><?php echo number_format($user['daily_offers']); ?></p>
    <h4 class="text-muted">Offers Today</h4>
  </div>
</div>
</div>

<div class="col-lg-3">
<div class="panel panel-default">
  <div class="panel-body text-center">
  <i class="fa fa-check fa-3x" aria-hidden="true"></i>
    <p class="stats-list text-success fa-2x"><?php echo number_format($offers_count); ?></p>
    <h4 class="text-muted">Offers Total</h4>
  </div>
</div>
</div>

<div class="col-lg-3">
<div class="panel panel-default">
  <div class="panel-body text-center">
  <i class="fa fa-user fa-3x" aria-hidden="true"></i>
    <p class="stats-list text-success fa-2x"><?php echo number_format($referrals_count); ?></p>
    <h4 class="text-muted">Referrals</h4>
  </div>
</div>
</div>

<div class="col-lg-3">
<div class="panel panel-default">
  <div class="panel-body text-center">
  <i class="fa fa-money fa-3x" aria-hidden="true"></i>
    <p class="stats-list text-success fa-2x"><?php echo "$".number_format(convert($redemptions_count)); ?></p>
    <h4 class="text-muted">Earned Total</h4>
  </div>
</div>
</div>

</div>

<h2 class="page-header">Recent News</h2>
<?php $get_latest_news = mysqli_query($conn, "SELECT * FROM `news` ORDER BY `news_date_added` DESC LIMIT 5") or die(mysqli_error($conn)); ?>
<?php if(mysqli_num_rows($get_latest_news) == 0) { ?>
<div class="alert alert-danger">No recent news has been added.</div>
<?php } else { ?>
<div class="alert alert-info">Keep up-to-date with the website.</div>
<table class="table table-condensed">
<tr>
    <th width="60%">Title</th>
    <th width="15%"></th>
    <th width="15%"></th>
</tr>
<?php while($news_row = mysqli_fetch_array($get_latest_news)) { ?>
   <tr>
   <td><?php echo truncate($news_row['news_title'],80); ?></td>
   <td><span class="help-block"><?php echo $news_row['news_date_added']; ?></span></td>
   <td><a class="btn btn-xs btn-primary" href="<?php echo $config['site_url']; ?>news/view/<?php echo $news_row['news_id']; ?>">View</a></td>  
   </tr>
<?php } ?>
</table>
<?php } ?>

<h2 class="page-header">Recent Activity</h2>
<?php $get_latest_offers = mysqli_query($conn, "SELECT * FROM `activity_history` WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."' ORDER BY `history_date` DESC LIMIT 25") or die(mysqli_error($conn)); ?>
<?php if(mysqli_num_rows($get_latest_offers) == 0) { ?>
<div class="alert alert-danger">No recent activity. Complete some offers, just click on the "Earn" navigation link.</div>
<?php } else { ?>
<ul class="list-group">
<?php while($row = mysqli_fetch_array($get_latest_offers)) { ?>
    <li class="list-group-item"><?php echo $row['history_text']; ?></li>
<?php } ?>
</ul>
<?php } ?>

</div>

<?php include_once("footer.php"); ?>