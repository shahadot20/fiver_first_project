<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include_once("header.php");

?>

<div class="col-lg-9">

<h2 class="page-header">Buy PTC Adpacks</h2>
<p>PTC Adpacks are packs that you can buy to create PTC ads.</p>

<?php
$get_adpacks = mysqli_query($conn, "SELECT * FROM ptc_adpacks") or die(mysqli_error($conn));

while($row = mysqli_fetch_array($get_adpacks)) {
?>
<div class="col-md-3">

<div class="panel panel-default">
<div class="panel-heading">
  <h3 class="panel-title"><i class="fa fa-certificate fa-panel" aria-hidden="true"></i> <?php echo $row['ptc_adpack_name']; ?></h3>
  </div>
  <div class="panel-body">
    <div class="points-balance"><?php echo number_format($row['ptc_adpack_points']); ?> <?php echo $config['site_currency']; ?>
</div>
  </div>
  <div class="panel-footer">
  <?php echo "$".$row['ptc_adpack_price']; ?> <a href="<?php echo $config['base_url']; ?>buy/adpack/<?php echo $row['ptc_adpack_id']; ?>" class="btn btn-xs btn-success pull-right">Buy via Paypal</a>
  </div>
</div>
</div>
<?php } ?>

</div>

<?php include_once("footer.php"); ?>