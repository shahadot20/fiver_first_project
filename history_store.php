<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/
session_start();


include_once("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}

$adjacents = 5;

    $query = mysqli_query($conn, "select COUNT(*) as num from `users_transactions` WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."'") or die(mysqli_error($conn));
    $total_pages = mysqli_fetch_array($query, MYSQLI_ASSOC);
    $total_pages = $total_pages['num'];

    $limit = $config['site_page_limit'];                                //how many items to show per page

    $page = abs((int) $_GET['page']);

    if($page) 
        $start = ($page - 1) * $limit;          //first item to display on this page
    else
        $start = 0;                             //if no page var is given, set start to 
    /* Get data. */
    $result = mysqli_query($conn, "select * from `users_transactions` WHERE `user_id`='".mysqli_real_escape_string($conn, $user['user_id'])."' ORDER BY date_added DESC LIMIT $start,$limit") or die(mysqli_error($conn));

    /* Setup page vars for display. */
    if ($page == 0) $page = 1;                  //if no page var is given, default to 1.
    $prev = $page - 1;                          //previous page is page - 1
    $next = $page + 1;                          //next page is page + 1
    $lastpage = ceil($total_pages/$limit);      //lastpage is = total pages / items per page, rounded up.
    $lpm1 = $lastpage - 1;                      //last page minus 1

    $pagination = "";
    if($lastpage > 1) {   
        $pagination .= "<ul class=\"pager\">";
        //previous button
        if ($page > 1) 
            $pagination.= "<li class=\"previous\"><a href=\"".$config['base_url']."history_store.php/$prev\">&laquo; previous</a></li>";
        else
            $pagination.= "<li class=\"previous disabled\"><a>&laquo; previous</a></li>"; 

        //next button
        if ($page < $lastpage) 
            $pagination.= "<li class=\"next\"><a href=\"".$config['base_url']."history_store.php/$next\">next &raquo;</a></li>";
        else
            $pagination.= "<li class=\"next disabled\"><a>next &raquo;</a></li>";
        $pagination.= "</ul>\n";       
    }

?>

<div class="col-lg-9">

<h2 class="page-header">Store History</h2>
<p>All your purchases will be displayed here.</p>

<?php if(mysqli_num_rows($result) == 0) { ?>
<div class="alert alert-danger">You haven't made any purchases yet.</div>
<?php } else { ?>
<table class="table">
<tr>
<th>Package</th>
<th>Price</th>
<th>Email</th>
<th>Date</th>
</tr>
<?php while($row = mysqli_fetch_assoc($result)) { ?>
<tr>
<td><?php echo $row['package']; ?></td>
<td><?php echo "$".$row['money']; ?></td>
<td><?php echo $row['paypal_email']; ?></td>
<td><?php echo $row['date_added']; ?></td>
</tr>
<?php } ?>
</table>
<?php } ?>

<?php echo $pagination; ?>

</div>

<?php include_once("footer.php"); ?>