<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/
session_start();


include_once("header.php");

// redirect user to login to access this page
if(!isset($user['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}

$pack_id = intval(str_clean(filter_var($_GET['pack_id'], FILTER_VALIDATE_INT)));

$get_adpack = mysqli_query($conn, "SELECT * FROM `ptc_adpacks` WHERE `ptc_adpack_id`='".mysqli_real_escape_string($conn, $pack_id)."' LIMIT 1");

if(mysqli_num_rows($get_adpack)) {

$row = mysqli_fetch_array($get_adpack);

?>

<div class="col-lg-9">

<h2 class="page-header">Buying: <?php echo $row['ptc_adpack_name']; ?> Adpack</h2>

<h3 class="page-header">Order Summary</h3>
<p>Please review the details of your order before making a purchase:</p>
<table class="table">
<tr>
<th>Adpack</th>
<th><?php echo $config['site_currency']; ?></th>
<th>Price</th>
</tr>
<tr>
<td><?php echo $row['ptc_adpack_name']; ?></td>
<td><?php echo number_format($row['ptc_adpack_points']); ?></td>
<td><?php echo "$".$row['ptc_adpack_price']; ?></td>
</tr>
</table>

<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
<input type="hidden" name="cmd" value="_xclick">
<input type="hidden" name="business" value="<?php echo $config['site_paypal_email']; ?>">
<input type="hidden" name="item_name" value="<?php echo $row['ptc_adpack_name']; ?>">
<input type="hidden" name="item_number" value="<?php echo $row['ptc_adpack_id']; ?>">
<input type="hidden" name="custom" value="<?php echo $user['user_id']; ?>">
<input type="hidden" name="amount" value="<?php echo $row['ptc_adpack_price']; ?>">
<input type="hidden" name="currency_code" value="USD">
<input type="hidden" name="no_note" value="1">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="rm" value="2">
<input type="hidden" name="return" value="<?php echo $config['base_url']; ?>">
<input type="hidden" name="cancel_return" value="<?php echo $config['base_url']; ?>store/ptc_adpacks">
<input type="hidden" name="notify_url" value="<?php echo $config['base_url']; ?>payments/paypal_ipn.php">
<input class="btn btn-lg btn-success btn-block" type="submit" name="submit" value="Buy Now with PayPal">
</form>
<div class="padding-down"></div>
<div class="clear"></div>

<?php } else { ?>
<div class="alert alert-danger">This adpack doesn't exist. Please try again.</div>
<?php } ?>

</div>

<?php include_once("footer.php"); ?>