<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include_once("includes/config.php");

if($user['banned'] > 0) {
session_destroy();
header("Location: ".$config['base_url']."");
exit;
}

if (strpos($_SERVER['PHP_SELF'], "header_ptc.php") !== false) { exit; }

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><?php echo $config['site_name']; ?></title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<link rel="canonical" href="<?php echo $config['base_url']; ?>" />
<link href="<?php echo $config['base_url']; ?>assets/css/bootstrap.<?php if(empty($user['site_theme']) || $user['site_theme'] == "") { echo $config['site_theme']; } else { echo $user['site_theme']; } ?>.min.css" rel="stylesheet">

<link href="<?php echo $config['base_url']; ?>assets/css/surf.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $config['base_url']; ?>assets/css/font-awesome.css" rel="stylesheet">

<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script><!-- jQuery always has to load first for Bootstrap -->
<script src="<?php echo $config['base_url']; ?>assets/js/bootstrap.min.js"></script>

<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>

<style>
body {
margin: 0;
padding: 0;
}

#iframe {
margin: 0;
padding: 0;
}
</style>
</head>

<body>