<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

error_reporting(1);
ini_set('display_errors', '1');

session_start();

include("../includes/config.php");

if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

if(isset($user['username']) || $user_id != $user['user_id']) { 

$user_id = filter_var($_POST['user_id'], FILTER_VALIDATE_INT);
$site_id = filter_var($_POST['site_id'], FILTER_VALIDATE_INT);
$timer = filter_var($_POST['timer'], FILTER_VALIDATE_INT);
$points = filter_var($_POST['points'], FILTER_VALIDATE_INT);

$check_exists = "SELECT * FROM `ptc_ads` WHERE `ptc_ad_id`='".mysqli_real_escape_string($conn, $site_id)."' AND `ptc_ad_active`='1'";
$result = mysqli_query($conn, $check_exists) or die(mysqli_error($conn));
$ptcad = mysqli_fetch_array($result);
$check = mysqli_num_rows($result);

$history_exists = "SELECT * FROM `ptc_ads_history` WHERE `ptc_ad_siteid`='".mysqli_real_escape_string($conn, $site_id)."' AND `ptc_ad_userid`='".mysqli_real_escape_string($conn, $user['user_id'])."'";
$result2 = mysqli_query($conn, $history_exists) or die(mysqli_error($conn));
$history_check = mysqli_num_rows($result2);


if(isset($user_id) && isset($site_id) && isset($timer) && $check > 0 && $history_check == 0) {

if($ptcad['ptc_ad_userid'] == $user['user_id']) {
echo "<div class=\"alert alert-danger\">You are viewing your own ad.</div>";
} else if($ptcad['ptc_ad_userid'] != $user['user_id']) {

$sql1 = "UPDATE `ptc_ads` SET `ptc_ad_total_views`=`ptc_ad_total_views`+'1', `ptc_ad_today_views`=`ptc_ad_today_views`+'1' WHERE `ptc_ad_id`='".$ptcad['ptc_ad_id']."'";
mysqli_query($conn, $sql1) or die(mysqli_error($conn));

$insert_sql = "INSERT INTO `ptc_ads_history` (ptc_ad_id, ptc_ad_siteid, ptc_ad_userid, ptc_ad_points, ptc_ad_date_viewed, ptc_ad_ip_address) VALUES(NULL, '".mysqli_real_escape_string($conn, $site_id)."', '".mysqli_real_escape_string($conn, $user_id)."', '".mysqli_real_escape_string($conn, $ptcad['ptc_ad_points'])."', NOW(), '".mysqli_real_escape_string($conn, get_user_ip())."')";
$result = mysqli_query($conn,$insert_sql) or die(mysqli_error($conn));


$sql2 = "update users set points=points+'".$ptcad['ptc_ad_points']."' where user_id='".$user['user_id']."'";
mysqli_query($conn, $sql2) or die(mysqli_error($conn));

$sql3 = "update users set advertiser_points=advertiser_points-'".$ptcad['ptc_ad_points']."' where user_id='".$ptcad['ptc_ad_userid']."'";
mysqli_query($conn, $sql3) or die(mysqli_error($conn));

echo "<div class=\"alert alert-success\">You've been credited <strong>".$ptcad['ptc_ad_points']."</strong> ".$config['site_currency']."!</div>";
} 
} else {
echo "<div class=\"alert alert-danger\">Invalid click. Please try another ad.</div>";
}
} else {
echo "<div class=\"alert alert-danger\">You need to be logged in to earn points.</div>";
}
}

 ?>