<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include_once("header.php");


// redirect user to dashboard if logged in
if(isset($user['username'])){
    echo "<script>document.location.href='".$config['base_url']."dashboard.php'</script>";
    exit;
}

$total_users = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM `users`"));
$total_rewards = mysqli_num_rows(mysqli_query($conn, "SELECT * FROM `redemption_history`"));

$get_earned = mysqli_query($conn, "SELECT SUM(history_points) FROM activity_history") or die(mysql_error());
while($row = mysqli_fetch_array($get_earned)){
$total_points = $row['SUM(history_points)'];
}
?>

<div class="sbucks-header" tabindex="-1">
      <div class="container-fluid">
        <h1>Get Paid To Complete Offers, Surveys, and More!</h1>
        <p>We offer many offerwalls, and networks to help you earn money online. It's fast, free and easy!</p>
      </div>
</div>
    
<div class="col-lg-12">
<div class="container">
<div class="row row-centered">

<h2 class="page-header">3 Easy Steps</h2>

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">
          <i class="fa fa-user fa-icon-lg"></i>
          <h2>Signup</h2>

        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <i class="fa fa-mouse-pointer fa-icon-lg"></i>
          <h2>Earn</h2>
         
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <i class="fa fa-money fa-icon-lg"></i>
          <h2>Redeem</h2>
          
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->

</div>
</div>

<div class="pull-down-container"></div>

<div class="container">
<div class="row row-centered">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">
          
          <h2><?php echo $total_users; ?> Users</h2>

        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          
          <h2><?php echo "$".convert(number_format($total_points)); ?> Earned</h2>
         
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          
          <h2><?php echo $total_rewards; ?> Rewards</h2>
          
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->

</div>
</div>

</div>


<div class="pull-down-container"></div>

<div class="ssignup-header" tabindex="-1">
      <div class="container-fluid">
              <h3 class="padding-bottom-30">Creating an account is free! Start earning money online today!</h3>

<div class="container">
<div class="row row-centered">

<div class="col-md-6">
<a class="btn btn-lg btn-block btn-success" href="<?php echo $config['base_url']; ?>signup.php">Signup</a>
</div>

<div class="col-md-6">
<a class="btn btn-lg btn-block btn-primary" href="<?php echo $config['base_url']; ?>login.php">Login</a>
</div>

</div>
</div>

</div>
</div>

</div>


<?php include_once("footer.php"); ?>