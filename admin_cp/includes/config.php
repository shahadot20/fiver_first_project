<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', '0');

$servername = "localhost";
$dbusername = "root";
$dbpassword = "";
$dbname = "dbdata";

// Create connection
$conn = new mysqli($servername, $dbusername, $dbpassword, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


mysqli_query($conn, "SET NAMES utf8");


include_once("functions.php");

if(isset($_SESSION['admin_username'])){
   $admin = mysqli_fetch_array(mysqli_query($conn, "SELECT *,UNIX_TIMESTAMP(`online`) AS `online` FROM `admins` WHERE `username`='".mysqli_real_escape_string($conn, $_SESSION['admin_username'])."' LIMIT 1"));
}
  
$config = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM `config` WHERE `site_id`='1' LIMIT 1"));

?>