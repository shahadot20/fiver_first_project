<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

$get_username = str_clean($_GET['admin_username']);

$get_username = mysqli_query($conn, "SELECT * FROM `admins` WHERE `admin_username`='".mysqli_real_escape_string($conn, $get_username)."' LIMIT 1") or die(mysqli_error($conn));

if(empty($get_username) || $get_username == "" || !$get_username) {
$error = "You never selected an admin to unban.";
} else if(mysqli_num_rows($get_username) == 0) {
$error = "This admin doesn't exist in the database.";
} else {

mysqli_query($conn, "DELETE FROM `admin` WHERE `admin_username`='".mysqli_real_escape_string($conn, $get_username)."'") or die(mysqli_error($conn));

$success = "This admin has been deleted.";

?>

<div class="container">

<?php if(isset($error)) { ?><div class="alert alert-danger"><?php echo $error; ?></div><?php } ?>
<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<?php } ?>

</div>

<?php include("footer.php"); ?>