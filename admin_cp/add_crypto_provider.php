<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

if(isset($_POST['add_payment_provider'])) {

$provider_name = str_clean($_POST['provider_name']);
$provider_seo_name = url_slug($_POST['provider_name']);
$provider_description = str_clean($_POST['provider_description']);
$provider_minimum =  str_clean($_POST['provider_minimum']);
$provider_fee = str_clean($_POST['provider_fee']);
$provider_referral_url = str_clean($_POST['provider_referral_url']);

$provider_active =  str_clean($_POST['provider_active']);

$provider_sql = mysqli_query($conn, "SELECT `provider_name` FROM `cryptocurrency_providers` WHERE `provider_name`='".mysqli_real_escape_string($conn, $provider_name)."'") or die(mysqli_error($conn));
$provider_exists = mysqli_num_rows($provider_sql);

if(empty($provider_name)) {
$error = "<li>You left the payment provider name blank.</li>";
}

if(empty($provider_description)) {
$error .= "<li>You left the payment provider description blank.</li>";
}

if($provider_active == "") {
$error .= "<li>You need to select if payment provider is active or not.</li>";
}

if(strlen($provider_description) > 500) {
$error .= "<li>Don't go past 500 characters.</li>";
}

if(!is_numeric($provider_active)) {
$error .= "<li>You need to enter 0(disabled) or 1(active).</li>";
}

if(!is_numeric($provider_fee) && $provider_fee != "0") {
$error .= "<li>Please enter only numbers for the provider fee.</li>";
}

if(!is_numeric($provider_minimum)) {
$error .= "<li>Please enter only numbers for the provider minimum.</li>";
}

if ($provider_exists > 0)  {
$error .= "<li>".$provider_name." is already in the database!</li>";
}

if($provider_active != "0" && $provider_active != "1") {
$error .= "<li>Is ".$provider_name." active or disabled?</li>";

}

if(!$error) {
mysqli_query($conn, "INSERT INTO `cryptocurrency_providers`(provider_id, provider_name,provider_seo_name,provider_description,provider_fee,provider_minimum,provider_referral_url,provider_active) values(NULL, 
'".mysqli_real_escape_string($conn, $provider_name)."',
'".mysqli_real_escape_string($conn, $provider_seo_name)."',
'".mysqli_real_escape_string($conn, $provider_description)."',
'".mysqli_real_escape_string($conn, $provider_fee)."',
'".mysqli_real_escape_string($conn, $provider_minimum)."',
'".mysqli_real_escape_string($conn, $provider_referral_url)."',
'".mysqli_real_escape_string($conn, $provider_active)."'
)") or die(mysqli_error($conn));
    $success = "Cryptocurrency provider <strong>".$provider_name."</strong> has been successfully added!";
}
}
?>

<div class="container">
<div class="row">

<?php if(isset($error)) { echo "<div class=\"alert alert-error\"><h3>Errors:</h3><ul>".$error."</ul></div>"; } ?>
<?php if(isset($success)) { echo "<div class=\"alert alert-success\">".$success."</div>"; } ?>

<form class="form-horizontal" role="form" method="post">

<h1>Add a Payment Provider</h1>

<div class="form-group">
<label class="control-label col-sm-2" for="provider_name">Cryptocurrency Provider Name</label>
<div class="col-sm-10">
<input class="form-control" name="provider_name" id="provider_name" type="text" value="<?php if(isset($provider_name)) { echo $provider_name; } else { echo $provider_name; } ?>" required/>
<span class="help-block">Gotta have a payment provider name.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="provider_description">Cryptocurrency Provider Description</label>
<div class="col-sm-10">
<input class="form-control" name="provider_description" id="provider_description" type="text" value="<?php if(isset($provider_description)) { echo $provider_description; } else { echo $provider_description; }?>" required/>
<span class="help-block">Make sure your payment provider description is under 200 characters</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="provider_referral_url">Cryptocurrency Provider Referral URL</label>
<div class="col-sm-10">
<input class="form-control" name="provider_referral_url" id="provider_referral_url" type="text" value="<?php if(isset($provider_referral_url)) { echo $provider_referral_url; } else { echo $provider_referral_url; }?>" />
<span class="help-block">Insert your payment provider referral link, leave blank if there is none.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="provider_fee">Cryptocurrency Provider Fee</label>
<div class="col-sm-10">
<input class="form-control" name="provider_fee" id="provider_fee" type="text" value="<?php if(isset($provider_fee)) { echo $provider_fee; } else { echo $provider_fee; }?>" />
<span class="help-block">Enter "0" for no fee, otherwise set your fee - don't make it too high though!</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="provider_minimum">Cryptocurrency Provider Minimum</label>
<div class="col-sm-10">
<input class="form-control" name="provider_minimum" id="provider_minimum" type="text" value="<?php if(isset($provider_minimum)) { echo $provider_minimum; } else { echo $provider_minimum; }?>" required/>
<span class="help-block">Set a point minimum that users can redeem with this payment provider. If no minimum, set to "0".</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="provider_active">Cryptocurrency Provider Active</label>
<div class="col-sm-10">
<select class="form-control" name="provider_active" id="provider_active">
<option value="0">Disabled</option>
<option value="1">Active</option>
</select>
<span class="help-block">Enable or deactivate a Payment Provider</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="add_payment_provider" type="submit" value="Add Payment Provider"/>
</div>
</div>
</form>

</div>
</div>

<?php include("footer.php"); ?>