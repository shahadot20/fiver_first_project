<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include('header.php');

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['site_url']."admin_cp/login.php'</script>";
    exit;
}

if(isset($_GET['query'])) {

$query = str_clean($_GET['query']);

if(empty($query)) {
$error = "<li>You left the search query blank.</li>";

} else {
    
    $query = str_clean($_GET['query']);

$adjacents = 5;

    $query2 = mysqli_query($conn, "select COUNT(*) as num FROM `users` WHERE (`username` LIKE '%".mysqli_real_escape_string($conn, $query)."%') or (`email` LIKE '%".mysqli_real_escape_string($conn, $query)."%') or (`ip` = '".mysqli_real_escape_string($conn, $query)."')") or die(mysqli_error($conn));
    $total_pages = mysqli_fetch_array($query2);
    $total_pages = $total_pages['num'];

    $limit = $config['site_page_limit'];                                //how many items to show per page

    $page = abs((int) $_GET['page']);

    if($page) 
        $start = ($page - 1) * $limit;          //first item to display on this page
    else
        $start = 0;                             //if no page var is given, set start to 
    /* Get data. */
    $result = mysqli_query($conn, "SELECT * FROM `users` WHERE (`username` LIKE '%".mysqli_real_escape_string($conn, $query)."%') or (`email` LIKE '%".mysqli_real_escape_string($conn, $query)."%') or (`ip` = '".mysqli_real_escape_string($conn, $query)."') LIMIT $start,$limit") or die(mysqli_error($conn));

    /* Setup page vars for display. */
    if ($page == 0) $page = 1;                  //if no page var is given, default to 1.
    $prev = $page - 1;                          //previous page is page - 1
    $next = $page + 1;                          //next page is page + 1
    $lastpage = ceil($total_pages/$limit);      //lastpage is = total pages / items per page, rounded up.
    $lpm1 = $lastpage - 1;                      //last page minus 1

    $pagination = "";
    if($lastpage > 1) {   
        $pagination .= "<ul class=\"pager\">";
        //previous button
        if ($page > 1) 
            $pagination.= "<li class=\"previous\"><a href=\"".$config['site_url']."admin_cp/users_search.php?page=$prev\">&laquo; previous</a></li>";
        else
            $pagination.= "<li class=\"previous disabled\"><a>&laquo; previous</a></li>"; 

        //next button
        if ($page < $lastpage) 
            $pagination.= "<li class=\"next\"><a href=\"".$config['site_url']."admin_cp/users_search.php?page=$next\">next &raquo;</a></li>";
        else
            $pagination.= "<li class=\"next disabled\"><a>next &raquo;</a></li>";
        $pagination.= "</ul>\n";       
    }
}
}


?>

<div class="container">

<h2 class="page-header">Users Search</h2>

<p>Search of all registered users.</p>

<div class="container">
<div class="row">
    <div class="col-lg-11">
<form class="form-horizontal" role="form" method="GET">

<div class="form-group">
  <div class="input-group">
    <input class="form-control input-md" name="query" type="text" placeholder="Enter a username, email, or IP address">
    <span class="input-group-btn">
      <button class="btn btn-default btn-md" type="submit">Search</button>
    </span>
  </div>
</div>

</form>
</div>
</div>
</div>

<?php if($query) { ?>

<div class="alert alert-info">There are <?php $results = mysqli_num_rows($result); echo number_format($results); ?> results.</div>

<table class="table">
<tr>
<th>User ID</th>
<th>Username</th>
<th>Points</th>
<th>Registered</th>
<th>Last Online</th>
<th>Actions</th>
</tr>
<?php while($row = mysqli_fetch_assoc($result)) {

$is_user_cban = mysqli_query($conn, "SELECT * FROM `chat_bans` WHERE `ban_username`='".mysqli_real_escape_string($conn, $row['username'])."'") or die(mysqli_error($conn));
if(mysqli_num_rows($is_user_cban) == 1) {
$chat_unban = "<a class=\"btn btn-xs btn-primary\" href='".$config['site_url']."admin_cp/chat_ban_delete.php?ban_username=".$row['username']."'>Unban Chat User</a>";
}
?>
<tr>
<td><?php echo $row['user_id']; ?></td>
<td><?php echo $row['username']; ?></td>
<td><?php echo $row['points']; ?></td>
<td><?php echo $row['signup']; ?></td>
<td><?php echo $row['online']; ?></td>
<td><a  class="btn btn-xs btn-primary" href="<?php echo $config['site_url']; ?>admin_cp/users_edit.php?id=<?php echo $row['user_id']; ?>">Edit</a>&nbsp;<a class="btn btn-xs btn-primary" href="<?php echo $config['site_url']; ?>admin_cp/add_ip_ban.php?ban_ip_address=<?php echo $row['ip']; ?>">Ban User IP</a>&nbsp;<a class="btn btn-xs btn-primary" href="<?php echo $config['site_url']; ?>admin_cp/add_chat_ban.php?ban_username=<?php echo $row['username']; ?>">Chat Ban User</a> <?php echo $chat_unban; ?></td>
</tr>
<?php } ?>
</table>

<?php echo $pagination; ?>

<?php } ?>

</div>

<?php include_once("footer.php"); ?>