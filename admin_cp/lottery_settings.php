<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['site_url']."admin_cp/login.php'</script>";
    exit;
}


// get the settings from `config` table
$get_settings = mysqli_query($conn, "SELECT * FROM `config` WHERE `site_id`='1' LIMIT 1") or die(mysqli_error($conn));


if(isset($_POST['edit_lottery_settings'])) {

$lottery_limit = intval(str_clean($_POST['lottery_limit']));
$lottery_minimum = intval(str_clean($_POST['lottery_minimum']));
$lottery_fee = intval(str_clean($_POST['lottery_fee']));

if(empty($lottery_limit)) {
$error = "You left the site name blank.<br />";
}

if(empty($lottery_minimum)) {
$error .= "You left the site description blank.<br />";
}

if(empty($lottery_fee)) {
$error .= "You left the site URL blank.<br />";
}

if(!is_numeric($lottery_limit)) {
$error .= "The lottery limit can only contain numbers.<br />";
}

if(!is_numeric($lottery_minimum)) {
$error .= "The lottery minimum can only contain numbers.<br />";
}

if(!is_numeric($lottery_fee)) {
$error .= "The lottery fee can only contain numbers.<br />";
}

if(!$error) {
mysqli_query($conn, "UPDATE `config` SET 
`lottery_limit`='".mysqli_real_escape_string($conn, $lottery_limit)."',
`lottery_minimum`='".mysqli_real_escape_string($conn, $lottery_minimum)."',
`lottery_fee`='".mysqli_real_escape_string($conn, $lottery_fee)."'
WHERE `site_id`='1'") or die(mysqli_error($conn));
    $success = "Lottery settings successfully updated!";
}

}

?>

<div class="container">

<?php if(isset($error)) { ?><div class="alert alert-danger"><?php echo $error; ?></div><?php } ?>
<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<?php while($row = mysqli_fetch_array($get_settings)) { ?>

<form class="form-horizontal" role="form" method="post">

<h1>Editing Lottery Settings</h1>

<div class="form-group">
<label class="control-label col-sm-2" for="lottery_limit">Lottery Limit</label>
<div class="col-sm-10">
<input class="form-control" name="lottery_limit" id="lottery_limit" type="text" value="<?php if(isset($row['lottery_limit'])) { echo $row['lottery_limit']; } else { echo $lottery_limit; } ?>"/>
<span class="help-block">How many times can a user enter the lottery?</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="lottery_minimum">Lottery Minimum</label>
<div class="col-sm-10">
<input class="form-control" name="lottery_minimum" id="lottery_minimum" type="text" value="<?php if(isset($row['lottery_minimum'])) { echo $row['lottery_minimum']; } else { echo $lottery_minimum; }?>" required/>
<span class="help-block">How many points minimum can the user pay for lottery ticket?</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="lottery_fee">Lottery Fee</label>
<div class="col-sm-10">
<input class="form-control" name="lottery_fee" id="lottery_fee" type="text" value="<?php if(isset($row['lottery_fee'])) { echo $row['lottery_fee']; } else { echo $lottery_fee; } ?>" required/>
<span class="help-block">Do you want to take a percentage of the lottery ticket price?</span>
</div>
</div>


<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="edit_lottery_settings" type="submit" value="Edit Lottery Settings"/>
</div>
</div>
</form>

<?php } ?>

</div>

<?php include("footer.php"); ?>