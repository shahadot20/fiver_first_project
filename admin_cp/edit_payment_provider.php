<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

$edit_id = str_clean($_GET['c_id']);

$get_provider = mysqli_query($conn, "SELECT * FROM `payment_providers` WHERE `provider_id`='".mysqli_real_escape_string($conn, $edit_id)."' LIMIT 1") or die(mysqli_error($conn));

if(empty($edit_id) || $edit_id == "" || !$edit_id) {
$error = "You never selected a provider to edit.";
} else if(mysqli_num_rows($get_provider) == 0) {
$error = "This payment provider doesn't exist.";
} else { 

if(isset($_POST['edit_provider'])) {

$provider_name = str_clean($_POST['provider_name']);
$provider_description = str_clean($_POST['provider_description']);
$provider_fee = str_clean($_POST['provider_fee']);
$provider_seo_name = url_slug($provider_name);
$provider_active =  str_clean($_POST['provider_active']);
$provider_referral_url = str_clean($_POST['provider_referral_url']);
$provider_minimum = str_clean($_POST['provider_minimum']);


$provider_sql = mysqli_query($conn, "SELECT `provider_name` FROM `payment_providers` WHERE `provider_name`='".mysqli_real_escape_string($conn, $provider_name)."'") or die(mysqli_error($conn));
$provider_exists = mysqli_num_rows($provider_sql);

if(empty($provider_name)) {
$error = "You left the payment provider name blank.";
}

if(empty($provider_description)) {
$error .= "You left the payment provider description blank.";
}

if($provider_active == "") {
$error .= "You need to select if payment provider is active or not.";
}

if(strlen($provider_description) > 200) {
$error .= "Don't go past 200 characters.";
}

if(empty($provider_fee) && $provider_fee != "0") {
$error .= "You need to enter 0 to disable, otherwise enter a value higher than 0 to add a fee on redemptions.</li>";
}

if(!is_numeric($provider_fee)) {
$error .= "Please enter only numbers for the provider minimum";
}

if(!is_numeric($provider_active)) {
$error .= "You need to enter 0(disabled) or 1(active).</li>";
}

if($provider_active != "0" && $provider_active != "1") {
$error .= "Is ".$provider_name." active or disabled?";

}

if(!$error) {
mysqli_query($conn, "UPDATE `payment_providers` SET 
`provider_name`='".mysqli_real_escape_string($conn, $provider_name)."',
`provider_description`='".mysqli_real_escape_string($conn, $provider_description)."',
`provider_fee`='".mysqli_real_escape_string($conn, $provider_fee)."',
`provider_minimum`='".mysqli_real_escape_string($conn, $provider_minimum)."',
`provider_referral_url`='".mysqli_real_escape_string($conn, $provider_referral_url)."',
`provider_active`='".mysqli_real_escape_string($conn, $provider_active)."' 
WHERE `provider_id`='".mysqli_real_escape_string($conn, $edit_id)."'") or die(mysqli_error($conn));
    $success = "Payment provider <strong>".$provider_name."</strong> has been successfully updated!";
}

}

if(isset($_POST['delete_provider'])) {
mysqli_query($conn, "DELETE FROM `payment_providers` WHERE `provider_id`='".mysqli_real_escape_string($conn, $edit_id)."'") or die(mysqli_error($conn));
$redirect = $config['base_url']."admin_cp/payment_providers.php?delete_payment_provider=1";
echo "<script>document.location.href='".$redirect."'</script>";
exit;
}
} 
?>

<div class="container">

<?php if(isset($error)) { ?><div class="alert alert-danger"><?php echo $error; ?></div><?php } ?>
<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<?php while($row = mysqli_fetch_array($get_provider)) { ?>

<form class="form-horizontal" role="form" method="post">

<h1 class="page-header">Editing Payment Provider: <?php echo $row['provider_name']; ?></h1>

<div class="form-group">
<label class="control-label col-sm-2" for="provider_name">Provider Name</label>
<div class="col-sm-10">
<input class="form-control" name="provider_name" id="provider_name" type="text" value="<?php if(isset($row['provider_name'])) { echo $row['provider_name']; } else { echo $provider_name; }?>" required/>
<span class="help-block">Gotta have a provider name.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="provider_description">Provider Description</label>
<div class="col-sm-10">
<input class="form-control" name="provider_description" id="provider_description" type="text" value="<?php if(isset($row['provider_description'])) { echo $row['provider_description']; } else { echo $provider_description; }?>" required/>
<span class="help-block">Make sure your provider description is under 200 characters.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="provider_referral_url">Provider Referral URL</label>
<div class="col-sm-10">
<input class="form-control" name="provider_referral_url" id="provider_referral_url" type="text" value="<?php if(isset($row['provider_referral_url'])) { echo $row['provider_referral_url']; } else { echo $provider_referral_url; }?>" />
<span class="help-block">If your payment provider allows referral, insert that link here otherwise leave blank.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="provider_fee">Provider Fee</label>
<div class="col-sm-10">
<input class="form-control" name="provider_fee" id="provider_fee" type="text" value="<?php if(isset($row['provider_fee'])) { echo $row['provider_fee']; } else { echo $provider_fee; } ?>" />
<span class="help-block">Enter "0" to disable, otherwise set a fee as a percentage. Example: 10 for 10%</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="provider_minimum">Provider Minimum</label>
<div class="col-sm-10">
<input class="form-control" name="provider_minimum" id="provider_minimum" type="text" value="<?php if(isset($row['provider_minimum'])) { echo $row['provider_minimum']; } else { echo $provider_minimum; } ?>" required/>
<span class="help-block">Set a point minimum that users can redeem with this payment provider. If no minimum, set to "0".</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="provider_active">Provider Active</label>
<div class="col-sm-10">
<select class="form-control" name="provider_active" id="provider_active">
<option value="0" <?php if($row['provider_active'] == 0) { ?>selected<?php } ?>>Disabled</option>
<option value="1" <?php if($row['provider_active'] == 1) { ?>selected<?php } ?>>Active</option>
</select>
<span class="help-block">This provider is currently set to: <?php if($row['provider_active'] == 1) { echo "<strong class=\"text text-success\">Active</strong>"; } else { echo "<strong class=\"text text-danger\"><i>Disabled</i></strong>"; } ?></span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="edit_provider" type="submit" value="Edit Provider"/>
<input class="btn btn-danger pull-right" name="delete_provider" type="submit" value="Delete Provider"/>
</div>
</div>
</form>

<?php
}
?>


</div>

<?php include("footer.php"); ?>