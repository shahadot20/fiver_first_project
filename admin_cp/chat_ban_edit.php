<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

$get_ban_id = str_clean($_GET['id']);

$get_ban = mysqli_query($conn, "SELECT * FROM `chat_bans` WHERE `ban_id`='".mysqli_real_escape_string($conn, $get_ban_id)."' LIMIT 1") or die(mysqli_error($conn));

if(empty($get_ban_id) || $get_ban_id == "" || !$get_ban_id) {
$error = "You never selected a chat ban to edit.";
} else if(mysqli_num_rows($get_ban) == 0) {
$error = "No record of this ban entry.";
} else { 

if(isset($_POST['edit_ban'])) {

$username = str_clean($_POST['ban_username']);
$banned_reason = str_clean($_POST['ban_reason']);

if(empty($username)) {
$error .= "You left the username blank.";
}

if(empty($banned_reason)) {
$error .= "You left the ban reason blank.";
}

if(!$error) {
mysqli_query($conn, "UPDATE `chat_bans` SET 
`ban_username`='".mysqli_real_escape_string($conn, $username)."',
`ban_reason`='".mysqli_real_escape_string($conn, $banned_reason)."'
 WHERE `ban_id`='".mysqli_real_escape_string($conn, $get_ban_id)."' LIMIT 1") or die(mysqli_error($conn));
    $success = "Ban for <strong>".$username."</strong> has been successfully updated!";
}

}

}
?>

<div class="container">

<?php if(isset($error)) { ?><div class="alert alert-danger"><?php echo $error; ?></div><?php } ?>
<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<?php while($row = mysqli_fetch_array($get_ban)) { ?>

<form class="form-horizontal" role="form" method="post">

<h1 class="page-header">Editing Ban: <?php echo $row['ban_username']; ?></h1>

<div class="form-group">
<label class="control-label col-sm-2" for="ip">Username</label>
<div class="col-sm-10">
<input class="form-control" name="ban_username" id="ban_username" type="text" value="<?php if(isset($row['ban_username'])) { echo $row['ban_username']; } ?>" required/>
<span class="help-block">Username that was chat banned.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ip">Chat Ban Reason</label>
<div class="col-sm-10">
<input class="form-control" name="ban_reason" id="ban_reason" type="text" value="<?php if(isset($row['ban_reason'])) { echo $row['ban_reason']; } ?>" required/>
<span class="help-block">Gotta have a chat ban reason.</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="edit_ban" type="submit" value="Edit Chat Ban"/>
</div>
</div>
</form>

<?php } ?>

</div>

<?php include("footer.php"); ?>