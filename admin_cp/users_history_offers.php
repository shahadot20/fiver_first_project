<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include('header.php');

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['site_url']."admin_cp/login.php'</script>";
    exit;
}

$adjacents = 5;

    $query = mysqli_query($conn, "select COUNT(*) as num from `activity_history`") or die(mysqli_error($conn));
    $total_pages = mysqli_fetch_array($query, MYSQLI_ASSOC);
    $total_pages = $total_pages['num'];

    $limit = $config['site_page_limit'];                                //how many items to show per page

    $page = abs((int) $_GET['page']);

    if($page) 
        $start = ($page - 1) * $limit;          //first item to display on this page
    else
        $start = 0;                             //if no page var is given, set start to 
    /* Get data. */
    $result = mysqli_query($conn, "select * from `activity_history` ORDER BY history_id DESC LIMIT $start,$limit") or die(mysqli_error($conn));

    /* Setup page vars for display. */
    if ($page == 0) $page = 1;                  //if no page var is given, default to 1.
    $prev = $page - 1;                          //previous page is page - 1
    $next = $page + 1;                          //next page is page + 1
    $lastpage = ceil($total_pages/$limit);      //lastpage is = total pages / items per page, rounded up.
    $lpm1 = $lastpage - 1;                      //last page minus 1

    $pagination = "";
    if($lastpage > 1) {   
        $pagination .= "<ul class=\"pager\">";
        //previous button
        if ($page > 1) 
            $pagination.= "<li class=\"previous\"><a href=\"".$config['site_url']."admin_cp/users_history_offers.php?page=$prev\">&laquo; previous</a></li>";
        else
            $pagination.= "<li class=\"previous disabled\"><a>&laquo; previous</a></li>"; 

        //next button
        if ($page < $lastpage) 
            $pagination.= "<li class=\"next\"><a href=\"".$config['site_url']."admin_cp/users_history_offers.php?page=$next\">next &raquo;</a></li>";
        else
            $pagination.= "<li class=\"next disabled\"><a>next &raquo;</a></li>";
        $pagination.= "</ul>\n";       
    }

?>

<div class="container">

<h2 class="page-header">Offer History</h2>
<p>List of all offer history.</p>

<?php if(mysqli_num_rows($result) == 0) { ?>
<div class="alert alert-danger">There are currently no completed offers.</div>
<?php } else { ?>
<table class="table">
<tr>
<th>User ID</th>
<th>Type</th>
<th>Provider</th>
<th>Text</th>
<th>Points</th>
<th>Date</th>
</tr>
<?php while($row = mysqli_fetch_assoc($result)) { ?>
<tr>
<td><?php echo $row['user_id']; ?></td>
<td><?php echo $row['history_type']; ?></td>
<td><?php echo $row['history_provider']; ?></td>
<td><?php echo $row['history_text']; ?></td>
<td><?php echo $row['history_points']; ?></td>
<td><?php echo $row['history_date']; ?></td>
</tr>
<?php } ?>
</table>
<?php } ?>

<?php echo $pagination; ?>

</div>

<?php include_once("footer.php"); ?>