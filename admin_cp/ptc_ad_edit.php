<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['site_url']."admin_cp/login.php'</script>";
    exit;
}

$edit_id = intval(str_clean($_GET['id']));

$get_ad = mysqli_query($conn, "SELECT * FROM `ptc_ads` WHERE `ptc_ad_id`='".mysqli_real_escape_string($conn, $edit_id)."' LIMIT 1") or die(mysqli_error($conn));

if(empty($edit_id) || $edit_id == "" || !$edit_id) {
$error = "You never selected a PTC ad to edit.";
} else if(mysqli_num_rows($get_ad) == 0) {
$error = "This PTC ad doesn't exist.";
} else { 

if(isset($_POST['edit_ptc_ad'])) {

$title = str_clean($_POST['ptc_ad_title']);
$description = str_clean($_POST['ptc_ad_description']);
$points = str_clean($_POST['ptc_ad_points']);
$url = str_clean($_POST['ptc_ad_url']);
$timer = intval(str_clean($_POST['ptc_ad_timer']));
$active = str_clean($_POST['ptc_ad_active']);

$get_ad_sql = mysqli_query($conn, "SELECT * FROM `ptc_ads` WHERE `ptc_ad_id`='".mysqli_real_escape_string($conn, $edit_id)."'") or die(mysqli_error($conn));
$ad_exists = mysqli_num_rows($get_user_sql);

if(empty($title)) {
$error .= "<li>You left the PTC Ad title blank.</li>";
}

if(empty($title) || $title == "") {
$error .= "<li>You never entered a title for your PTC Ad.</li>";
}

if(strlen($title) > 60) {
$error .= "<li>The PTC ad title character maximum is 60 characters.</li>";
}

if(empty($description) || $description == "") {
$error .= "<li>You never entered a description for your PTC Ad.</li>";
}

if(strlen($description) > 140) {
$error .= "<li>The PTC ad description character maximum is 140 characters.</li>";
}

if(empty($url) || $url == "") {
$error .= "<li>You never entered a URL for your PTC Ad.</li>";
}

if(!preg_match('/^(http|https):\/\/[a-z0-9_]+([\-\.]{1}[a-z_0-9]+)*\.[_a-z]{2,5}'.'((:[0-9]{1,5})?\/.*)?$/i', $url)){
$error .= "<li>You need to add a valid website URL.</li>";
}

if(substr($url, -4) == '.exe') {
$error .= "<li>This URL is not allowed, don't add .exe files.</li>";
}

if($points > 1) {
$error .= "<li>The maximum CPC is 1.</li>";
}

if($points < 0.001) {
$error .= "<li>The minimum CPC is 0.001.</li>";
}

if(!is_numeric($points)) { 
$error .= "<li>Points must be a valid number.</li>";
}

if(!is_numeric($timer)) {
$error .= "<li>Timer must be a valid number.</li>";
}

if($active != "0" && $active != "1") {
$error .= "<li>Is this PTC active or paused?</li>";
}

if(!$error) {
mysqli_query($conn, "UPDATE `ptc_ads` SET 
`ptc_ad_title`='".mysqli_real_escape_string($conn, $title)."',
`ptc_ad_description`='".mysqli_real_escape_string($conn, $description)."',
`ptc_ad_points`='".mysqli_real_escape_string($conn, $points)."',
`ptc_ad_url`='".mysqli_real_escape_string($conn, $url)."',
`ptc_ad_timer`='".mysqli_real_escape_string($conn, $timer)."',
`ptc_ad_active`='".mysqli_real_escape_string($conn, $active)."'
 WHERE `ptc_ad_id`='".mysqli_real_escape_string($conn, $edit_id)."' LIMIT 1") or die(mysqli_error($conn));
    $success = "PTC Ad <strong>".$title."</strong> has been successfully updated!";
}

}

}
?>

<div class="container">

<?php if(isset($error)) { ?><div class="alert alert-danger"><h2 class="no-top-padding">Errors: </h2><ul><?php echo $error; ?></ul></div><?php } ?>
<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<?php while($row = mysqli_fetch_array($get_ad)) { ?>

<form class="form-horizontal" role="form" method="post">

<h1 class="page-header">Editing PTC Ad: <?php echo $row['ptc_ad_title']; ?></h1>

<div class="form-group">
<label class="control-label col-sm-2" for="ptc_ad_title">Title</label>
<div class="col-sm-10">
<input class="form-control" name="ptc_ad_title" id="ptc_ad_title" type="text" value="<?php if(isset($row['ptc_ad_title'])) { echo $row['ptc_ad_title']; } ?>" required/>
<span class="help-block">Update PTC ad title?</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ptc_ad_description">Description</label>
<div class="col-sm-10">
<textarea class="form-control" name="ptc_ad_description" id="ptc_ad_description" rows="3" type="text" required><?php if(isset($row['ptc_ad_description'])) { echo $row['ptc_ad_description']; } ?></textarea>
<span class="help-block">Update PTC ad description?</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ptc_ad_points">Points (CPC)</label>
<div class="col-sm-10">
<input class="form-control" name="ptc_ad_points" id="ptc_ad_points" type="text" value="<?php if(isset($row['ptc_ad_points'])) { echo $row['ptc_ad_points']; } ?>" />
<span class="help-block">Update PTC ad point CPC?</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ptc_ad_url">URL</label>
<div class="col-sm-10">
<input class="form-control" name="ptc_ad_url" id="ptc_ad_url" type="text" value="<?php if(isset($row['ptc_ad_url'])) { echo $row['ptc_ad_url']; } ?>" />
<span class="help-block">Update PTC ad URL?</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ptc_ad_timer">Timer</label>
<div class="col-sm-10">
<input class="form-control" name="ptc_ad_timer" id="ptc_ad_timer" type="number" value="<?php if(isset($row['ptc_ad_timer'])) { echo $row['ptc_ad_timer']; } ?>" min="1" max="10" />
<span class="help-block">Update PTC ad timer? 1 = 1 Second, 10 = 10 Seconds</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ptc_ad_active">Active</label>
<div class="col-sm-10">
<select class="form-control" name="ptc_ad_active" id="ptc_ad_active">
<option value="0" <?php if($row['ptc_ad_active'] == 0) { ?>selected<?php } ?>>Active</option>
<option value="1" <?php if($row['ptc_ad_active'] == 1) { ?>selected<?php } ?>>Paused</option>
</select>
<span class="help-block">Update PTC ad status?</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="edit_ptc_ad" type="submit" value="Edit PTC Ad"/>
</div>
</div>
</form>

<?php } ?>

</div>

<?php include("footer.php"); ?>