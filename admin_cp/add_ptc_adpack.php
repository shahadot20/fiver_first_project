<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

if(isset($_POST['add_ptc_adpack'])) {

$ptc_adpack_name = str_clean($_POST['ptc_adpack_name']);
$ptc_adpack_points = str_clean($_POST['ptc_adpack_points']);
$ptc_adpack_price = str_clean($_POST['ptc_adpack_price']);

if(empty($ptc_adpack_name)) {
$error = "<li>You left the ptc adpack name blank.</li>";
}

if(empty($ptc_adpack_points)) {
$error .= "<li>You left the ptc adpack points blank.</li>";
}

if(empty($ptc_adpack_price)) {
$error .= "<li>You left the ptc adpack price blank.</li>";
}

if(!is_numeric($ptc_adpack_points)) {
$error .= "<li>Invalid value for adpack points.</li>";
}

if(!is_numeric($ptc_adpack_price)) {
$error .= "<li>Invalid value for adpack points.</li>";
}

if(!$error) {
mysqli_query($conn, "INSERT INTO `ptc_adpacks`(ptc_adpack_id,ptc_adpack_name,ptc_adpack_points,ptc_adpack_price) values(NULL, 
'".mysqli_real_escape_string($conn, $ptc_adpack_name)."',
'".mysqli_real_escape_string($conn, $ptc_adpack_points)."',
'".mysqli_real_escape_string($conn, $ptc_adpack_price)."'
)") or die(mysqli_error($conn));
    $success = "<strong>".$ptc_adpack_name."</strong> successfully added!";
}
}
?>

<div class="container">
<div class="row">

<?php if(isset($error)) { echo "<div class=\"alert alert-error\"><h3>Errors:</h3><ul>".$error."</ul></div>"; } ?>
<?php if(isset($success)) { echo "<div class=\"alert alert-success\">".$success."</div>"; } ?>

<form class="form-horizontal" role="form" method="post">

<h1>Adding New PTC Adpack</h1>

<div class="form-group">
<label class="control-label col-sm-2" for="ptc_adpack_name">Adpack Name</label>
<div class="col-sm-10">
<input class="form-control" name="ptc_adpack_name" id="ptc_adpack_name" type="text" value="<?php if(isset($ptc_adpack_name)) { echo $ptc_adpack_name; } else { echo $_GET['ptc_adpack_name']; } ?>" required/>
<span class="help-block">Gotta have a ptc adpack name.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ptc_adpack_points">Adpack Points</label>
<div class="col-sm-10">
<input class="form-control" name="ptc_adpack_points" id="ptc_adpack_points" type="text" value="<?php if(isset($ptc_adpack_points)) { echo $ptc_adpack_points; } else { echo $_GET['ptc_adpack_points']; } ?>" required/>
<span class="help-block">Enter the amount of adpack points for this pack.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ptc_adpack_price">Adpack Price</label>
<div class="col-sm-10">
<input class="form-control" name="ptc_adpack_price" id="ptc_adpack_price" type="text" value="<?php if(isset($ptc_adpack_price)) { echo $ptc_adpack_price; } else { echo $_GET['ptc_adpack_price']; } ?>" required/>
<span class="help-block">Enter the price of this adpack.</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="add_ptc_adpack" type="submit" value="Add Adpack"/>
</div>
</div>
</form>

</div>
</div>

<?php include("footer.php"); ?>