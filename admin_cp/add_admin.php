<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

if(isset($_POST['add_admin'])) {

$admin_username = str_clean($_POST['admin_username']);
$admin_password = str_clean($_POST['admin_password']);

$admin_sql = mysqli_query($conn, "SELECT `username` FROM `admins` WHERE `username`='".mysqli_real_escape_string($conn, $admin_username)."'");
$admin_exists = mysqli_num_rows($admin_sql);

if(empty($admin_username)) {
$error = "<li>You left the admin username blank.</li>";
}

if(!preg_match("/^[a-z0-9]+$/i", $admin_username)) {
$error .= "Username must contain only letters and numbers.";
}

if(strlen($admin_username) > 20) {
$error .= "Username must be shorter than 20 characters";
}

if(strlen($admin_username) < 5) {
$error .= "Username must be greater than 5 characters";
}

if(empty($admin_password)) {
$error .= "<li>You left the admin password blank.</li>";
}

if ($admin_exists > 0)  {
$error .= "<li>".$admin_username." is already in the database! Please try a different username.</li>";
}


if(!$error) {
$md5_admin_password = md5($admin_password);
mysqli_query($conn, "INSERT INTO `admins`(user_id, username,password,signup) values(NULL, 
'".mysqli_real_escape_string($conn, $admin_username)."',
'".mysqli_real_escape_string($conn, $md5_admin_password)."',
NOW()
)") or die(mysqli_error($conn));
    $success = "New Admin <strong>".$admin_username."</strong> has been successfully added!";
}
}
?>

<div class="container">
<div class="row">

<?php if(isset($error)) { echo "<div class=\"alert alert-error\"><h3>Errors:</h3><ul>".$error."</ul></div>"; } ?>
<?php if(isset($success)) { echo "<div class=\"alert alert-success\">".$success."</div>"; } ?>

<form class="form-horizontal" role="form" method="post">

<h1>Adding New Admin</h1>

<div class="form-group">
<label class="control-label col-sm-2" for="admin_username">Username</label>
<div class="col-sm-10">
<input class="form-control" name="admin_username" id="admin_username" type="text" value="<?php if(isset($admin_username)) { echo $admin_username; } else { echo $admin_username; } ?>" required/>
<span class="help-block">Gotta have a username.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="admin_password">Password</label>
<div class="col-sm-10">
<input class="form-control" name="admin_password" id="admin_password" type="password" value="<?php if(isset($admin_password)) { echo $admin_password; } else { echo $admin_password; } ?>" required/>
<span class="help-block">Gotta have a password.</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="add_admin" type="submit" value="Add New Admin"/>
</div>
</div>
</form>

</div>
</div>

<?php include("footer.php"); ?>