<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

$news_id = str_clean($_GET['news_id']);

$get_page = mysqli_query($conn, "SELECT * FROM `news` WHERE `news_id`='".mysqli_real_escape_string($conn, intval($news_id))."' LIMIT 1") or die(mysqli_error($conn));

if(empty($news_id) || $news_id == "" || !$news_id) {
$error = "You never selected a news to edit.";
} else if(mysqli_num_rows($get_page) == 0) {
$error = "This news doesn't exist.";
} else { 

if(isset($_POST['edit_news'])) {

$news_title = str_clean($_POST['news_title']);
$news_description = str_clean($_POST['news_description']);
$news_active =  str_clean($_POST['news_active']);

if(empty($news_title)) {
$error = "You left the news title blank.";
}

if(empty($news_description)) {
$error .= "You left the hews description blank.";
}

if($news_active == "") {
$error .= "You need to select if news is active or not.";
}

if(!is_numeric($news_active)) {
$error .= "You need to enter 0(disabled) or 1(active).</li>";
}

if($news_active != "0" && $news_active != "1") {
$error .= "Is ".$news_title." active or disabled?";

}

if(!$error) {
mysqli_query($conn, "UPDATE `news` SET 
`news_title`='".mysqli_real_escape_string($conn, $news_title)."',
`news_description`='".mysqli_real_escape_string($conn, $news_description)."',
`news_active`='".mysqli_real_escape_string($conn, intval($news_active))."'
 WHERE `news_id`='".mysqli_real_escape_string($conn, intval($news_id))."'") or die(mysqli_error($conn));
    $success = "News <strong>".$news_title."</strong> has been successfully updated!";
}

}

if(isset($_POST['delete_news'])) {
mysqli_query($conn, "DELETE FROM `news` WHERE `news_id`='".mysqli_real_escape_string($conn, intval($news_id))."'") or die(mysqli_error($conn));
$redirect = $config['base_url']."admin_cp/news.php?delete_news=1";
echo "<script>document.location.href='".$redirect."'</script>";
exit;
}
}
?>

<div class="container">

<?php if(isset($error)) { ?><div class="alert alert-danger"><?php echo $error; ?></div><?php } ?>
<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<?php $row = mysqli_fetch_array($get_page); ?>

<form class="form-horizontal" role="form" method="post">

<h1 class="page-header">Editing News: <?php echo $row['news_title']; ?></h1>

<div class="form-group">
<label class="control-label col-sm-2" for="news_title">News Title</label>
<div class="col-sm-10">
<input class="form-control" name="news_title" id="news_title" type="text" value="<?php if(isset($row['news_title'])) { echo $row['news_title']; } else { echo $news_title; } ?>" required/>
<span class="help-block">Gotta have a news title.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="news_description">News Description</label>
<div class="col-sm-10">
<textarea rows="15" class="form-control" name="news_description" id="news_description" required/><?php if(isset($row['news_description'])) { echo html_entity_decode($row['news_description']); } else { echo html_entity_decode($news_description); } ?></textarea>
<span class="help-block">Edit the contents of your news description (HTML is allowed.)</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="news_active">News Active</label>
<div class="col-sm-10">
<select class="form-control" name="news_active" id="news_active">
<option value="0" <?php if($row['news_active'] == 0) { ?>selected<?php } ?>>Disabled</option>
<option value="1" <?php if($row['news_active'] == 1) { ?>selected<?php } ?>>Active</option>
</select>
<span class="help-block">This news is currently set to: <?php if($row['news_active'] == 1) { echo "<strong class=\"text text-success\">Active</strong>"; } else { echo "<strong class=\"text text-danger\"><i>Disabled</i></strong>"; } ?></span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="edit_news" type="submit" value="Edit News"/>
<input class="btn btn-danger pull-right" name="delete_news" type="submit" value="Delete News"/>
</div>
</div>
</form>

</div>

<link rel="stylesheet" href="<?php echo $config['base_url']; ?>/admin_cp/assets/js/sceditor/minified/themes/square.min.css" />

    <script src="<?php echo $config['base_url']; ?>admin_cp/assets/js/sceditor/minified/sceditor.min.js.php"></script>
<script src="<?php echo $config['base_url']; ?>/admin_cp/assets/js/sceditor/minified/formats/xhtml.js"></script>
<script>
// Replace the textarea #example with SCEditor
var textarea = document.getElementById('news_description');
sceditor.create(textarea, {
	format: 'xhtml',
	style: '<?php echo $config['base_url']; ?>/admin_cp/assets/js/sceditor/minified/themes/content/default.min.css'
});
</script>
<?php include("footer.php"); ?>