<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}


// get the settings from `config` table
$get_settings = mysqli_query($conn, "SELECT * FROM `config` WHERE `site_id`='1' LIMIT 1") or die(mysqli_error($conn));


if(isset($_POST['edit_dailybonus_settings'])) {

$site_daily_offers = str_clean($_POST['site_daily_offers']);
$site_daily_offers_points = str_clean($_POST['site_daily_offers_points']);
$site_daily_offers_points_minimum = str_clean($_POST['site_daily_offers_points_minimum']);
$site_daily_active = str_clean($_POST['site_daily_active']);

if(empty($site_daily_offers) && $site_daily_active == 1) {
$error = "You left the site daily offers amount blank.<br />";
}

if(empty($site_daily_offers_points) && $site_daily_active == 1) {
$error .= "You left the site daily offer bonus points blank.<br />";
}

if(!is_numeric($site_daily_offers)) {
$error .= "The daily offers amount can only contain numbers.<br />";
}

if(!is_numeric($site_daily_offers_points)) {
$error .= "The daily offer bonus points can only contain numbers.<br />";
}

if(!is_numeric($site_daily_offers_points_minimum)) {
$error .= "The daily offer point minimum can only contain numbers.<br />";
}

if($site_daily_active != "0" && $site_daily_active != "1") {
$error .= "Is daily bonus active?";
}

if(!$error) {
mysqli_query($conn, "UPDATE `config` SET 
`site_daily_offers`='".mysqli_real_escape_string($conn, $site_daily_offers)."',
`site_daily_offers_points`='".mysqli_real_escape_string($conn, $site_daily_offers_points)."',
`site_daily_offers_points_minimum`='".mysqli_real_escape_string($conn, $site_daily_offers_points_minimum)."',
`site_daily_active`='".mysqli_real_escape_string($conn, $site_daily_active)."'
WHERE `site_id`='1'") or die(mysqli_error($conn));
    $success = "Daily bonus settings successfully updated!";
}

}

?>

<div class="container">

<?php if(isset($error)) { ?><div class="alert alert-danger"><?php echo $error; ?></div><?php } ?>
<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<?php while($row = mysqli_fetch_array($get_settings)) { ?>

<form class="form-horizontal" role="form" method="post">

<h1>Editing Daily Bonus Settings</h1>

<div class="form-group">
<label class="control-label col-sm-2" for="site_daily_offers_points_minimum">Daily Offer Point Minimum</label>
<div class="col-sm-10">
<input class="form-control" name="site_daily_offers_points_minimum" id="site_daily_offers_points_minimum" type="text" value="<?php if(isset($row['site_daily_offers_points_minimum'])) { echo $row['site_daily_offers_points_minimum']; } else { echo $site_daily_offers; } ?>"/>
<span class="help-block">How many points minimum should an offer be?</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="">Daily Offers</label>
<div class="col-sm-10">
<input class="form-control" name="site_daily_offers" id="site_daily_offers" type="text" value="<?php if(isset($row['site_daily_offers'])) { echo $row['site_daily_offers']; } else { echo $site_daily_offers; } ?>"/>
<span class="help-block">How many offers should a user complete before claiming daily bonus?</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="site_daily_offers_points">Daily Offer Payout</label>
<div class="col-sm-10">
<input class="form-control" name="site_daily_offers_points" id="site_daily_offers_points" type="text" value="<?php if(isset($row['site_daily_offers_points'])) { echo $row['site_daily_offers_points']; } else { echo $site_daily_offers_points; }?>" required/>
<span class="help-block">How many points do you want to offer when someone meets the daily bonus offer amount?</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="site_daily_active">Daily Bonus Status</label>
<div class="col-sm-10">
<select class="form-control" name="site_daily_active" id="site_daily_active">
<option value="0" <?php if($row['site_daily_active'] == 0) { ?>selected<?php } ?>>Disabled</option>
<option value="1" <?php if($row['site_daily_active'] == 1) { ?>selected<?php } ?>>Enabled</option>
</select>
<span class="help-block">Update the daily bonus status.</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="edit_dailybonus_settings" type="submit" value="Edit Daily Bonus Settings"/>
</div>
</div>
</form>

<?php } ?>

</div>

<?php include("footer.php"); ?>