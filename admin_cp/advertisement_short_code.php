<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

$edit_id = str_clean($_GET['id']);

$get_advertisement = mysqli_query($conn, "SELECT * FROM `advertisements` WHERE `ad_id`='".mysqli_real_escape_string($conn, intval($edit_id))."' LIMIT 1") or die(mysqli_error($conn));

if(empty($edit_id) || $edit_id == "" || !$edit_id) {
$error = "You never selected an advertisement to edit.";
} else if(mysqli_num_rows($get_advertisement) == 0) {
$error = "This advertisement doesn't exist.";
}
?>

<div class="container">

<?php if(isset($error)) { ?><div class="alert alert-danger"><?php echo $error; ?></div><?php die(); } ?>
<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<?php $row = mysqli_fetch_array($get_advertisement); ?>

<form class="form-horizontal" role="form" method="post">

<h1 class="page-header">Advertisement Short Code for: <?php echo $row['ad_name']; ?></h1>

<div class="form-group">
<label class="control-label col-sm-2" for="ad_html_code">Advertisement Short Code</label>
<div class="col-sm-10">
<textarea rows="2" class="form-control" name="ad_html_code" id="ad_html_code" required>
<?php
$short_code = "<?php html_advertisement(".$row['ad_id']."); ?>";
echo html_entity_decode($short_code);
?>
</textarea>
<span class="help-block">Copy and paste this advertisement short code on pages you want your advertisement to appear. <strong>Note: </strong>This code will only work on PHP pages.</span>
</div>
</div>

</form>
</div>

<?php include("footer.php"); ?>