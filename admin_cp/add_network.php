<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

if(isset($_POST['add_network'])) {

$network_name = str_clean($_POST['network_name']);
$network_description = str_clean($_POST['network_description']);
$network_seo_name = url_slug($_POST['network_name']);
$network_html_code = str_clean($_POST['network_html_code']);
$network_active =  intval(str_clean($_POST['network_active']));
$network_popular =  intval(str_clean($_POST['network_popular']));
$network_conversion_ratio =  intval(str_clean($_POST['network_conversion_ratio']));


$network_sql = mysqli_query($conn, "SELECT `network_name` FROM `networks` WHERE `network_name`='".mysqli_real_escape_string($conn, $network_name)."'") or die(mysqli_error($conn));
$network_exists = mysqli_num_rows($network_sql);

if(empty($network_name)) {
$error = "<li>You left the network name blank.</li>";
}

if(empty($network_description)) {
$error .= "<li>You left the network description blank.</li>";
}

if(empty($network_html_code)) {
$error .= "<li>You left the network html code blank.</li>";
}

if($network_active == "") {
$error .= "<li>You need to select if network is active or not.</li>";
}

if(empty($network_conversion_ratio)) {
$error .= "<li>You left the network conversion ratio blank, this value is required.</li>";
}

if(strlen($network_description) > 200) {
$error .= "<li>Don't go past 200 characters.</li>";
}

if(!is_numeric($network_active)) {
$error .= "<li>You need to enter 0(disabled) or 1(active).</li>";
}

if(!is_numeric($network_conversion_ratio)) {
$error .= "<li>Network conversion ratio must be a number.</li>";
}

if(!is_numeric($network_popular)) {
$error .= "<li>You need to enter 0(unpopular) or 1(popular).</li>";
}

if($network_conversion_ratio > 100) {
$error .= "<li>Network conversion ratio can't exceed 100.</li>";
}

if ($network_exists > 0)  {
$error .= "<li>".$network_name." is already in the database!</li>";
}

if($network_active != "0" && $network_active != "1") {
$error .= "<li>Is ".$network_name." active or disabled?</li>";
}

if($network_popular != "0" && $network_popular != "1") {
$error .= "<li>Is ".$network_name." popular?</li>";
}

if(!$error) {
mysqli_query($conn, "INSERT INTO `networks`(network_id, network_name,network_seo_name,network_description,network_html_code,network_active,network_popular,network_conversion_ratio) values(NULL, 
'".mysqli_real_escape_string($conn, $network_name)."',
'".mysqli_real_escape_string($conn, $network_seo_name)."',
'".mysqli_real_escape_string($conn, $network_description)."',
'".mysqli_real_escape_string($conn, $network_html_code)."',
'".mysqli_real_escape_string($conn, $network_active)."',
'".mysqli_real_escape_string($conn, $network_popular)."',
'".mysqli_real_escape_string($conn, $network_conversion_ratio)."'
)") or die(mysqli_error($conn));
    $success = "Network <strong>".$network_name."</strong> has been successfully added!";
}
}
?>

<div class="container">
<div class="row">

<?php if(isset($error)) { echo "<div class=\"alert alert-error\"><h3>Errors:</h3><ul>".$error."</ul></div>"; } ?>
<?php if(isset($success)) { echo "<div class=\"alert alert-success\">".$success."</div>"; } ?>

<form class="form-horizontal" role="form" method="post">

<h1>Add a Network</h1>

<div class="form-group">
<label class="control-label col-sm-2" for="network_name">Network Name</label>
<div class="col-sm-10">
<input class="form-control" name="network_name" id="network_name" type="text" value="<?php if(isset($network_name)) { echo $network_name; } else { echo $network_name; }?>" required/>
<span class="help-block">Gotta have a network name.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="network_description">Network Description</label>
<div class="col-sm-10">
<input class="form-control" name="network_description" id="network_description" type="text" value="<?php if(isset($network_description)) { echo $network_description; } else { echo $network_description; } ?>" required/>
<span class="help-block">Make sure your network description is under 200 characters</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="network_html_code">Network HTML Code</label>
<div class="col-sm-10">
<textarea class="form-control" name="network_html_code" id="network_html_code" rows="3" required><?php if(isset($network_html_code)) { echo $network_html_code; } else { echo $network_html_code; }?></textarea>
<span class="help-block">Add the offerwall HTML code only, example: <code><?php $html = '<iframe src="http://unlck.com/wall/Af/-USERNAME-"></iframe>'; echo htmlentities($html); ?></code> Also use -USERNAME- code where the username(subid) should be added, example: http://unlck.com/wall/Af/-USERNAME-</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="network_conversion_ratio">Network Conversion Ratio</label>
<div class="col-sm-10">
<input class="form-control" type="number" value="<?php if(isset($network_conversion_ratio)) { echo $network_conversion_ratio; } else { echo $network_conversion_ratio; }?>" placeholder="60" name="network_conversion_ratio">
<span class="help-block">Set your network conversion ratio, this is the same ratio you specified on the offerwall network. This is used to calculate admin profits, and is required.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="network_active">Network Active</label>
<div class="col-sm-10">
<select class="form-control" name="network_active" id="network_active">
<option value="0">Disabled</option>
<option value="1">Active</option>
</select>
<span class="help-block">Enable or deactivate a network</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="network_popular">Network Popular</label>
<div class="col-sm-10">
<select class="form-control" name="network_popular" id="network_popular">
<option value="0">Unpopular</option>
<option value="1">Popular</option>
</select>
<span class="help-block">Toggle the popularity of the network.</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="add_network" type="submit" value="Add Network"/>
</div>
</div>
</form>

</div>
</div>

<?php include("footer.php"); ?>