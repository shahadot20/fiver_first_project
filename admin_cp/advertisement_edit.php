<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

$edit_id = str_clean($_GET['id']);

$get_advertisement = mysqli_query($conn, "SELECT * FROM `advertisements` WHERE `ad_id`='".mysqli_real_escape_string($conn, intval($edit_id))."' LIMIT 1") or die(mysqli_error($conn));

if(empty($edit_id) || $edit_id == "" || !$edit_id) {
$error = "You never selected an advertisement to edit.";
} else if(mysqli_num_rows($get_advertisement) == 0) {
$error = "This advertisement doesn't exist.";
} else { 

if(isset($_POST['edit_advertisement'])) {

$ad_name = str_clean($_POST['ad_name']);
$ad_html_code = str_clean($_POST['ad_html_code']);
$ad_active =  str_clean($_POST['ad_active']);

if(empty($ad_name)) {
$error = "You left the ad name blank.";
}

if(empty($ad_html_code)) {
$error .= "You left the ad HTML code blank.";
}

if($ad_active == "") {
$error .= "You need to select if the advertisement is active or not.";
}

if(!is_numeric($ad_active)) {
$error .= "You need to enter 0(disabled) or 1(active).</li>";
}

if($ad_active != "0" && $ad_active != "1") {
$error .= "Is ".$ad_name." active or disabled?";

}

if(!$error) {
mysqli_query($conn, "UPDATE `advertisements` SET 
`ad_name`='".mysqli_real_escape_string($conn, $ad_name)."',
`ad_html_code`='".mysqli_real_escape_string($conn, $ad_html_code)."',
`ad_active`='".mysqli_real_escape_string($conn, $ad_active)."' 
WHERE `ad_id`='".mysqli_real_escape_string($conn, $edit_id)."'") or die(mysqli_error($conn));
    $success = "Advertisement <strong>".$provider_name."</strong> has been successfully updated!";
}

}

if(isset($_POST['delete_advertisement'])) {
mysqli_query($conn, "DELETE FROM `advertisements` WHERE `ad_id`='".mysqli_real_escape_string($conn, intval($edit_id))."'") or die(mysqli_error($conn));
$redirect = $config['base_url']."admin_cp/advertisements.php?delete_advertisement=1";
    echo "<script>document.location.href='".$redirect."'</script>";
    exit;
}
} 
?>

<div class="container">

<?php if(isset($error)) { ?><div class="alert alert-danger"><?php echo $error; ?></div><?php } ?>
<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<?php $row = mysqli_fetch_array($get_advertisement); ?>

<form class="form-horizontal" role="form" method="post">

<h1 class="page-header">Editing Advertisement: <?php echo $row['ad_name']; ?></h1>

<div class="form-group">
<label class="control-label col-sm-2" for="ad_name">Advertisement Name</label>
<div class="col-sm-10">
<input class="form-control" name="ad_name" id="ad_name" type="text" value="<?php if(isset($row['ad_name'])) { echo $row['ad_name']; } else { echo $ad_name; }?>" required/>
<span class="help-block">Gotta have an advertisement name.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ad_html_code">Provider Description</label>
<div class="col-sm-10">
<textarea rows="4" class="form-control" name="ad_html_code" id="ad_html_code" required><?php if(isset($row['ad_html_code'])) { echo $row['ad_html_code']; } else { echo $ad_html_code; }?></textarea>
<span class="help-block">Edit your advertisement code.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ad_active">Advertisement Active</label>
<div class="col-sm-10">
<select class="form-control" name="ad_active" id="ad_active">
<option value="0" <?php if($row['ad_active'] == 0) { ?>selected<?php } ?>>Disabled</option>
<option value="1" <?php if($row['ad_active'] == 1) { ?>selected<?php } ?>>Active</option>
</select>
<span class="help-block">This advertisement is currently set to: <?php if($row['ad_active'] == 1) { echo "<strong class=\"text text-success\">Active</strong>"; } else { echo "<strong class=\"text text-danger\"><i>Disabled</i></strong>"; } ?></span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="edit_advertisement" type="submit" value="Edit Advertisement"/>
<input class="btn btn-danger pull-right" name="delete_advertisement" type="submit" value="Delete Advertisement"/>
</div>
</div>
</form>

</div>

<?php include("footer.php"); ?>