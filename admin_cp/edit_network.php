<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

$edit_id = str_clean($_GET['c_id']);

$get_network = mysqli_query($conn, "SELECT * FROM `networks` WHERE `network_id`='".mysqli_real_escape_string($conn, $edit_id)."' LIMIT 1") or die(mysqli_error($conn));

if(empty($edit_id) || $edit_id == "" || !$edit_id) {
$error = "You never selected a network to edit.";
} else if(mysqli_num_rows($get_network) == 0) {
$error = "This network doesn't exist.";
} else { 

if(isset($_POST['edit_network'])) {

$network_name = str_clean($_POST['network_name']);
$network_seo_name = url_slug($network_name);
$network_description = str_clean($_POST['network_description']);
$network_html_code = str_clean($_POST['network_html_code']);
$network_active =  intval(str_clean($_POST['network_active']));
$network_popular =  intval(str_clean($_POST['network_popular']));
$network_conversion_ratio =  intval(str_clean($_POST['network_conversion_ratio']));


$network_sql = mysqli_query($conn, "SELECT `network_name` FROM `networks` WHERE `network_name`='".mysqli_real_escape_string($conn, $network_name)."'") or die(mysqli_error($conn));
$network_exists = mysqli_num_rows($network_sql);

if(empty($network_name)) {
$error = "You left the network name blank.";
}

if(empty($network_description)) {
$error .= "You left the network description blank.";
}

if(empty($network_html_code)) {
$error .= "You left the network HTML code blank.";
}

if(empty($network_conversion_ratio)) {
$error .= "You left the network conversion ratio blank.";
}

if(strlen($network_description) > 200) {
$error .= "Don't go past 200 characters.";
}

if($network_conversion_ratio > 100) {
$error .= "Network conversion ratio can't exceed 100.";
}

if(!is_numeric($network_active)) {
$error .= "You need to enter 0(disabled) or 1(active).";
}

if(!is_numeric($network_popular)) {
$error .= "You need to enter 0(unpopular) or 1(popular).";
}

if(!is_numeric($network_conversion_ratio)) {
$error .= "Network conversion ratio must be a number.";
}

if($network_active != "0" && $network_active != "1") {
$error .= "Is ".$network_name." active or disabled?";
}

if($network_popular != "0" && $network_popular != "1") {
$error .= "Is ".$network_name." popular or unpopular?";
}

if(!$error) {
mysqli_query($conn, "UPDATE `networks` SET 
`network_name`='".mysqli_real_escape_string($conn, $network_name)."',
`network_seo_name`='".mysqli_real_escape_string($conn, $network_seo_name)."',
`network_description`='".mysqli_real_escape_string($conn, $network_description)."',
`network_html_code`='".mysqli_real_escape_string($conn, $network_html_code)."',
`network_conversion_ratio`='".mysqli_real_escape_string($conn, $network_conversion_ratio)."',
`network_active`='".mysqli_real_escape_string($conn, $network_active)."',
`network_popular`='".mysqli_real_escape_string($conn, $network_popular)."' 
 WHERE `network_id`='".mysqli_real_escape_string($conn, $edit_id)."'") or die(mysqli_error($conn));
    $success = "Network <strong>".$network_name."</strong> has been successfully updated!";
}

}

if(isset($_POST['delete_network'])) {
mysqli_query($conn, "DELETE FROM `networks` WHERE `network_id`='".mysqli_real_escape_string($conn, $edit_id)."'") or die(mysqli_error($conn));
$redirect = $config['base_url']."admin_cp/networks.php?delete_network=1";
    echo "<script>document.location.href='".$redirect."'</script>";
exit;
}
}
?>

<div class="container">

<?php if(isset($error)) { ?><div class="alert alert-danger"><?php echo $error; ?></div><?php } ?>
<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<?php while($row = mysqli_fetch_array($get_network)) { ?>

<form class="form-horizontal" role="form" method="post">

<h1 class="page-header">Editing Network: <?php echo $row['network_name']; ?></h1>

<div class="form-group">
<label class="control-label col-sm-2" for="network_name">Network Name</label>
<div class="col-sm-10">
<input class="form-control" name="network_name" id="network_name" type="text" value="<?php if(isset($row['network_name'])) { echo $row['network_name']; } else { echo $network_name; } ?>" required/>
<span class="help-block">Gotta have a network name.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="network_description">Network Description</label>
<div class="col-sm-10">
<input class="form-control" name="network_description" id="network_description" type="text" value="<?php if(isset($row['network_description'])) { echo $row['network_description']; } else { echo $network_description; } ?>" required/>
<span class="help-block">Make sure your network description is under 200 characters.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="network_html_code">Network HTML Code</label>
<div class="col-sm-10">
<textarea class="form-control" name="network_html_code" id="network_html_code" type="text" rows="3" required><?php if(isset($row['network_html_code'])) { echo $row['network_html_code']; } else { echo $network_html_code; }?></textarea>
<span class="help-block">Your networks HTML Code.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="network_conversion_ratio">Network Conversion Ratio</label>
<div class="col-sm-10">
<input type="number" class="form-control" name="network_conversion_ratio" id="network_conversion_ratio" value="<?php if(isset($row['network_conversion_ratio'])) { echo $row['network_conversion_ratio']; } else { echo $network_conversion_ratio; }?>" required>
<span class="help-block">Your network conversion ratio, this is the same ratio you specified on the offerwall network. This is used to calculate admin profits, and is required..</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="network_active">Network Active</label>
<div class="col-sm-10">
<select class="form-control" name="network_active" id="network_active">
<option value="0" <?php if($row['network_active'] == 0) { ?>selected<?php } ?>>Disabled</option>
<option value="1" <?php if($row['network_active'] == 1) { ?>selected<?php } ?>>Active</option>
</select>
<span class="help-block">This network is currently set to: <?php if($row['network_active'] == 1) { echo "<strong class=\"text text-success\">Active</strong>"; } else { echo "<strong class=\"text text-danger\"><i>Disabled</i></strong>"; } ?></span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="network_popular">Network Popular</label>
<div class="col-sm-10">
<select class="form-control" name="network_popular" id="network_popular">
<option value="0" <?php if($row['network_popular'] == 0) { ?>selected<?php } ?>>Unpopular</option>
<option value="1" <?php if($row['network_popular'] == 1) { ?>selected<?php } ?>>Popular</option>
</select>
<span class="help-block">This network's popularity is currently set to: <?php if($row['network_popular'] == 1) { echo "<strong class=\"text text-success\">Popular</strong>"; } else { echo "<strong class=\"text text-danger\"><i>Unpopular</i></strong>"; } ?></span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="edit_network" type="submit" value="Edit Network"/>
<input class="btn btn-danger pull-right" name="delete_network" type="submit" value="Delete Network"/>
</div>
</div>
</form>

<?php } ?>

</div>

<?php include("footer.php"); ?>