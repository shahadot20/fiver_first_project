<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include('header.php');

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

?>

<div class="container">
<div class="row">

<h1 class="page-header">Dashboard</h1>

<div class="alert alert-info">Basic back-end statistical data.</div>

    
<h2 class="page-header">Site Profit</h2>

<div class="row">
<div class="col-lg-3">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Total Profit</h3>
  </div>
  <div class="panel-body text-center">
    <?php  
      $get_earned_sql = mysqli_query($conn, "SELECT SUM(amount) FROM admin_earnings") or die(mysqli_error($conn));
while($row = mysqli_fetch_array($get_earned_sql)){
$total_earned = $row['SUM(amount)'];
}

echo "$".number_format($total_earned, 2);
?>
</div>
</div>
</div>

<div class="col-lg-3">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Profit <span class="label label-info pull-right">last 24 hours</span></h3>
  </div>
  <div class="panel-body text-center">
    <?php  
      $get_earned_daily_sql = mysqli_query($conn, "SELECT SUM(amount) FROM admin_earnings WHERE date_added >= NOW() - INTERVAL 1 DAY") or die(mysqli_error($conn));
while($row = mysqli_fetch_array($get_earned_daily_sql)){
$total_earned_daily = $row['SUM(amount)'];
}

echo "$".number_format($total_earned_daily, 2);
?>
</div>
</div>
</div>

<div class="col-lg-3">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Profit <span class="label label-info pull-right">last week</span></h3>
  </div>
  <div class="panel-body text-center">
    <?php  
      $get_earned_weekly_sql = mysqli_query($conn, "SELECT SUM(amount) FROM admin_earnings WHERE date_added >= NOW() - INTERVAL 1 WEEK") or die(mysqli_error($conn));
while($row = mysqli_fetch_array($get_earned_weekly_sql)){
$total_earned_weekly = $row['SUM(amount)'];
}

echo "$".number_format($total_earned_weekly, 2);
?>
</div>
</div>
</div>

<div class="col-lg-3">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Profit <span class="label label-info pull-right">last month</span></h3>
  </div>
  <div class="panel-body text-center">
    <?php  
      $get_earned_monthly_sql = mysqli_query($conn, "SELECT SUM(amount) FROM admin_earnings WHERE date_added >= NOW() - INTERVAL 1 MONTH") or die(mysqli_error($conn));
while($row = mysqli_fetch_array($get_earned_monthly_sql)){
$total_earned_montly = $row['SUM(amount)'];
}

echo "$".number_format($total_earned_montly, 2);
?>
</div>
</div>
</div>


<!-- end admin profit section -->

</div>


<h2 class="page-header">Users</h2>

<div class="row">
<div class="col-lg-3">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Total Users</h3>
  </div>
  <div class="panel-body text-center">
    <?php  
      $get_users_sql = mysqli_query($conn, "SELECT * FROM users") or die(mysqli_error($conn));
     $total_users = mysqli_num_rows($get_users_sql);

echo number_format($total_users);
?>
</div>
</div>
</div>

<div class="col-lg-3">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Signups <span class="label label-info pull-right">last 24 hours</span></h3>
  </div>
  <div class="panel-body text-center">
    <?php  
      $get_day_signups_sql = mysqli_query($conn, "SELECT * FROM users WHERE signup >= NOW() - INTERVAL 1 DAY") or die(mysqli_error($conn));
      $get_day_signups = mysqli_num_rows($get_day_signups_sql);

echo number_format($get_day_signups);
?>
</div>
</div>
</div>

<div class="col-lg-3">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Signups <span class="label label-info pull-right">last week</span></h3>
  </div>
  <div class="panel-body text-center">
    <?php  
      $get_weekly_signups_sql = mysqli_query($conn, "SELECT * FROM users WHERE signup >= NOW() - INTERVAL 1 WEEK") or die(mysqli_error($conn));
      $get_weekly_signups = mysqli_num_rows($get_weekly_signups_sql);

echo number_format($get_weekly_signups);
?>
</div>
</div>
</div>

<div class="col-lg-3">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Signups <span class="label label-info pull-right">last month</span></h3>
  </div>
  <div class="panel-body text-center">
    <?php  
      $get_monthly_signups_sql = mysqli_query($conn, "SELECT * FROM users WHERE signup >= NOW() - INTERVAL 1 MONTH") or die(mysqli_error($conn));
      $get_monthly_signups = mysqli_num_rows($get_monthly_signups_sql); 

echo number_format($get_monthly_signups);
?>
</div>
</div>
</div>


<!-- end users section -->

</div>

<h2 class="page-header">Offer Leads <small>Completed offers</small></h2>

<div class="row">
<div class="col-lg-3">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Total Offer Leads</h3>
  </div>
  <div class="panel-body text-center">
    <?php  
      $get_leads_total_sql = mysqli_query($conn, "SELECT * FROM activity_history") or die(mysqli_error($conn));
     $total_offers = mysqli_num_rows($get_leads_total_sql);

echo number_format($total_offers);
?>
</div>
</div>
</div>

<div class="col-lg-3">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Leads <span class="label label-info pull-right">last 24 hours</span></h3>
  </div>
  <div class="panel-body text-center">
    <?php  
      $get_day_leads_sql = mysqli_query($conn, "SELECT * FROM activity_history WHERE history_date >= NOW() - INTERVAL 1 DAY") or die(mysqli_error($conn));
      $get_day_leads = mysqli_num_rows($get_day_leads_sql);

echo number_format($get_day_leads);
?>
</div>
</div>
</div>

<div class="col-lg-3">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Leads <span class="label label-info pull-right">last week</span></h3>
  </div>
  <div class="panel-body text-center">
    <?php  
      $get_weekly_leads_sql = mysqli_query($conn, "SELECT * FROM activity_history WHERE history_date >= NOW() - INTERVAL 1 WEEK") or die(mysqli_error($conn));
      $get_weekly_leads = mysqli_num_rows($get_weekly_leads_sql);

echo number_format($get_weekly_leads);
?>
</div>
</div>
</div>

<div class="col-lg-3">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Leads <span class="label label-info pull-right">last month</span></h3>
  </div>
  <div class="panel-body text-center">
    <?php  
      $get_monthly_leads_sql = mysqli_query($conn, "SELECT * FROM activity_history WHERE history_date >= NOW() - INTERVAL 1 MONTH") or die(mysqli_error($conn));
      $get_monthly_leads = mysqli_num_rows($get_monthly_leads_sql); 

echo number_format($get_monthly_leads);
?>
</div>
</div>
</div>

<!-- end offers section -->

</div>

</div>
</div>

<?php include('footer.php'); ?>