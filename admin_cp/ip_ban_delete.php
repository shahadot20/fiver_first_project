<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['site_url']."admin_cp/login.php'</script>";
    exit;
}

$get_ban_id = str_clean($_GET['id']);

$get_ip = mysqli_query($conn, "SELECT * FROM `banned_ips` WHERE `ban_id`='".mysqli_real_escape_string($conn, $get_ban_id)."' LIMIT 1") or die(mysqli_error($conn));

if(empty($get_ban_id) || $get_ban_id == "" || !$get_ban_id) {
$error = "You never selected a IP Address to delete.";
} else if(mysqli_num_rows($get_ip) == 0) {
$error = "No record of this ban entry.";
} else {

mysqli_query($conn, "DELETE FROM `banned_ips` WHERE `ban_id`='".mysqli_real_escape_string($conn, $get_ban_id)."'") or die(mysqli_error($conn));

$success = "This IP address has been deleted.";

?>

<div class="container">

<?php if(isset($error)) { ?><div class="alert alert-danger"><?php echo $error; ?></div><?php } ?>
<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>


<?php } ?>

</div>

<?php include("footer.php"); ?>