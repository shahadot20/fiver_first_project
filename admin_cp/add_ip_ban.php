<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

if(isset($_POST['add_ip_ban'])) {

$ban_ip = str_clean($_POST['ip_address']);
$ban_reason = str_clean($_POST['ban_reason']);

$ip_exist_sql = mysqli_query($conn, "SELECT `ban_ip_address` FROM `banned_ips` WHERE `ban_ip_address`='".mysqli_real_escape_string($conn, $ban_up)."'") or die(mysqli_error($conn));
$ip_exists = mysqli_num_rows($ip_exist_sql);

if(empty($ban_ip)) {
$error = "<li>You left the IP address blank.</li>";
}

if ($ip_exists > 0)  {
$error .= "<li>".$ban_ip." is already in the database!</li>";
}


if(!$error) {
mysqli_query($conn, "INSERT INTO `banned_ips`(ban_id, ban_ip_address,ban_reason,ban_banner,ban_date_added) values(NULL, 
'".mysqli_real_escape_string($conn, $ban_ip)."',
'".mysqli_real_escape_string($conn, $ban_reason)."',
'".mysqli_real_escape_string($conn, $_SESSION['admin_username'])."',
NOW()
)") or die(mysqli_error($conn));
    $success = "<strong>".$ban_ip."</strong> has been successfully been banned!";
}
}
?>

<div class="container">
<div class="row">

<?php if(isset($error)) { echo "<div class=\"alert alert-error\"><h3>Errors:</h3><ul>".$error."</ul></div>"; } ?>
<?php if(isset($success)) { echo "<div class=\"alert alert-success\">".$success."</div>"; } ?>

<form class="form-horizontal" role="form" method="post">

<h1>Adding New IP Ban</h1>

<div class="form-group">
<label class="control-label col-sm-2" for="ip_address">IP Address</label>
<div class="col-sm-10">
<input class="form-control" name="ip_address" id="ip_address" type="text" value="<?php if(isset($ip_address)) { echo $admin_username; } else { echo $_GET['ban_ip_address']; } ?>" required/>
<span class="help-block">Gotta have an IP to ban.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ban_reason">Ban Reason</label>
<div class="col-sm-10">
<input class="form-control" name="ban_reason" id="ban_reason" type="text" value="<?php if(isset($ban_reason)) { echo $ban_reason; } else { echo $ban_reason; } ?>" required/>
<span class="help-block">Gotta have a reason to ban IP address.</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="add_ip_ban" type="submit" value="Ban IP Address"/>
</div>
</div>
</form>

</div>
</div>

<?php include("footer.php"); ?>