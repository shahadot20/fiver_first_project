<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

if(isset($_POST['add_news'])) {

$news_title = str_clean($_POST['news_title']);
$news_description = str_clean($_POST['news_description']);
$news_active =  intval(str_clean($_POST['news_active']));

$news_sql = mysqli_query($conn, "SELECT `news_title` FROM `news` WHERE `news_title`='".mysqli_real_escape_string($conn, $news_title)."'") or die(mysqli_error($conn));
$news_exists = mysqli_num_rows($news_sql);

if(empty($news_title)) {
$error = "<li>You left the news title blank.</li>";
}

if(empty($news_description)) {
$error .= "<li>You left the news description blank.</li>";
}

if($news_active == "") {
$error .= "<li>You need to select if news article is active or not.</li>";
}

if(!is_numeric($news_active)) {
$error .= "<li>You need to enter 0(disabled) or 1(active).</li>";
}

if ($news_exists > 0)  {
$error .= "<li>".$news_title." title is already in the database!</li>";
}

if($news_active != "0" && $news_active != "1") {
$error .= "<li>Is ".$news_name." active or disabled?</li>";

}

if(!$error) {
mysqli_query($conn, "INSERT INTO `news`(news_id,news_title,news_description,news_date_added,news_active) values(NULL, 
'".mysqli_real_escape_string($conn, $news_title)."',
'".mysqli_real_escape_string($conn, $news_description)."',
NOW(),
'".mysqli_real_escape_string($conn, $news_active)."'
)") or die(mysqli_error($conn));

if($news_active == 1) {
mysqli_query($conn, "UPDATE users SET `news_count`=`news_count`+'1'") or die(mysqli_error($conn));
}
    $success = "News <strong>".$news_title."</strong> has been successfully added! Your users will be notified on-site.";
}
}
?>

<div class="container">
<div class="row">

<?php if(isset($error)) { echo "<div class=\"alert alert-error\"><h3>Errors:</h3><ul>".$error."</ul></div>"; } ?>
<?php if(isset($success)) { echo "<div class=\"alert alert-success\">".$success."</div>"; } ?>

<form class="form-horizontal" role="form" method="post">

<h1>Add a News Update</h1>

<div class="form-group">
<label class="control-label col-sm-2" for="news_title">News Title</label>
<div class="col-sm-10">
<input class="form-control" name="news_title" id="news_title" type="text" value="<?php if(isset($news_title)) { echo $news_title; } else { echo $news_title; }?>" required/>
<span class="help-block">Gotta have a news title.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="news_description">News Description</label>
<div class="col-sm-10">
<textarea rows="15" class="form-control" name="news_description" id="news_description" required><?php if(isset($news_description)) { echo $news_description; } else { echo $news_description; } ?></textarea>
<span class="help-block">Enter the contents of your news description (HTML is allowed.)</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="news_active">News Status</label>
<div class="col-sm-10">
<select class="form-control" name="news_active" id="news_active">
<option value="0">Disabled</option>
<option value="1">Active</option>
</select>
<span class="help-block">Enable or disable this news update</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="add_news" type="submit" value="Add News Update"/>
</div>
</div>
</form>

</div>
</div>
<link rel="stylesheet" href="<?php echo $config['base_url']; ?>/admin_cp/assets/js/sceditor/minified/themes/square.min.css" />

    <script src="<?php echo $config['base_url']; ?>admin_cp/assets/js/sceditor/minified/sceditor.min.js.php"></script>
<script src="<?php echo $config['base_url']; ?>/admin_cp/assets/js/sceditor/minified/formats/xhtml.js"></script>
<script>
// Replace the textarea #example with SCEditor
var textarea = document.getElementById('news_description');
sceditor.create(textarea, {
	format: 'xhtml',
	style: '<?php echo $config['base_url']; ?>/admin_cp/assets/js/sceditor/minified/themes/content/default.min.css'
});
</script>

<?php include("footer.php"); ?>