<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['site_url']."admin_cp/login.php'</script>";
    exit;
}

$edit_id = intval(str_clean($_GET['id']));

$get_user = mysqli_query($conn, "SELECT * FROM `users` WHERE `user_id`='".mysqli_real_escape_string($conn, $edit_id)."' LIMIT 1") or die(mysqli_error($conn));

if(empty($edit_id) || $edit_id == "" || !$edit_id) {
$error = "You never selected a user to edit.";
} else if(mysqli_num_rows($get_user) == 0) {
$error = "This user doesn't exist.";
} else { 

if(isset($_POST['edit_user'])) {

$username = str_clean($_POST['username']);

$email = str_clean($_POST['email']);
$payment_email = str_clean($_POST['payment_email']);
$points = str_clean($_POST['points']);
$advertiser_points = str_clean($_POST['advertiser_points']);
$daily_bonus = str_clean($_POST['daily_bonus']);
$daily_offers = str_clean($_POST['daily_offers']);
$daily_bonus_offers = str_clean($_POST['daily_bonus_offers']);
$banned = str_clean($_POST['banned']);
$banned_reason = str_clean($_POST['banned_reason']);


if(empty($username)) {
$error .= "You left the username blank.";
}

if(!preg_match("/^[a-z0-9]+$/i", $username)) {
$error .= "Username must contain only letters and numbers.";
}

if(strlen($username) > 20) {
$error .= "Username must be shorter than 20 characters";
}

if(strlen($username) < 5) {
$error .= "Username must be greater than 5 characters";
}

if(empty($email)) {
$error .= "You left the email blank.";
}

if(!filter_var($email, FILTER_VALIDATE_EMAIL)) { 
$error .= "Invalid email format. Valid: <i>admin@example.com</i>";
}

if(!is_numeric($points)) {
$error .= "Please enter only numbers for points.";
}

if(!is_numeric($advertiser_points)) {
$error .= "Please enter only numbers for advertiser points.";
}

if(!is_numeric($daily_offers)) {
$error .= "Please enter only numbers for daily offers count.";
}

if(!is_numeric($daily_bonus_offers)) {
$error .= "Please enter only numbers for daily bonus offers count.";
}

if($daily_bonus != "0" && $daily_bonus != "1") {
$error .= "Please select whether the user claimed the daily bonus or not.";
}

if($banned != "0" && $banned != "1") {
$error .= "Is ".$username." active or banned?";
}

if(!$error) {
mysqli_query($conn, "UPDATE `users` SET 
`username`='".mysqli_real_escape_string($conn, $username)."',
`email`='".mysqli_real_escape_string($conn, $email)."',
`points`='".mysqli_real_escape_string($conn, $points)."',
`advertiser_points`='".mysqli_real_escape_string($conn, $advertiser_points)."',
`banned`='".mysqli_real_escape_string($conn, $banned)."',
`banned_reason`='".mysqli_real_escape_string($conn, $banned_reason)."', 
`payment_email`='".mysqli_real_escape_string($conn, $payment_email)."',
`daily_bonus`='".mysqli_real_escape_string($conn, $daily_bonus)."',
`daily_offers`='".mysqli_real_escape_string($conn, $daily_offers)."',
`daily_bonus_offers`='".mysqli_real_escape_string($conn, $daily_bonus_offers)."'
 WHERE `user_id`='".mysqli_real_escape_string($conn, $edit_id)."' LIMIT 1") or die(mysqli_error($conn));
    $success = "<strong>".$username."</strong>'s account has been successfully updated!";
}

}

}
?>

<div class="container">

<?php if(isset($error)) { ?><div class="alert alert-danger"><h2 class="no-top-padding">Errors:</h2><ul><?php echo $error; ?></ul></div><?php } ?>
<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<?php $row = mysqli_fetch_array($get_user); ?>

<form class="form-horizontal" role="form" method="post">

<h1 class="page-header">Editing User: <?php echo $row['username']; ?></h1>

<div class="form-group">
<label class="control-label col-sm-2" for="username">Username</label>
<div class="col-sm-10">
<input class="form-control" name="username" id="username" type="text" value="<?php if(isset($row['username'])) { echo $row['username']; } ?>" required/>
<span class="help-block">Gotta have a username.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="email">Email</label>
<div class="col-sm-10">
<input class="form-control" name="email" id="email" type="text" value="<?php if(isset($row['email'])) { echo $row['email']; } ?>" required/>
<span class="help-block">Update user's email address?</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="payment_email">Payment Email</label>
<div class="col-sm-10">
<input class="form-control" name="payment_email" id="payment_email" type="text" value="<?php if(isset($row['payment_email'])) { echo $row['payment_email']; } ?>" />
<span class="help-block">Update user's payment email address?</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="points">Points</label>
<div class="col-sm-10">
<input class="form-control" name="points" id="points" type="text" value="<?php if(isset($row['points'])) { echo $row['points']; } ?>" />
<span class="help-block">Update the user's points balance.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="advertiser_points">Advertiser Points</label>
<div class="col-sm-10">
<input class="form-control" name="advertiser_points" id="advertiser_points" type="text" value="<?php if(isset($row['advertiser_points'])) { echo $row['advertiser_points']; } ?>" />
<span class="help-block">Update the user's advertiser point balance.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="daily_offers">Daily Offers</label>
<div class="col-sm-10">
<input class="form-control" name="daily_offers" id="daily_offers" type="text" value="<?php if(isset($row['daily_offers'])) { echo $row['daily_offers']; } ?>" />
<span class="help-block">Update the user's daily offer count.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="daily_bonus_offers">Daily Bonus Offers</label>
<div class="col-sm-10">
<input class="form-control" name="daily_bonus_offers" id="daily_bonus_offers" type="text" value="<?php if(isset($row['daily_bonus_offers'])) { echo $row['daily_bonus_offers']; } ?>" />
<span class="help-block">Update the user's daily bonus offer count. This is used if you have the daily bonus feature set to active.</span>
</div>
</div>


<div class="form-group">
<label class="control-label col-sm-2" for="active">Daily Bonus</label>
<div class="col-sm-10">
<select class="form-control" name="daily_bonus" id="daily_bonus">
<option value="0" <?php if($row['daily_bonus'] == 0) { ?>selected<?php } ?>>Unclaimed</option>
<option value="1" <?php if($row['daily_bonus'] == 1) { ?>selected<?php } ?>>Claimed</option>
</select>
<span class="help-block">Update user daily bonus status.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="active">Active</label>
<div class="col-sm-10">
<select class="form-control" name="banned" id="banned">
<option value="0" <?php if($row['banned'] == 0) { ?>selected<?php } ?>>Active</option>
<option value="1" <?php if($row['banned'] == 1) { ?>selected<?php } ?>>Banned</option>
</select>
<input class="form-control" name="banned_reason" id="banned_reason" type="text" value="<?php if(isset($row['banned_reason'])) { echo $row['banned_reason']; } ?>" placeholder="Leave this field blank if user is not banned." />
<span class="help-block">Set a banned reason if the user is banned.</span>
<span class="help-block">This user is currently set to: <?php if($row['banned'] == 1) { echo "<strong class=\"text text-danger\">Banned</strong>"; } else { echo "<strong class=\"text text-success\"><i>Active</i></strong>"; } ?></span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="edit_user" type="submit" value="Edit User"/>
</div>
</div>
</form>

<?php
$get_user_iplogs = mysqli_query($conn, "SELECT * FROM users_ip_logs WHERE user_id='".mysqli_real_escape_string($conn, $row['user_id'])."' AND date_added BETWEEN NOW() - INTERVAL 30 DAY AND NOW()");
?>
<h2 class="page-header">User IP Logs</h2>
<div class="alert alert-info">List all the user's IP addresses associated with the account during the last 30 days.</div>

<?php if(mysqli_num_rows($get_user_iplogs) == 0) { ?>
<div class="alert alert-danger">There are no IP addresses associated with the account during the last 30 days.</div>
<?php } else { ?>
<table class="table table-condensed">
<tr>
<th>IP Address</th>
<th>Date Added</th>
<th>Actions</th>
</tr>
<?php while($ip_row = mysqli_fetch_array($get_user_iplogs)) { ?>
<tr>
<td><?php echo $ip_row['ip_address']; ?></td>
<td><?php echo $ip_row['date_added']; ?></td>
<td><a class="btn btn-xs btn-primary" target="_blank" href="<?php echo $config['site_url']; ?>admin_cp/add_ip_ban.php?ban_ip_address=<?php echo $ip_row['ip_address']; ?>">Ban IP</a></td>
</tr>
<?php } ?>
</table>
<?php } ?>


</div>

<?php include("footer.php"); ?>