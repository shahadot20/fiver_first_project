<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include('header.php');

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

if(isset($_GET['delete_payment_provider'])) {
$success = "Payment provider was deleted.";
}

?>

<div class="container">

<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<h1>Cryptocurrency Providers <a class="pull-right btn btn-success" href="<?php echo $config['base_url']; ?>admin_cp/add_crypto_provider.php">Add Cryptocurrency Provider</a></h1>

<p>This page list all your current cryptocurrency payment providers.</p>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Cryptocurrency Providers</h3>
  </div>

<table class="table">
<tr>
<th>Name</th>
<th>Fee</th>
<th>Status</th>
<th>Actions</th>
</tr>


<?php
$list_providers = mysqli_query($conn, "SELECT * FROM `cryptocurrency_providers` ORDER BY `provider_name` ASC") or die(mysqli_error($conn));
while($row = mysqli_fetch_array($list_providers)) {
?>
<tr>
<td><?php echo $row['provider_name']; ?></td>
<td><?php echo $row['provider_fee']."%"; ?></td>
<td><?php if($row['provider_active'] == 1) { echo "<span class=\"text-success\">Active</span>"; } else { echo "<span class=\"text-danger\">Disabled</span>"; } ?></td>
<td><a class="btn btn-xs btn-primary" href="<?php echo $config['base_url']; ?>admin_cp/edit_crypto_provider.php?c_id=<?php echo $row['provider_id']; ?>">Edit</a> 
<a class="btn btn-xs btn-primary" target="_blank" href="<?php echo $config['base_url']; ?>redeem/crypto/<?php echo $row['provider_seo_name']; ?>">View</a></td>
</tr>
<?php } ?>
</table>


</div>

</div>

<?php include('footer.php'); ?>