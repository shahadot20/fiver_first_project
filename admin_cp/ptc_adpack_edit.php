<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['site_url']."admin_cp/login.php'</script>";
    exit;
}

$pack_id = intval(str_clean($_GET['id']));

$get_packs = mysqli_query($conn, "SELECT * FROM `ptc_adpacks` WHERE `ptc_adpack_id`='".mysqli_real_escape_string($conn, $pack_id)."' LIMIT 1") or die(mysqli_error($conn));

if(empty($pack_id) || $pack_id == "" || !$pack_id) {
$error = "You never selected a PTC adpack to edit.";
} else if(mysqli_num_rows($get_packs) == 0) {
$error = "No record of this PTC adpack.";
} else { 

if(isset($_POST['edit_adpack'])) {

$ptc_adpack_name = str_clean($_POST['ptc_adpack_name']);
$ptc_adpack_points = intval(str_clean($_POST['ptc_adpack_points']));
$ptc_adpack_price = intval(str_clean($_POST['ptc_adpack_price']));

if(empty($ptc_adpack_name)) {
$error .= "You left the adpack name blank.";
}

if(empty($ptc_adpack_points)) {
$error .= "You left the adpack points blank.";
}

if(!is_numeric($ptc_adpack_points)) {
$error .= "Invalid value for adpack points. Points must be an integer.";
}

if(empty($ptc_adpack_price)) {
$error .= "You left the adpack price blank.";
}

if(!is_numeric($ptc_adpack_price)) {
$error .= "Invalid value for adpack price. Price must be an integer.";
}

if(!$error) {
mysqli_query($conn, "UPDATE `ptc_adpacks` SET 
`ptc_adpack_name`='".mysqli_real_escape_string($conn, $ptc_adpack_name)."',
`ptc_adpack_points`='".mysqli_real_escape_string($conn, $ptc_adpack_points)."',
`ptc_adpack_price`='".mysqli_real_escape_string($conn, $ptc_adpack_price)."'
 WHERE `ptc_adpack_id`='".mysqli_real_escape_string($conn, $pack_id)."' LIMIT 1") or die(mysqli_error($conn));
    $success = "<strong>".$ptc_adpack_name."</strong> has been successfully updated!";
}

}

}
?>

<div class="container">

<?php if(isset($error)) { ?><div class="alert alert-danger"><?php echo $error; ?></div><?php } ?>
<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<?php while($row = mysqli_fetch_array($get_packs)) { ?>

<form class="form-horizontal" role="form" method="post">

<h1 class="page-header">Editing Adpack: <?php echo $row['ptc_adpack_name']; ?></h1>

<div class="form-group">
<label class="control-label col-sm-2" for="ip">Adpack Name</label>
<div class="col-sm-10">
<input class="form-control" name="ptc_adpack_name" id="ptc_adpack_name" type="text" value="<?php if(isset($row['ptc_adpack_name'])) { echo $row['ptc_adpack_name']; } ?>" required/>
<span class="help-block">Name of your adpack.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ptc_adpack_points">Adpack Points</label>
<div class="col-sm-10">
<input class="form-control" name="ptc_adpack_points" id="ptc_adpack_points" type="text" value="<?php if(isset($row['ptc_adpack_points'])) { echo $row['ptc_adpack_points']; } ?>" required/>
<span class="help-block">Amount of points for this adpack.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ptc_adpack_points">Adpack Price</label>
<div class="col-sm-10">
<input class="form-control" name="ptc_adpack_price" id="ptc_adpack_price" type="text" value="<?php if(isset($row['ptc_adpack_price'])) { echo $row['ptc_adpack_price']; } ?>" required/>
<span class="help-block">Price for this adpack.</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="edit_adpack" type="submit" value="Edit Adpack"/>
</div>
</div>
</form>

<?php } ?>

</div>

<?php include("footer.php"); ?>