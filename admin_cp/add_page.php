<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

if(isset($_POST['add_page'])) {

$page_name = str_clean($_POST['page_name']);
$page_description = str_clean($_POST['page_description']);
$page_seo_name = url_slug($_POST['page_name']);
$page_active =  str_clean($_POST['page_active']);

$page_sql = mysqli_query($conn, "SELECT `page_name` FROM `pages` WHERE `page_name`='".mysqli_real_escape_string($conn, $page_name)."'") or die(mysqli_error($conn));
$page_exists = mysqli_num_rows($page_sql);

if(empty($page_name)) {
$error = "<li>You left the page name blank.</li>";
}

if(empty($page_description)) {
$error .= "<li>You left the page description blank.</li>";
}

if($page_active == "") {
$error .= "<li>You need to select if page is active or not.</li>";
}

if(!is_numeric($page_active)) {
$error .= "<li>You need to enter 0(disabled) or 1(active).</li>";
}

if ($page_exists > 0)  {
$error .= "<li>".$page_name." is already in the database!</li>";
}

if($page_active != "0" && $page_active != "1") {
$error .= "<li>Is ".$page_name." active or disabled?</li>";

}

if(!$error) {
mysqli_query($conn, "INSERT INTO `pages`(page_id,page_name,page_seo_name,page_description,page_active) values(NULL, 
'".mysqli_real_escape_string($conn, $page_name)."',
'".mysqli_real_escape_string($conn, $page_seo_name)."',
'".mysqli_real_escape_string($conn, $page_description)."',
'".mysqli_real_escape_string($conn, $page_active)."'
)") or die(mysqli_error($conn));
    $success = "Page <strong>".$page_name."</strong> has been successfully added!";
}
}
?>

<div class="container">
<div class="row">

<?php if(isset($error)) { echo "<div class=\"alert alert-error\"><h3>Errors:</h3><ul>".$error."</ul></div>"; } ?>
<?php if(isset($success)) { echo "<div class=\"alert alert-success\">".$success."</div>"; } ?>

<form class="form-horizontal" role="form" method="post">

<h1>Add a Page</h1>

<div class="form-group">
<label class="control-label col-sm-2" for="page_name">Page Name</label>
<div class="col-sm-10">
<input class="form-control" name="page_name" id="page_name" type="text" value="<?php if(isset($page_name)) { echo $page_name; } else { echo $page_name; }?>" required/>
<span class="help-block">Gotta have a page name.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="page_description">Page Description</label>
<div class="col-sm-10">
<textarea rows="15" class="form-control" name="page_description" id="page_description" required/><?php if(isset($page_description)) { echo $page_description; } else { echo $page_description; } ?></textarea>
<span class="help-block">Enter the contents of your page description (HTML is allowed.)</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="page_active">Page Active</label>
<div class="col-sm-10">
<select class="form-control" name="page_active" id="page_active">
<option value="0">Disabled</option>
<option value="1">Active</option>
</select>
<span class="help-block">Enable or deactivate a page</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="add_page" type="submit" value="Add Page"/>
</div>
</div>
</form>

</div>
</div>

<link rel="stylesheet" href="<?php echo $config['base_url']; ?>/admin_cp/assets/js/sceditor/minified/themes/square.min.css" />

    <script src="<?php echo $config['base_url']; ?>admin_cp/assets/js/sceditor/minified/sceditor.min.js.php"></script>
<script src="<?php echo $config['base_url']; ?>/admin_cp/assets/js/sceditor/minified/formats/xhtml.js"></script>
<script>
// Replace the textarea #example with SCEditor
var textarea = document.getElementById('page_description');
sceditor.create(textarea, {
	format: 'xhtml',
	style: '<?php echo $config['base_url']; ?>/admin_cp/assets/js/sceditor/minified/themes/content/default.min.css'
});
</script>

<?php include("footer.php"); ?>