<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

if(isset($_POST['add_chat_ban'])) {

$ban_username = str_clean($_POST['ban_username']);
$ban_reason = str_clean($_POST['ban_reason']);

$ban_exist_sql = mysqli_query($conn, "SELECT `ban_username` FROM `chat_bans` WHERE `ban_username`='".mysqli_real_escape_string($conn, $ban_username)."'") or die(mysqli_error($conn));
$ban_exists = mysqli_num_rows($ban_exist_sql);

$user_exist_sql = mysqli_query($conn, "SELECT `username` FROM `users` WHERE `username`='".mysqli_real_escape_string($conn, $ban_username)."'") or die(mysqli_error($conn));
$user_exists = mysqli_num_rows($user_exist_sql);

if(empty($ban_username)) {
$error = "<li>You left the ban username blank.</li>";
}

if(empty($ban_reason)) {
$error .= "<li>You left the ban reason blank.</li>";
}

if ($user_exists == 0)  {
$error .= "<li>".$ban_username." doesn't exist in the database!</li>";
}

if ($ban_exists > 0)  {
$error .= "<li>".$ban_username." is already in the database!</li>";
}


if(!$error) {
mysqli_query($conn, "INSERT INTO `chat_bans`(ban_id,ban_username,ban_reason,ban_banner,ban_date_added) values(NULL, 
'".mysqli_real_escape_string($conn, $ban_username)."',
'".mysqli_real_escape_string($conn, $ban_reason)."',
'".mysqli_real_escape_string($conn, $_SESSION['admin_username'])."',
NOW()
)") or die(mysqli_error($conn));
    $success = "<strong>".$ban_username."</strong> has been successfully been banned from chat!";
}
}
?>

<div class="container">
<div class="row">

<?php if(isset($error)) { echo "<div class=\"alert alert-error\"><h3>Errors:</h3><ul>".$error."</ul></div>"; } ?>
<?php if(isset($success)) { echo "<div class=\"alert alert-success\">".$success."</div>"; } ?>

<form class="form-horizontal" role="form" method="post">

<h1>Adding New Chat Ban</h1>

<div class="form-group">
<label class="control-label col-sm-2" for="username">Username</label>
<div class="col-sm-10">
<input class="form-control" name="ban_username" id="ban_username" type="text" value="<?php if(isset($ban_username)) { echo $ban_username; } else { echo $_GET['ban_username']; } ?>" required/>
<span class="help-block">Gotta have an username to ban.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ban_reason">Ban Reason</label>
<div class="col-sm-10">
<input class="form-control" name="ban_reason" id="ban_reason" type="text" value="<?php if(isset($ban_reason)) { echo $ban_reason; } else { echo $ban_reason; } ?>" required/>
<span class="help-block">Gotta have a reason to ban a user from chat.</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="add_chat_ban" type="submit" value="Ban User from Chat"/>
</div>
</div>
</form>

</div>
</div>

<?php include("footer.php"); ?>