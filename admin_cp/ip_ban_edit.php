<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['site_url']."admin_cp/login.php'</script>";
    exit;
}

$get_ban_id = intval(str_clean($_GET['id']));

$get_ip = mysqli_query($conn, "SELECT * FROM `banned_ips` WHERE `ban_id`='".mysqli_real_escape_string($conn, $get_ban_id)."' LIMIT 1") or die(mysqli_error($conn));

if(empty($get_ban_id) || $get_ban_id == "" || !$get_ban_id) {
$error = "You never selected a IP Address to edit.";
} else if(mysqli_num_rows($get_ip) == 0) {
$error = "No record of this ban entry.";
} else { 

if(isset($_POST['edit_ban'])) {

$ip_ban = str_clean($_POST['ban_ip_address']);
$banned_reason = str_clean($_POST['ban_reason']);

if(empty($ip_ban)) {
$error .= "You left the IP address blank.";
}

if(empty($banned_reason)) {
$error .= "You left the ban reason blank.";
}

if(!$error) {
mysqli_query($conn, "UPDATE `banned_ips` SET 
`ban_ip_address`='".mysqli_real_escape_string($conn, $ip_ban)."',
`ban_reason`='".mysqli_real_escape_string($conn, $banned_reason)."'
 WHERE `ban_id`='".mysqli_real_escape_string($conn, $get_ban_id)."' LIMIT 1") or die(mysqli_error($conn));
    $success = "<strong>".$ip_ban."</strong> has been successfully updated!";
}

}

}
?>

<div class="container">

<?php if(isset($error)) { ?><div class="alert alert-danger"><?php echo $error; ?></div><?php } ?>
<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<?php while($row = mysqli_fetch_array($get_ip)) { ?>

<form class="form-horizontal" role="form" method="post">

<h1 class="page-header">Editing IP: <?php echo $row['ban_ip_address']; ?></h1>

<div class="form-group">
<label class="control-label col-sm-2" for="ip">IP</label>
<div class="col-sm-10">
<input class="form-control" name="ban_ip_address" id="ban_ip_address" type="text" value="<?php if(isset($row['ban_ip_address'])) { echo $row['ban_ip_address']; } ?>" required/>
<span class="help-block">Gotta have a IP.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ip">IP Ban Reason</label>
<div class="col-sm-10">
<input class="form-control" name="ban_reason" id="ban_reason" type="text" value="<?php if(isset($row['ban_reason'])) { echo $row['ban_reason']; } ?>" required/>
<span class="help-block">Gotta have a IP ban reason.</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="edit_ban" type="submit" value="Edit IP Ban"/>
</div>
</div>
</form>

<?php } ?>

</div>

<?php include("footer.php"); ?>