<?php
include('includes/config.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $config['site_name']; ?> - Admin CP</title>

    <!-- Bootstrap -->
    <link href="<?php echo $config['base_url']; ?>admin_cp/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $config['base_url']; ?>admin_cp/assets/css/style.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo $config['base_url']; ?>admin_cp/assets/js/sceditor/minified/themes/default.min.css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>
  <body>

<?php if(isset($admin['username'])) { ?>
<div class="navbar navbar-default navbar-fixed-top">
<div class="container">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="<?php echo $config['base_url']; ?>admin_cp/index.php">Admin Panel</a>
  </div>
  <div class="navbar-collapse collapse navbar-responsive-collapse">
    <ul class="nav navbar-nav">
          
    </ul>
    
    <ul class="nav navbar-nav navbar-right">
      
      <li class="dropdown">
          <a href="<?php echo $config['base_url']; ?>admin_cp/edit_settings.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">Settings <span class="caret"></span></a>
                                  <ul class="dropdown-menu" role="menu">
      <li><a href="<?php echo $config['base_url']; ?>admin_cp/edit_settings.php">Site Settings</a></li>
                               <div class="dropdown-divider"></div>
                        <h6 class="dropdown-header">Chat</h6>
            <li><a href="<?php echo $config['base_url']; ?>admin_cp/chat_logs.php">Chat Logs</a></li>
            <li><a href="<?php echo $config['base_url']; ?>admin_cp/chat_ban_logs.php">Chat Ban Logs</a></li>
            <li><a href="<?php echo $config['base_url']; ?>admin_cp/add_chat_ban.php">Chat Ban User</a></li>

                         <div class="dropdown-divider"></div>
                        <h6 class="dropdown-header">Security</h6>
                  <li><a href="<?php echo $config['base_url']; ?>admin_cp/add_ip_ban.php">Ban IP</a></li>
                  <li><a href="<?php echo $config['base_url']; ?>admin_cp/ban_ip_logs.php">Ban IP Logs</a></li>


                             </ul>
                      </li>
                      
                                            <li class="dropdown">
                        <a href="<?php echo $config['base_url']; ?>admin_cp/users.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">Ads <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/add_ptc_adpack.php">Add PTC Adpack</a></li>
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/ptc_ad_packs.php">PTC Ad Packs</a></li>
                        <div class="dropdown-divider"></div>
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/ptc_ads.php">PTC Ads</a></li>
                        <div class="dropdown-divider"></div>
                        <h6 class="dropdown-header">HTML Ads</h6>
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/add_advertisement.php">Add HTML Ad</a></li>
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/advertisements.php">HTML Ads</a></li>
                        </ul>
                      </li>
                      
                        <li class="dropdown">
                        <a href="<?php echo $config['base_url']; ?>admin_cp/users.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">Users <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/users.php">View Users</a></li>
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/users_search.php">Search Users</a></li>
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/users_payouts.php">Payout Users</a></li>
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/users_coupons.php">Users Coupons</a></li>
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/users_transactions.php">Users Transactions</a></li>
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/users_reversed_offers.php">Users Reversed Offers</a></li>
                        <div class="dropdown-divider"></div>
                        <h6 class="dropdown-header">History</h6>
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/users_history_activity.php">Activity History</a></li>
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/users_history_offers.php">Offer History</a></li>

                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/users_history_dailybonus.php">Daily Bonus History</a></li>
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/users_history_coupons.php">Coupon History</a></li>
                        </ul>
                      </li>
                      <li class="dropdown">
                        <a href="<?php echo $config['base_url']; ?>admin_cp/users.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">Admins <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/admins.php">View Admins</a></li>
                        <div class="dropdown-divider"></div>
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/add_admin.php">Add Admin</a></li>

                        </ul>
                      </li>
                      
                            <li class="dropdown">
                        <a href="<?php echo $config['base_url']; ?>admin_cp/networks.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">Networks <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/networks.php">View Networks</a></li>
<li><a href="<?php echo $config['base_url']; ?>admin_cp/add_network.php">Add Network</a></li>
                        </ul>
                      </li>
                            <li class="dropdown">
                        <a href="<?php echo $config['base_url']; ?>admin_cp/payment_providers.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">Payment Providers <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                        <div class="dropdown-divider"></div>
                        <h6 class="dropdown-header">Cash</h6>
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/payment_providers.php">View Cash Providers</a></li>
<li><a href="<?php echo $config['base_url']; ?>admin_cp/add_cash_provider.php">Add Cash Provider</a></li>
                        <div class="dropdown-divider"></div>
                        <h6 class="dropdown-header">Cryptocurrency</h6>
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/cryptocurrency_providers.php">View Cryptocurrency Providers</a></li>

<li><a href="<?php echo $config['base_url']; ?>admin_cp/add_crypto_provider.php">Add Cyrptocurrency Provider</a></li>
                        </ul>
                      </li>
                      
                            <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">Games <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/dailybonus_settings.php">Daily Bonus Settings</a></li>
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/lottery_settings.php">Lottery Settings</a></li>
                        </ul>
                      </li>
                      
                            <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">Pages <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/pages.php">View Pages</a></li>
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/add_page.php">Add Page</a></li>
                        </ul>
                      </li>
                            <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">News <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/news.php">View News</a></li>
                        <li><a href="<?php echo $config['base_url']; ?>admin_cp/add_news.php">Add News</a></li>
                        </ul>
                      </li>
            <li class="dropdown">
          <a href="<?php echo $config['base_url']; ?>admin_cp/edit_settings.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true"><?php echo $admin['username']; ?> <span class="caret"></span></a>
                                  <ul class="dropdown-menu" role="menu">
      <li><a href="<?php echo $config['base_url']; ?>admin_cp/dashboard.php">Dashboard</a></li>
            <li><a href="<?php echo $config['base_url']; ?>admin_cp/logout.php">Logout</a></li>
                             </ul>
                      </li>
    </ul>
  </div>
</div>
</div>
<?php } ?>

<div class="pull-down-container"></div>