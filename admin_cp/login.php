<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include('header.php');

if(isset($_POST['user_login'])) {

$admins_username = mysqli_real_escape_string($conn, str_clean($_POST['admin_username']));
$admins_password = mysqli_real_escape_string($conn, md5($_POST['admin_password']));

$check_login = mysqli_query($conn, "SELECT username,password FROM `admins` WHERE `username`='".$admins_username."' AND `password`='".$admins_password."'") or die(mysqli_error($conn));
$check_exists = mysqli_num_rows($check_login);

if($check_exists == 0) {
$message = "<div class=\"alert alert-danger\">No account exists with that username/password combination.</div>";
} else if(empty($_POST['admin_username']) || $_POST['admin_username'] == "") {
$message = "<div class=\"alert alert-danger\">Username is empty, try again.</div>";
} else if(empty($_POST['admin_password']) || $_POST['admin_password'] == "") {
$message = "<div class=\"alert alert-danger\">Password is empty, try again.</div>";
} else if($check_exists == 1) {
mysqli_query($conn, "UPDATE `admins` SET `online`=NOW() WHERE `username`='".$admins_username."' LIMIT 1") or die(mysqli_error($conn));
$_SESSION['admin_username'] = $admins_username;
echo "<script>document.location.href='".$config['site_url']."admin_cp/dashboard.php'</script>";
} else {
$message = "<div class=\"alert alert-danger\">Invalid login details. Try again.</div>";
}
}

?>

<div class="container">

      <form class="form-signin" role="form" method="post" action="">
        <h2 class="form-signin-heading text-center">Login</h2>

<?php if(isset($message)) echo $message; ?>

<div class="alert alert-info">Log into your Admin CP.</div>
        <label for="inputEmail" class="sr-only">Username</label>
        <input type="text" name="admin_username" id="inputUsername" class="form-control" placeholder="Username" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="admin_password" id="inputPassword" class="form-control" placeholder="Password" required>

        <button class="btn btn-lg btn-primary btn-block" name="user_login" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->

<?php include('footer.php'); ?>