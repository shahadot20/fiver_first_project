<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

$edit_id = str_clean($_GET['id']);

$get_user = mysqli_query($conn, "SELECT * FROM `admins` WHERE `user_id`='".mysqli_real_escape_string($conn, $edit_id)."' LIMIT 1") or die(mysqli_error($conn));

if(empty($edit_id) || $edit_id == "" || !$edit_id) {
$error = "You never selected an admin to edit.";
} else if(mysqli_num_rows($get_user) == 0) {
$error = "This admin doesn't exist.";
} else { 

if(isset($_POST['edit_user'])) {

$username = str_clean($_POST['username']);

$get_user_sql = mysqli_query($conn, "SELECT `user_id`,`username` FROM `admins` WHERE `username`='".mysqli_real_escape_string($conn, $username)."'") or die(mysqli_error($conn));
$user_exists = mysqli_num_rows($get_user_sql);

if(empty($username)) {
$error .= "You left the username blank.";
}

if(!preg_match("/^[a-z0-9]+$/i", $username)) {
$error .= "Username must contain only letters and numbers.";
}

if(strlen($username) > 20) {
$error .= "Username must be shorter than 20 characters";
}

if(strlen($username) < 5) {
$error .= "Username must be greater than 5 characters";
}

if(!$error) {
mysqli_query($conn, "UPDATE `admins` SET 
`username`='".mysqli_real_escape_string($conn, $username)."' WHERE `user_id`='".mysqli_real_escape_string($conn, $edit_id)."' LIMIT 1") or die(mysqli_error($conn));
    $success = "<strong>".$username."</strong>'s account has been successfully updated!";
    session_destroy();
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
    }

}

if(isset($_POST['update_password'])) {

$password = md5($_POST['password']);
$new_password = md5($_POST['new_password']);

if(empty($password)) {
$error = "Please enter your password.<br />";
}

if(empty($new_password)) {
$error = "Please enter your new password.<br />";
}

if($new_password == $admin['password']) { 
$error = "No use in using the same password..<br />";
}

if($admin['password'] != $password) {
$error = "Wrong password.. try again.<br />";
}

if(!$error) {
$update_password = md5($new_password);
mysqli_query($conn, "UPDATE `admins` SET 
`password`='".mysqli_real_escape_string($conn, $new_password)."' WHERE `user_id`='".mysqli_real_escape_string($conn, $edit_id)."' LIMIT 1") or die(mysqli_error($conn));
$success = "Account Password updated!";
}

}

}
?>

<div class="container">

<?php if(isset($error)) { ?><div class="alert alert-danger"><?php echo $error; ?></div><?php } ?>
<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<?php while($row = mysqli_fetch_array($get_user)) { ?>

<form class="form-horizontal" role="form" method="post">

<h1 class="page-header">Editing Admin: <?php echo $row['username']; ?></h1>

<div class="form-group">
<label class="control-label col-sm-2" for="username">Username</label>
<div class="col-sm-10">
<input class="form-control" name="username" id="username" type="text" value="<?php if(isset($row['username'])) { echo $row['username']; } else { echo $username; }?>" required/>
<span class="help-block">Gotta have a username. <strong>Notice: </strong>If you change the username you'll be logged out afterwards!</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="edit_user" type="submit" value="Edit User"/>
</div>
</div>
</form>

<form class="form-horizontal" role="form" method="post">

<h2>Update your Password</h2>

<div class="alert alert-warning"><p class="text-warning">
CREATE A NEW PASSWORD YOU NEVER USED BEFORE FOR ULTIMATE PROTECTION.</p></div>

<div class="form-group">
<label class="control-label col-sm-2" for="password">Password</label>
<div class="col-sm-10">
<input class="form-control" name="password" id="password" type="text" value=""/>
<span class="help-block">Your account's current password.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="new_password">New Password</label>
<div class="col-sm-10">
<input class="form-control" name="new_password" id="new_password" type="text" value="" />
<span class="help-block">Your account's new password.</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="update_password" type="submit" value="Update Password"/>
</div>
</div>

</form>

<?php } ?>

</div>

<?php include("footer.php"); ?>