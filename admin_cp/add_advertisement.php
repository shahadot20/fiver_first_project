<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

if(isset($_POST['add_advertisement'])) {

$ad_name = str_clean($_POST['ad_name']);
$ad_html_code = str_clean($_POST['ad_html_code']);
$ad_active = str_clean($_POST['ad_active']);


if(empty($ad_name)) {
$error = "<li>You left the advertisement name blank.</li>";
}

if(empty($ad_html_code)) {
$error .= "<li>You left the advertisement code blank.</li>";
}

if($ad_active != 0 && $ad_active != 1) {
$error .= "<li>Is the advertisement active or disabled?</li>";
}

if(!$error) {
mysqli_query($conn, "INSERT INTO `advertisements`(ad_id,ad_name,ad_html_code,ad_active) values(NULL, 
'".mysqli_real_escape_string($conn, $ad_name)."',
'".mysqli_real_escape_string($conn, $ad_html_code)."',
'".mysqli_real_escape_string($conn, intval($ad_active))."'
)") or die(mysqli_error($conn));
    $success = "Advertisement has been successfully been added!";
}
}
?>

<div class="container">
<div class="row">

<?php if(isset($error)) { echo "<div class=\"alert alert-error\"><h3>Errors:</h3><ul>".$error."</ul></div>"; } ?>
<?php if(isset($success)) { echo "<div class=\"alert alert-success\">".$success."</div>"; } ?>

<form class="form-horizontal" role="form" method="post">

<h1>Adding Advertisement</h1>

<div class="form-group">
<label class="control-label col-sm-2" for="ad_name">Ad Name</label>
<div class="col-sm-10">
<input type="text" class="form-control" name="ad_name" id="ad_name" value="<?php if(isset($ad_name)) { echo $ad_name; } else { echo $_GET['ad_name']; } ?>" required>
<span class="help-block">Gotta have the ad name.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ad_html_code">Ad HTML Code</label>
<div class="col-sm-10">
<textarea rows="4" class="form-control" name="ad_html_code" id="ad_html_code" required><?php if(isset($ad_html_code)) { echo $ad_html_code; } else { echo $_GET['ad_html_code']; } ?></textarea>
<span class="help-block">Gotta have the ad HTML code.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="ban_reason">Ad Status</label>
<div class="col-sm-10">
<select class="form-control" name="ad_active" id="ad_active">
<option value="0">Disabled</option>
<option value="1">Active</option>
</select>
<span class="help-block">Is the ad disabled or active?</span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="add_advertisement" type="submit" value="Add Advertisement"/>
</div>
</div>
</form>

</div>
</div>

<?php include("footer.php"); ?>