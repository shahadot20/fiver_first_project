<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

$get_ban_username = str_clean($_GET['ban_username']);

$get_username = mysqli_query($conn, "SELECT * FROM `chat_bans` WHERE `ban_username`='".mysqli_real_escape_string($conn, $get_ban_username)."' LIMIT 1") or die(mysqli_error($conn));

if(empty($get_ban_username) || $get_ban_username == "" || !$get_ban_username) {
$error = "You never selected a user to unban.";
} else if(mysqli_num_rows($get_username) == 0) {
$error = "No record of this ban entry.";
} else {

mysqli_query($conn, "DELETE FROM `chat_bans` WHERE `ban_username`='".mysqli_real_escape_string($conn, $get_ban_username)."'") or die(mysqli_error($conn));

$success = "This user has been unbanned/deleted from the chat ban.";

?>

<div class="container">

<?php if(isset($error)) { ?><div class="alert alert-danger"><?php echo $error; ?></div><?php } ?>
<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>


<?php } ?>

</div>

<?php include("footer.php"); ?>