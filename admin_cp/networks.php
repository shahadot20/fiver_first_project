<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include('header.php');

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['site_url']."admin_cp/login.php'</script>";
    exit;
}

if(isset($_GET['delete_network'])) {
$success = "Network was deleted.";
}

?>

<div class="container">

<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<h1>Networks <a class="pull-right btn btn-success" href="<?php echo $config['site_url']; ?>admin_cp/add_network.php">Add Networks</a></h1>

<p>This page list all your current networks.</p>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Networks</h3>
  </div>

<table class="table">
<tr>
<th>Network Name</th>
<th>Network Status</th>
<th>Network Actions</th>
</tr>


<?php
$list_networks = mysqli_query($conn, "SELECT * FROM `networks` ORDER BY `network_name` ASC") or die(mysqli_error($conn));
while($row = mysqli_fetch_array($list_networks)) {
?>
<tr>
<td><?php echo $row['network_name']; ?></td>
<td><?php if($row['network_active'] == 1) { echo "<span class=\"text-success\">Active</span>"; } else { echo "<span class=\"text-danger\">Disabled</span>"; } ?></td>
<td><a class="btn btn-xs btn-primary" href="<?php echo $config['site_url']; ?>admin_cp/edit_network.php?c_id=<?php echo $row['network_id']; ?>">Edit</a> 
<a class="btn btn-xs btn-primary" target="_blank" href="<?php echo $config['site_url']; ?>earn/<?php echo $row['network_seo_name']; ?>">View</a></td>
</tr>
<?php } ?>
</table>


</div>

</div>

<?php include('footer.php'); ?>