<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

$get_settings = mysqli_query($conn, "SELECT * FROM `config` WHERE `site_id`='1' LIMIT 1") or die(mysqli_error($conn));


if(isset($_POST['edit_settings'])) {

$site_name = str_clean($_POST['site_name']);
$site_description = str_clean($_POST['site_description']);
$site_url = str_clean($_POST['site_url']);
$site_theme = str_clean($_POST['site_theme']);
$site_signup_bonus = intval(str_clean($_POST['site_signup_bonus']));
$site_referral_bonus = intval(str_clean($_POST['site_referral_bonus']));
$site_referral_point_bonus = intval(str_clean($_POST['site_referral_point_bonus']));
$site_page_limit = intval(str_clean($_POST['site_page_limit']));
$site_currency = str_clean($_POST['site_currency']);
$site_point_conversion = intval(str_clean($_POST['site_point_conversion']));
$site_paypal_email = str_clean($_POST['site_paypal_email']);
$site_email = str_clean($_POST['site_email']);
$site_timezone = str_clean($_POST['site_timezone']);
$site_reversal_limit = str_clean($_POST['site_reversal_limit']);



if(empty($site_name)) {
$error = "You left the site name blank.<br />";
}

if(empty($site_description)) {
$error .= "You left the site description blank.<br />";
}

if(empty($site_url)) {
$error .= "You left the site URL blank.<br />";
}

if(empty($site_theme)) {
$error .= "You left the site theme blank.<br />";
}

if(empty($site_signup_bonus)) {
$error .= "You left the site signup bonus blank.<br />";
}

if(empty($site_currency)) {
$error .= "You left the site currency blank.<br />";
}

if(empty($site_timezone)) {
$error .= "You left the site timezone blank.<br />";
}

if(empty($site_reversal_limit)) {
$error .= "You left the site reversal limit blank.<br />";
}

if(!is_numeric($site_reversal_limit)) {
$error .= "The site reversal limit can only be numbers.<br />";
}

if(!is_numeric($site_signup_bonus)) {
$error .= "The site signup bonus can only be numbers.<br />";
}

if(empty($site_referral_bonus) && $site_referral_bonus != "0") {
$error .= "You left the site referral bonus blank.<br />";
}


if(!is_numeric($site_referral_bonus)) {
$error .= "The site referral bonus can only contain numbers.<br />";
}

if(empty($site_referral_point_bonus) && $site_referral_point_bonus != "0") {
$error .= "You left the site referral point bonus blank.<br />";
}


if(!is_numeric($site_referral_point_bonus)) {
$error .= "The site referral point bonus can only contain numbers.<br />";
}

if(empty($site_page_limit)) {
$error .= "You left the site page limit blank.<br />";
}


if(!is_numeric($site_page_limit)) {
$error .= "The page limit can only contain numbers.<br />";
}

if(strlen($site_description) > 200) {
$error .= "Don't go past 200 characters for the site description.<br />";
}

if(!is_numeric($site_point_conversion) && empty($site_point_conversion)) {
$error .= "You need to add the point conversion.<br />";
}

if(!filter_var($site_paypal_email, FILTER_VALIDATE_EMAIL)) { 
$error .= "Invalid paypal email format. Valid: <i>admin@example.com</i><br />";
}

if(!filter_var($site_email, FILTER_VALIDATE_EMAIL)) { 
$error .= "Invalid site email format. Valid: <i>admin@example.com</i><br />";
}

if($site_theme != "default" && $site_theme != "cerulean" && $site_theme != "cosmo" && $site_theme != "cyborg" && $site_theme != "darkly" && $site_theme != "flatly" && $site_theme != "journal" && $site_theme != "lumen" && $site_theme != "paper" && $site_theme != "readable" && $site_theme != "sandstone" && $site_theme != "simplex" && $site_theme != "slate" && $site_theme != "spacelab" && $site_theme != "superhero" && $site_theme != "united" && $site_theme != "yeti") {
$error .= "This theme doesn't exist, please choose a theme from the options below.";
}


if(!$error) {
mysqli_query($conn, "UPDATE `config` SET 
`site_name`='".mysqli_real_escape_string($conn, $site_name)."',
`site_description`='".mysqli_real_escape_string($conn, $site_description)."',
`site_theme`='".mysqli_real_escape_string($conn, $site_theme)."',
`site_url`='".mysqli_real_escape_string($conn, $site_url)."',
`site_signup_bonus`='".mysqli_real_escape_string($conn, $site_signup_bonus)."',
`site_referral_bonus`='".mysqli_real_escape_string($conn, $site_referral_bonus)."',
`site_referral_point_bonus`='".mysqli_real_escape_string($conn, $site_referral_point_bonus)."',
`site_page_limit`='".mysqli_real_escape_string($conn, $site_page_limit)."',
`site_point_conversion`='".mysqli_real_escape_string($conn, $site_point_conversion)."',
`site_paypal_email`='".mysqli_real_escape_string($conn, $site_paypal_email)."',
`site_email`='".mysqli_real_escape_string($conn, $site_email)."',
`site_timezone`='".mysqli_real_escape_string($conn, $site_timezone)."', 
`site_currency`='".mysqli_real_escape_string($conn, $site_currency)."',
`site_reversal_limit`='".mysqli_real_escape_string($conn, $site_reversal_limit)."' 
WHERE `site_id`='1'") or die(mysqli_error($conn));
    $success = "Site settings successfully updated!";
}

}

?>

<div class="container">

<?php if(isset($error)) { ?><div class="alert alert-danger"><?php echo $error; ?></div><?php } ?>
<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<?php $row = mysqli_fetch_array($get_settings); ?>

<form class="form-horizontal" role="form" method="post">

<h1>Editing Site Settings</h1>

<div class="form-group">
<label class="control-label col-sm-2" for="site_name">Site Name</label>
<div class="col-sm-10">
<input class="form-control" name="site_name" id="site_name" type="text" value="<?php if(isset($row['site_name'])) { echo $row['site_name']; } else { echo $site_name; } ?>"/>
<span class="help-block">The name of your website.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="site_description">Site Description</label>
<div class="col-sm-10">
<input class="form-control" name="site_description" id="site_description" type="text" value="<?php if(isset($row['site_description'])) { echo $row['site_description']; } else { echo $site_description; }?>" required/>
<span class="help-block">Make sure your site's description is under 200 characters, and make it awesome for SEO.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="site_url">Site URL/Link</label>
<div class="col-sm-10">
<input class="form-control" name="site_url" id="site_url" type="text" value="<?php if(isset($row['site_url'])) { echo $row['site_url']; } else { echo $site_url; } ?>" required/>
<span class="help-block">The URL/Link of your website. Example: http://www.example.com/</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="site_paypal_email">Site Email</label>
<div class="col-sm-10">
<input class="form-control" name="site_email" id="site_email" type="text" value="<?php if(isset($row['site_email'])) { echo $row['site_paypal_email']; } else { echo $site_email; }?>" required/>
<span class="help-block">Set your site email.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="site_paypal_email">Site Paypal</label>
<div class="col-sm-10">
<input class="form-control" name="site_paypal_email" id="site_paypal_email" type="text" value="<?php if(isset($row['site_paypal_email'])) { echo $row['site_paypal_email']; } else { echo $site_paypal_email; }?>" required/>
<span class="help-block">Set your Paypal email to accept payments from on-site advertising.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="site_theme">Site Theme</label>
<div class="col-sm-10">
<div class="input-group">
<select name="site_theme" class="form-control">
<option value="default">Default</option>
<option value="cerulean">Cerulean</option>
<option value="cosmo">Cosmo</option>
<option value="cyborg">Cyborg</option>
<option value="darkly">Darkly</option>
<option value="flatly">Flatly</option>
<option value="journal">Journal</option>
<option value="lumen">Lumen</option>
<option value="paper">Paper</option>
<option value="readable">Readable</option>
<option value="sandstone">Sandstone</option>
<option value="simplex">Simplex</option>
<option value="slate">Slate</option>
<option value="spacelab">Spacelab</option>
<option value="superhero">Superhero</option>
<option value="united">United</option>
<option value="yeti">Yeti</option>
</select>
</div>
<span class="help-block">Theme you want to appear on the site.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="site_currency">Site Currency</label>
<div class="col-sm-10">
<input class="form-control" name="site_currency" id="site_currency" type="text" value="<?php if(isset($row['site_currency'])) { echo $row['site_currency']; } else { echo $site_currency; } ?>"/>
<span class="help-block">The currency of your site. Example: points, credits, tokens, diamonds, etc</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="site_affiliate_id">Site Signup Bonus</label>
<div class="col-sm-10">
<input class="form-control" name="site_signup_bonus" id="site_signup_bonus" type="text" value="<?php if(isset($row['site_signup_bonus'])) {echo $row['site_signup_bonus']; } else { echo $site_signup_bonus; } ?>" required/>
<span class="help-block">How many points do you want to give to newly registered users?</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="site_referral_bonus">Site Referral Bonus</label>
<div class="col-sm-10">
<input class="form-control" name="site_referral_bonus" id="site_referral_bonus" type="text" value="<?php if(isset($row['site_referral_bonus'])) { echo $row['site_referral_bonus']; } else { echo $site_referral_bonus; } ?>" required/>
<span class="help-block">How much percentage of referrals offers do you want to give the referee?</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="site_referral_point_bonus">Site Referral Point Bonus</label>
<div class="col-sm-10">
<input class="form-control" name="site_referral_point_bonus" id="site_referral_bonus" type="text" value="<?php if(isset($row['site_referral_point_bonus'])) { echo $row['site_referral_point_bonus']; } else { echo $site_referral_point_bonus; } ?>" required/>
<span class="help-block">How many points do you want to give a users that refer another user?</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="site_point_conversion">Site Point Conversion</label>
<div class="col-sm-10">
<input class="form-control" name="site_point_conversion" id="site_point_conversion" type="text" value="<?php if(isset($row['site_point_conversion'])) { echo $row['site_point_conversion']; } else { echo $site_point_conversion; } ?>" required/>
<span class="help-block">How many points equals $1.00? Example: 100 points = $1, or 1000 points = $1</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="site_reversal_limit">Site Reversal Limit</label>
<div class="col-sm-10">
<input class="form-control" name="site_reversal_limit" id="site_reversal_limit" type="text" value="<?php if(isset($row['site_reversal_limit'])) { echo $row['site_reversal_limit']; } else { echo $site_reversal_limit; } ?>" required/>
<span class="help-block">This feature will automatically ban accounts if they have a certain amount of offer reversals. This is a required security feature.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="site_page_limit">Site Page Limit</label>
<div class="col-sm-10">
<input class="form-control" name="site_page_limit" id="site_page_limit" type="text" value="<?php if(isset($row['site_page_limit'])) { echo $row['site_page_limit']; } else { echo $site_page_limit; }?>" required/>
<span class="help-block">How many records to display per page? Keep it under 100 for faster loading times, default is set to 50 automatically.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="site_theme">Site Timezone</label>
<div class="col-sm-10">
<div class="input-group">
<select name="site_timezone" class="form-control">
    <?php 
    $get_timezones = mysqli_query($conn, "SELECT * FROM `timezones` ORDER BY timezone ASC");
        $timezone = '';

    while($tzrow=mysqli_fetch_array($get_timezones)) {
    if($tzrow['timezone'] == $config['site_timezone']) {
        $selected = 'selected';
    } else {
        $selected = '';
    }
    ?>
<option value="<?php echo $tzrow['timezone']; ?>" <?php echo $selected; ?>><?php echo $tzrow['timezone']; ?></option>
<?php } ?>
</select>
</div>
<span class="help-block">Site timezone, either use your server's timezone or your own.<br /><strong>Current Timezone: </strong><?php echo $config['site_timezone'];?></span>
</div>
</div>


<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="edit_settings" type="submit" value="Edit Settings"/>
</div>
</div>
</form>

</div>

<?php include("footer.php"); ?>