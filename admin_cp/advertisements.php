<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include('header.php');

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

if(isset($_GET['delete_advertisement'])) {
$success = "Advertisement was deleted.";
}

?>

<div class="container">

<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<h1>Advertisements <a class="pull-right btn btn-success" href="<?php echo $config['base_url']; ?>admin_cp/add_advertisement.php">Add Advertisement</a></h1>

<p>This page list all your current advertisements.</p>

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Advertisements</h3>
  </div>

<table class="table table-condensed">
<tr>
<th>Ad Name</th>
<th>Ad Status</th>
<th>Ad Actions</th>
</tr>


<?php
$list_advertisements = mysqli_query($conn, "SELECT * FROM `advertisements` ORDER BY `ad_name` ASC") or die(mysqli_error($conn));
while($row = mysqli_fetch_array($list_advertisements)) {
?>
<tr>
<td><?php echo $row['ad_name']; ?></td>
<td><?php if($row['ad_active'] == 1) { echo "<span class=\"text-success\">Active</span>"; } else { echo "<span class=\"text-danger\">Disabled</span>"; } ?></td>
<td><a class="btn btn-xs btn-primary" href="<?php echo $config['base_url']; ?>admin_cp/advertisement_edit.php?id=<?php echo $row['ad_id']; ?>">Edit</a> 
<a class="btn btn-xs btn-primary" href="<?php echo $config['base_url']; ?>admin_cp/advertisement_short_code.php?id=<?php echo $row['ad_id']; ?>">Get Short Code</a></td>
</tr>
<?php } ?>
</table>


</div>

</div>

<?php include('footer.php'); ?>