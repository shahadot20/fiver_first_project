<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include('header.php');

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

switch($_GET['action'])
{
case 'delete':
x_delete_chat_log();
break;

case 'bansender':
x_bansender();
break;

default:
x_chat_logs();
break;
}

?>

<?php 
function x_chat_logs() {
global $conn, $user, $config;

$adjacents = 5;

    $query = mysqli_query($conn, "select COUNT(*) as num from `chat`") or die(mysqli_error($conn));
    $total_pages = mysqli_fetch_array($query, MYSQLI_ASSOC);
    $total_pages = $total_pages['num'];

    $limit = $config['site_page_limit'];                                //how many items to show per page

    $page = abs((int) $_GET['page']);

    if($page) 
        $start = ($page - 1) * $limit;          //first item to display on this page
    else
        $start = 0;                             //if no page var is given, set start to 
    /* Get data. */
    $result = mysqli_query($conn, "select * from `chat` ORDER BY id DESC LIMIT $start,$limit") or die(mysqli_error($conn));

    /* Setup page vars for display. */
    if ($page == 0) $page = 1;                  //if no page var is given, default to 1.
    $prev = $page - 1;                          //previous page is page - 1
    $next = $page + 1;                          //next page is page + 1
    $lastpage = ceil($total_pages/$limit);      //lastpage is = total pages / items per page, rounded up.
    $lpm1 = $lastpage - 1;                      //last page minus 1

    $pagination = "";
    if($lastpage > 1) {   
        $pagination .= "<ul class=\"pager\">";
        //previous button
        if ($page > 1) 
            $pagination.= "<li class=\"previous\"><a href=\"".$config['base_url']."admin_cp/chat_logs.php?page=$prev\">&laquo; previous</a></li>";
        else
            $pagination.= "<li class=\"previous disabled\"><a>&laquo; previous</a></li>"; 

        //next button
        if ($page < $lastpage) 
            $pagination.= "<li class=\"next\"><a href=\"".$config['base_url']."admin_cp/chat_logs.php?page=$next\">next &raquo; /a></li>";
        else
            $pagination.= "<li class=\"next disabled\"><a>next &raquo;</a></li>";
        $pagination.= "</ul>\n";       
    }
?>

<div class="container">

<h2 class="page-header">Chat Log History</h2>
<p>List of all the chat logs.</p>

<?php if(mysqli_num_rows($result) == 0) { ?>
<div class="alert alert-danger">There is currently no chat log history.</div>
<?php } else { ?>
<table class="table">
<tr>
<th>Username</th>
<th width="300">Message</th>
<th>Date</th>
<th>IP Address</th>
<th>Actions</th>
</tr>
<?php while($row = mysqli_fetch_assoc($result)) { ?>
<tr>
<td><?php echo $row['username']; ?></td>
<td><?php echo $row['message']; ?></td>
<td><?php echo $row['date_time']; ?></td>
<td><?php echo $row['ip_address']; ?></td>
<td><a class="btn btn-xs btn-danger" href="<?php echo $config['base_url']; ?>admin_cp/chat_logs.php?action=delete&id=<?php echo $row['id']; ?>">Delete</a> <a class="btn btn-xs btn-primary" href="<?php echo $config['base_url']; ?>admin_cp/chat_logs.php?action=bansender&username=<?php echo $row['username']; ?>">Ban Chat User</a></td>
</tr>
<?php } ?>
</table>
<?php } ?>

<?php echo $pagination; ?>

</div>
<?php } ?>


<?php function x_delete_chat_log() { 
global $conn, $user, $config;

$id = str_clean($_GET['id']);

if((isset($_GET['action']) == 'delete') && isset($_GET['id'])) {
mysqli_query($conn, "DELETE FROM `chat` WHERE `id`='".mysqli_real_escape_string($conn, $id)."'") or die(mysqli_error($conn));
}

?>
<div class="container">

<h2 class="page-header">Deleting Chat Log</h2>

<div class="alert alert-success">This chat log has been successfully deleted!</div>
<?php } ?>

<?php function x_bansender() { 
global $conn, $admin, $config;

$ban_username = str_clean($_GET['username']);

if((isset($_GET['action']) == 'bansender') && isset($_GET['username'])) {

$username = mysqli_real_escape_string($conn, $_GET['username']);
$get_bans_sql = mysqli_query($conn, "SELECT * FROM `chat_bans` WHERE `ban_username`='".$username."' LIMIT 1") or die(mysqli_error($conn));

if(mysqli_num_rows($get_bans_sql) == 0) {
mysqli_query($conn, "INSERT INTO `chat_bans`(ban_id,ban_username,ban_banner,ban_date_added) values(NULL, 
'".mysqli_real_escape_string($conn, $ban_username)."',
'".mysqli_real_escape_string($conn, $_SESSION['admin_username'])."',
NOW()
)") or die(mysqli_error($conn));
mysqli_query($conn, "DELETE FROM chat WHERE username='".mysqli_real_escape_string($conn, $ban_username)."' LIMIT 1") or die(mysqli_error($conn));
$message = '<div class="alert alert-success">This user has successfully been banned, and their chat logs have been removed!</div>';

} else {
$message = '<div class="alert alert-danger">This user is already chat banned!</div>';
}
}

?>
<div class="container">

<h2 class="page-header">Banning user from chat</h2>

<?php if(isset($message)) echo $message; ?>
<?php } ?>


<?php include_once("footer.php"); ?>