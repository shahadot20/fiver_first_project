<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['base_url']."admin_cp/login.php'</script>";
    exit;
}

$page_id = str_clean($_GET['page_id']);

$get_page = mysqli_query($conn, "SELECT * FROM `pages` WHERE `page_id`='".mysqli_real_escape_string($conn, $page_id)."' LIMIT 1") or die(mysqli_error($conn));

if(empty($page_id) || $page_id == "" || !$page_id) {
$error = "You never selected a page to edit.";
} else if(mysqli_num_rows($get_page) == 0) {
$error = "This page doesn't exist.";
} else { 

if(isset($_POST['edit_page'])) {

$page_name = str_clean($_POST['page_name']);
$page_seo_name = url_slug($page_name);
$page_description = str_clean($_POST['page_description']);
$page_active =  str_clean($_POST['page_active']);


$page_sql = mysqli_query($conn, "SELECT `page_name` FROM `pages` WHERE `page_name`='".mysqli_real_escape_string($conn, $page_name)."'") or die(mysqli_error($conn));
$page_exists = mysqli_num_rows($page_sql);

if(empty($page_name)) {
$error = "You left the page name blank.";
}

if(empty($page_description)) {
$error .= "You left the page description blank.";
}

if($page_active == "") {
$error .= "You need to select if page is active or not.";
}

if(!is_numeric($page_active)) {
$error .= "You need to enter 0(disabled) or 1(active).</li>";
}

if($page_active != "0" && $page_active != "1") {
$error .= "Is ".$page_name." active or disabled?";

}

if(!$error) {
mysqli_query($conn, "UPDATE `pages` SET 
`page_name`='".mysqli_real_escape_string($conn, $page_name)."',
`page_seo_name`='".mysqli_real_escape_string($conn, $page_seo_name)."',
`page_description`='".mysqli_real_escape_string($conn, $page_description)."',
`page_active`='".mysqli_real_escape_string($conn, $page_active)."'
 WHERE `page_id`='".mysqli_real_escape_string($conn, $page_id)."'") or die(mysqli_error($conn));
    $success = "Page <strong>".$page_name."</strong> has been successfully updated!";
}

}

if(isset($_POST['delete_page'])) {
mysqli_query($conn, "DELETE FROM `pages` WHERE `page_id`='".mysqli_real_escape_string($conn, $page_id)."'") or die(mysqli_error($conn));
$redirect = $config['base_url']."admin_cp/pages.php?delete_page=1";
echo "<script>document.location.href='".$redirect."'</script>";
exit;
}
}
?>

<div class="container">

<?php if(isset($error)) { ?><div class="alert alert-danger"><?php echo $error; ?></div><?php } ?>
<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>

<?php $row = mysqli_fetch_array($get_page); ?>

<form class="form-horizontal" role="form" method="post">

<h1 class="page-header">Editing Page: <?php echo $row['page_name']; ?></h1>

<div class="form-group">
<label class="control-label col-sm-2" for="page_name">Page Name</label>
<div class="col-sm-10">
<input class="form-control" name="page_name" id="page_name" type="text" value="<?php if(isset($row['page_name'])) { echo $row['page_name']; } else { echo $page_name; } ?>" required/>
<span class="help-block">Gotta have a page name.</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="page_description">Page Description</label>
<div class="col-sm-10">
<textarea rows="15" class="form-control" name="page_description" id="page_description" required/><?php if(isset($row['page_description'])) { echo html_entity_decode($row['page_description']); } else { echo html_entity_decode($page_description); } ?></textarea>
<span class="help-block">Edit the contents of your description (HTML is allowed.)</span>
</div>
</div>

<div class="form-group">
<label class="control-label col-sm-2" for="page_active">Page Active</label>
<div class="col-sm-10">
<select class="form-control" name="page_active" id="page_active">
<option value="0" <?php if($row['page_active'] == 0) { ?>selected<?php } ?>>Disabled</option>
<option value="1" <?php if($row['page_active'] == 1) { ?>selected<?php } ?>>Active</option>
</select>
<span class="help-block">This page is currently set to: <?php if($row['page_active'] == 1) { echo "<strong class=\"text text-success\">Active</strong>"; } else { echo "<strong class=\"text text-danger\"><i>Disabled</i></strong>"; } ?></span>
</div>
</div>

<div class="form-group">        
<div class="col-sm-offset-2 col-sm-10">
<input class="btn btn-success" name="edit_page" type="submit" value="Edit Page"/>
<input class="btn btn-danger pull-right" name="delete_page" type="submit" value="Delete Page"/>
</div>
</div>
</form>

</div>

<link rel="stylesheet" href="<?php echo $config['base_url']; ?>/admin_cp/assets/js/sceditor/minified/themes/square.min.css" />

    <script src="<?php echo $config['base_url']; ?>admin_cp/assets/js/sceditor/minified/sceditor.min.js.php"></script>
<script src="<?php echo $config['base_url']; ?>/admin_cp/assets/js/sceditor/minified/formats/xhtml.js"></script>
<script>
// Replace the textarea #example with SCEditor
var textarea = document.getElementById('page_description');
sceditor.create(textarea, {
	format: 'xhtml',
	style: '<?php echo $config['base_url']; ?>/admin_cp/assets/js/sceditor/minified/themes/content/default.min.css'
});
</script>
<?php include("footer.php"); ?>