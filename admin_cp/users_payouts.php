<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

include('header.php');

// redirect user to login to access this page
if(!isset($_SESSION['admin_username'])){
    echo "<script>document.location.href='".$config['site_url']."admin_cp/login.php'</script>";
    exit;
}


$paid_id = intval($_GET['id']);

	switch($_GET['action']) {
		case "refund": x_refund_user($paid_id); break;
		case "paid": x_paid_user($paid_id); break;
		default: x_view($paid_id); break;
	}
 

function x_view($paid_id) {
    global $conn, $config, $paid_id;
    
$adjacents = 5;

    $query = mysqli_query($conn, "select COUNT(*) as num from `redemption_history` WHERE `redemption_paid`='0'") or die(mysqli_error($conn));
    $total_pages = mysqli_fetch_array($query, MYSQLI_ASSOC);
    $total_pages = $total_pages['num'];

    $limit = $config['site_page_limit'];                                //how many items to show per page

    $page = abs((int) $_GET['page']);

    if($page) 
        $start = ($page - 1) * $limit;          //first item to display on this page
    else
        $start = 0;                             //if no page var is given, set start to 
    /* Get data. */
    $result = mysqli_query($conn, "select * from `redemption_history` WHERE `redemption_paid`='0' ORDER BY redemption_date_added DESC LIMIT $start,$limit") or die(mysqli_error($conn));

    /* Setup page vars for display. */
    if ($page == 0) $page = 1;                  //if no page var is given, default to 1.
    $prev = $page - 1;                          //previous page is page - 1
    $next = $page + 1;                          //next page is page + 1
    $lastpage = ceil($total_pages/$limit);      //lastpage is = total pages / items per page, rounded up.
    $lpm1 = $lastpage - 1;                      //last page minus 1

    $pagination = "";
    if($lastpage > 1) {   
        $pagination .= "<ul class=\"pager\">";
        //previous button
        if ($page > 1) 
            $pagination.= "<li class=\"previous\"><a href=\"".$config['site_url']."admin_cp/users_payouts.php?page=$prev\">&laquo; previous</a></li>";
        else
            $pagination.= "<li class=\"previous disabled\"><a>&laquo; previous</a></li>"; 

        //next button
        if ($page < $lastpage) 
            $pagination.= "<li class=\"next\"><a href=\"".$config['site_url']."admin_cp/users_payouts.php?page=$next\">next &raquo;</a></li>";
        else
            $pagination.= "<li class=\"next disabled\"><a>next &raquo;</a></li>";
        $pagination.= "</ul>\n";       
    }

$paid_id = intval(str_clean($_GET['id']));
$find_redemptions = mysqli_query($conn, "SELECT redemption_id,redemption_paid FROM redemption_history WHERE `redemption_id`='".mysqli_real_escape_string($conn, $paid_id)."' AND `redemption_paid`='0' LIMIT 1") or die(mysqli_error($conn));


if(isset($_GET['action']) && $_GET['action'] == "pay" && isset($paid_id)) {

if(mysqli_num_rows($find_redemptions) == 0) {
$error = "<div class=\"alert alert-danger\">This redemption doesn't exist or was already paid.</div>";
} else {
mysqli_query($conn, "UPDATE `redemption_history` SET `redemption_paid`='1',`redemption_date_paid`=NOW() WHERE `redemption_id`='".mysqli_real_escape_string($conn, $paid_id)."' LIMIT 1") or die(mysqli_error($conn));
$success = "<div class=\"alert alert-success\">Redemption has been marked as \"paid\"! Thank you!</div>";
}
}

?>

<div class="container">

<h2 class="page-header">Users Payouts</h2>

<?php if(isset($error)) { echo $error; } elseif(isset($success)) { echo $success; } ?>

<p>List of all user's current payouts.</p>

<?php if(mysqli_num_rows($result) == 0) { ?>
<div class="alert alert-danger">There are currently no user payouts.</div>
<?php } else { ?>
<table class="table">
<tr>
<th>User ID</th>
<th>Payment Email</th>
<th>Notes</th>
<th>Provider</th>
<th>Points</th>
<th>Cash</th>
<th>Date Added</th>
<th>Action</th>
</tr>
<?php while($row = mysqli_fetch_assoc($result)) {


if($row['redemption_fee'] > 0) {
$total_points2 = $row['redemption_points'] * "0.".$row['redemption_fee'];
$total_points = $row['redemption_points'] - $total_points2;
//$total_fee = $row['redemption_points'] * "0.".$row['redemption_fee'];
$total_fee = ($total_points / 100) * $row['redemption_fee'];
} else {
$total_points = $row['redemption_points'];
$total_fee = 0;
}

$cash = convert($total_points);
if($row['redemption_paid'] == 1) {
$paid = "Paid";
} else {
$paid = "<a class=\"btn btn-xs btn-primary\" href=\"".$config['site_url']."admin_cp/users_payouts.php?action=paid&id=".$row['redemption_id']."\">Mark as Paid</a>";
}

if(!empty($row['redemption_email'])) {
    $email = $row['redemption_email'];
} else {
    $email = 'N/A';
}

if(!empty($row['redemption_notes'])) {
    $notes = $row['redemption_notes'];
} else {
    $notes = 'N/A';
}

$refund = "<a href=\"".$config['site_url']."admin_cp/users_payouts.php?action=refund&id=".$row['redemption_id']."\" onclick=\"return confirm('Are you sure you want to refund this request? The redemption amount will go back into the user\'s account');\" class=\"btn btn-xs btn-warning\">Refund</a>";
?>
<tr>
<td><?php echo $row['user_id']; ?></td>
<td><?php echo $email; ?></td>
<td><?php echo $notes; ?></td>
<td><?php echo $row['redemption_provider']; ?></td>
<td><?php echo $total_points; ?></td>
<td><?php echo "$".$cash; ?></td>
<td><?php echo $row['redemption_date_added']; ?></td>
<td><?php echo $paid ?> <?php echo $refund; ?></td>
</tr>
<?php } ?>
</table>
<?php } ?>

<?php echo $pagination; ?>

<?php } ?>

<?php
function x_refund_user($paid_id) {
    global $conn, $config, $paid_id;
    
    echo "<div class=\"container\">";
    
    $result = mysqli_query($conn, "SELECT * FROM redemption_history WHERE `redemption_id`='".mysqli_real_escape_string($conn, $paid_id)."' AND `redemption_paid`='0' LIMIT 1") or die(mysqli_error($conn));
$row = mysqli_fetch_array($result);
    
if(mysqli_num_rows($result) == 0) {
echo "<div class=\"alert alert-danger\">This redemption doesn't exist or was already refunded.</div>";
} else {

$redemption_points = $row['redemption_points'];
    
if($row['redemption_fee'] > 0) {
$total_points2 = sprintf($redemption_points * (($row['provider_fee']) / 100));	
$total_points = $redemption_points + $total_points2;
} else {
$total_points = $redemption_points;
}

mysqli_query($conn, "UPDATE users SET points=points+'".mysqli_real_escape_string($conn, $total_points)."' WHERE user_id='".mysqli_real_escape_string($conn, $row['user_id'])."' LIMIT 1") or die(mysqli_error($conn));
mysqli_query($conn, "DELETE FROM `redemption_history` WHERE `redemption_id`='".mysqli_real_escape_string($conn, $paid_id)."' LIMIT 1") or die(mysqli_error($conn));
echo "<div class=\"alert alert-success\">Redemption amount was refunded back into the user's account.</div>";
}
}


function x_paid_user($paid_id) {
    global $conn, $config, $paid_id;
    
    echo "<div class=\"container\">";

    
$result = mysqli_query($conn, "SELECT * FROM redemption_history WHERE `redemption_id`='".mysqli_real_escape_string($conn, $paid_id)."' AND `redemption_paid`='0' LIMIT 1") or die(mysqli_error($conn));

if(mysqli_num_rows($result) == 0 || !mysqli_num_rows($result)) {
echo "<div class=\"alert alert-danger\">This redemption doesn't exist or was already paid.</div>";
} else {
mysqli_query($conn, "UPDATE `redemption_history` SET `redemption_paid`='1',`redemption_date_paid`=NOW() WHERE `redemption_id`='".mysqli_real_escape_string($conn, $paid_id)."' LIMIT 1") or die(mysqli_error($conn));
echo "<div class=\"alert alert-success\">Redemption has been marked as <strong>PAID</strong>!</div>";
}
}
?>
</div>


<?php include_once("footer.php"); ?>