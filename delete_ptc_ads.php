<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include_once("header.php");

// redirect user to login to access this page
if(!isset($_SESSION['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}

$get_id = str_clean($_GET['ptc_id']);

$get_ad = mysqli_query($conn, "SELECT * FROM `ptc_ads` WHERE `ptc_ad_id`='".mysqli_real_escape_string($conn, $get_id)."' AND ptc_ad_userid='".$user['user_id']."' LIMIT 1") or die(mysqli_error($conn));

if(empty($get_id) || $get_id == "" || !$get_id) {
$error = "You never selected a PTC ad to delete.";
} else {

mysqli_query($conn, "DELETE FROM `ptc_ads` WHERE `ptc_ad_id`='".mysqli_real_escape_string($conn, $get_id)."' AND ptc_ad_userid='".$user['user_id']."'") or die(mysqli_error($conn));

$success = "This PTC ad has been  successfully deleted.";
}

?>

<div class="col-lg-9">

<h2 class="page-header">Deleting PTC Ad</h2>

<?php if(mysqli_num_rows($get_ad) == 0) { ?>
<div class="alert alert-danger">This PTC ad doesn't exist.</div>
<?php } else { ?>

<?php if(isset($success)) { ?><div class="alert alert-success"><?php echo $success; ?></div><?php } ?>


<?php } ?>

</div>

<?php include("footer.php"); ?>