<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/
session_start();


include('header.php');

// redirect user to login to access this page
if(isset($_SESSION['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}

if(isset($_POST['user_login'])) {

$username = str_clean($_POST['username']);
$password = str_clean(md5($_POST['password']));
$get_ban_ip = str_clean(get_user_ip());

$check_login = mysqli_query($conn, "SELECT username,password,banned,banned_reason FROM `users` WHERE `username`='".mysqli_real_escape_string($conn, $username)."' AND `password`='".mysqli_real_escape_string($conn, $password)."' LIMIT 1") or die(mysqli_error($conn));
$login = mysqli_fetch_array($check_login);
$check_exists = mysqli_num_rows($check_login);

$check_ip_ban = mysqli_query($conn, "SELECT ban_ip_address,ban_reason FROM `banned_ips` WHERE `ban_ip_address`='".mysqli_real_escape_string($conn, $get_ban_ip)."'") or die(mysqli_error($conn));
$ipban = mysqli_fetch_array($check_ip_ban);
$check_ip_exists = mysqli_num_rows($check_ip_ban);

if($login['banned'] > 0) {
$message = "<div class=\"alert alert-danger\">Your account is banned. <strong>Ban Reason: </strong>".$login['banned_reason']."</div>";
} else if($check_exists == 0) {
$message = "<div class=\"alert alert-danger\">No account exists with that username/password combination.</div>";
} else if($check_ip_exists > 0) { 
$message = "<div class=\"alert alert-danger\">This IP address is banned. <strong>Ban Reason: </strong>".$ipban['ban_reason'].".</div>";
} else if(empty($_POST['username']) || $_POST['username'] == "") {
$message = "<div class=\"alert alert-danger\">Username is empty, try again.</div>";
} else if(empty($_POST['password']) || $_POST['password'] == "") {
$message = "<div class=\"alert alert-danger\">Password is empty, try again.</div>";
} else if($check_exists == 1) {
mysqli_query($conn, "UPDATE `users` SET `online`=NOW() WHERE `username`='".mysqli_real_escape_string($conn, $username)."' LIMIT 1") or die(mysqli_error($conn));
$_SESSION['username'] = $username;
echo "<script>document.location.href='".$config['base_url']."dashboard.php'</script>";
} else {
$message = "<div class=\"alert alert-danger\">Invalid login details. Try again.</div>";
}
}

?>

<div class="container-fluid">

<div class="row row-centered">
<div class="col-lg-5 col-centered">
      <form class="form-signin" role="form" method="post" action="">
        <h2 class="form-signin-heading text-center">Login</h2>

<?php if(isset($message)) echo $message; ?>

<div class="form-group">
        <label for="inputEmail" class="sr-only">Username</label>
        <input type="text" name="username" id="inputUsername" class="form-control" placeholder="Username" required autofocus>
        </div>
        
        <div class="form-group">
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="password" required>
        </div>
<div class="form-group">
        <button class="btn btn-lg btn-primary btn-block" name="user_login" type="submit">Sign in</button>
        <a class="btn btn-sm btn-primary btn-block" href="<?php echo $config['base_url']; ?>forgot_password.php">Forgot password?</a>
        <a class="btn btn-sm btn-primary btn-block" href="<?php echo $config['base_url']; ?>forgot_username.php">Forgot username?</a>
        <hr class="sep"></hr>
        <a class="btn btn-lg btn-success btn-block" href="<?php echo $config['base_url']; ?>signup.php">Signup</a>
        </div>
      </form>

    </div>
    </div>
    </div> <!-- /container -->


<?php include('footer.php'); ?>