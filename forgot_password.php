<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/
session_start();


include('header.php');

if(isset($_POST['forgot_password'])) {

$email = str_clean($_POST['email']);
$username = str_clean($_POST['username']);
$reset_token = coupon_code_generator(15);


$check_email = mysqli_query($conn, "SELECT * FROM `users` WHERE `email`='".mysqli_real_escape_string($conn, $email)."' AND `username`='".mysqli_real_escape_string($conn, $username)."'") or die(mysqli_error($conn));
$login = mysqli_fetch_array($check_email);
$check_exists = mysqli_num_rows($check_email);

if($login['banned'] > 0) {
$message = "<div class=\"alert alert-danger\">Your account is banned. <strong>Ban Reason: </strong>".$login['banned_reason']."</div>";
} else if($check_exists == 0) {
$message = "<div class=\"alert alert-danger\">No account exists with that username or email combination.</div>";
} else if(empty($email) || $email == "") {
$message = "<div class=\"alert alert-danger\">Email is empty, try again.</div>";
} else if(empty($username) || $username == "") {
$message = "<div class=\"alert alert-danger\">Username is empty, try again.</div>";
} else if($check_exists == 1) {

$check_token = mysqli_query($conn, "SELECT * FROM `users_reset_tokens` WHERE `token_email`='".mysqli_real_escape_string($conn, $email)."'") or die(mysqli_error($conn));

if(mysqli_num_rows($check_token) == 0) {

		mail($login['email'],"{$config['site_name']} - Reset your password","
Hello {$login['username']},

You have recently requested to reset your password on {$config['site_name']}, please follow this link to do so:

{$config['site_url']}reset_password/{$reset_token}/{$login['user_id']}/{$login['email']}


Best Regards,
{$config['site_name']}
{$config['site_url']}
","From: {$config['site_name']} <{$config['site_email']}>");

$insert_sql = "INSERT INTO `users_reset_tokens` (token_id, token_userid, token_code, token_email, token_date_added) VALUES(NULL, '".mysqli_real_escape_string($conn, $login['user_id'])."', '".mysqli_real_escape_string($conn, $reset_token)."', '".mysqli_real_escape_string($conn, $login['email'])."', NOW())";
$result = mysqli_query($conn, $insert_sql) or die(mysqli_error($conn));

$message = "<div class=\"alert alert-success\">We have sent an email containing your reset password details. Please check your spam/bulk folders if you can not find our emails.</div>";
} else {

$check_email = mysqli_query($conn, "SELECT * FROM `users` WHERE `email`='".mysqli_real_escape_string($conn, $email)."' AND `username`='".mysqli_real_escape_string($conn, $username)."'") or die(mysqli_error($conn));
$login = mysqli_fetch_array($check_email);

$check_token = mysqli_query($conn, "SELECT * FROM `users_reset_tokens` WHERE `token_email`='".mysqli_real_escape_string($conn, $email)."' AND `token_userid`='".mysqli_real_escape_string($conn, $login['user_id'])."'") or die(mysqli_error($conn));
$token = mysqli_fetch_array($check_token);


		mail($login['email'],"{$config['site_name']} - Reset your password","
Hello {$login['username']},

You have recently requested to reset your password on {$config['site_name']}, please follow this link to do so:

{$config['site_url']}reset_password/{$token['token_code']}/{$login['user_id']}/{$login['email']}


Best Regards,
{$config['site_name']}
{$config['site_url']}
","From: {$config['site_name']} <{$config['site_email']}>");

$message = "<div class=\"alert alert-success\">We have sent an email containing your reset password details. Please check your spam/bulk folders if you can not find our emails.</div>";

}

//
} else {
$message = "<div class=\"alert alert-danger\">Invalid email. Try again.</div>";
}
}

?>

<div class="container">

<div class="row row-centered">
<div class="col-lg-5 col-centered">
      <form class="form-signin" role="form" method="post" action="">
        <h2 class="form-signin-heading text-center">Forgot Password</h2>
        
        <div class="alert alert-info">Enter your email and username to reset your password.</div>

<?php if(isset($message)) echo $message; ?>
<div class="form-group">
        <label for="inputEmail" class="sr-only">Email</label>
        <input type="text" name="email" id="inputEmail" class="form-control" placeholder="you@domain.com" required autofocus>
        </div>
        
        <div class="form-group">
        <label for="inputUsername" class="sr-only">Username</label>
        <input type="text" name="username" id="inputUsername" class="form-control" placeholder="Username" required autofocus>
        </div>
        
        
<div class="form-group">
        <button class="btn btn-lg btn-primary btn-block" name="forgot_password" type="submit">Reset Password</button>
        <hr class="sep"></hr>
        <a class="btn btn-lg btn-success btn-block" href="<?php echo $config['base_url']; ?>login.php">Login</a>
        </div>
      </form>

    </div>
    </div>
    </div> <!-- /container -->


<?php include('footer.php'); ?>