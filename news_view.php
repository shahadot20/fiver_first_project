<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include_once("header.php");

// redirect user to login to access this page
if(!isset($user['username'])){
    echo "<script>document.location.href='".$config['base_url']."login.php'</script>";
    exit;
}

$get_id = intval(str_clean($_GET['id']));

$news_sql = mysqli_query($conn, "SELECT * FROM `news` WHERE `news_id`='".mysqli_real_escape_string($conn, $get_id)."' AND `news_active`='1' LIMIT 1") or die(mysqli_error($conn));
$row = mysqli_fetch_array($news_sql, MYSQLI_ASSOC);

$check_news = mysqli_query($conn, "SELECT * FROM `news` WHERE `news_id`='".mysqli_real_escape_string($conn, $get_id)."' AND `news_active`='1' LIMIT 1") or die(mysqli_error($conn));
$news_exists = mysqli_num_rows($check_news);

?>

<div class="col-lg-9">

<?php if($news_exists == 0) { ?>
<h2 class="page-header">Error!</h2>
<div class="alert alert-danger">This news article doesn't exist.</div>
<?php } else { ?>
<h2 class="page-header"><?php echo $row['news_title']; ?></h2>
<p class="pull-right help-block"><?php echo $row['news_date_added']; ?></p>
<p><?php echo html_entity_decode($row['news_description']); ?></p>

<?php } ?>

</div>

<?php include_once("footer.php"); ?>