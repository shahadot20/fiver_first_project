s<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/
session_start();


include('header.php');

$code = str_clean($_GET['token']);
$user_id = str_clean($_GET['user_id']);
$email = str_clean($_GET['email']);

$check_token = mysqli_query($conn, "SELECT * FROM `users_reset_tokens` WHERE `token_email`='".mysqli_real_escape_string($conn, $email)."' AND `token_userid`='".mysqli_real_escape_string($conn, $user_id)."' AND `token_code`='".mysqli_real_escape_string($conn, $code)."' LIMIT 1") or die(mysqli_error($conn));
$check_exists = mysqli_num_rows($check_token);

if($check_exists == 0 || !$check_exists) {
print "<div class=\"alert alert-dismissable alert-danger\">
  <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
  <strong>Error..</strong> Invalid password reset link. Please try again.
</div>
";
} else {

if(isset($_POST['reset_password'])) {

$code = str_clean($_GET['token']);
$user_id = str_clean($_GET['user_id']);
$email = str_clean($_GET['email']);

$password = str_clean($_POST['password']);
$password2 = str_clean($_POST['password2']);


$check_token = mysqli_query($conn, "SELECT * FROM `users_reset_tokens` WHERE `token_email`='".mysqli_real_escape_string($conn, $email)."' AND `token_userid`='".mysqli_real_escape_string($conn, $user_id)."' AND `token_code`='".mysqli_real_escape_string($conn, $code)."' LIMIT 1") or die(mysqli_error($conn));
$login = mysqli_fetch_array($check_token);
$check_exists = mysqli_num_rows($check_token);

if($check_exists == 0) {
$message = "<div class=\"alert alert-danger\">This password reset request doesn't exist. Please try again.</div>";
} else if(empty($email) || $email == "") {
$message = "<div class=\"alert alert-danger\">Email is empty, try again.</div>";
} else if(empty($user_id) || $user_id == "") {
$message = "<div class=\"alert alert-danger\">User ID is empty, try again.</div>";
} else if(empty($code) || $code == "") {
$message = "<div class=\"alert alert-danger\">Token code is empty, try again.</div>";
} else if(empty($password) || $password == "") {
$message = "<div class=\"alert alert-danger\">Password is empty, try again.</div>";
} else if(empty($password2) || $password2 == "") {
$message = "<div class=\"alert alert-danger\">Confirm password is empty, try again.</div>";
} else if($password != $password2) {
$message = "<div class=\"alert alert-danger\">Passwords do not match, try again.</div>";
} else if($check_exists == 1) {

$password_md5 = md5($password);

mysqli_query($conn, "UPDATE `users` SET `password`='".mysqli_real_escape_string($conn, $password_md5)."'WHERE `email`='".mysqli_real_escape_string($conn, $email)."' AND `user_id`='".mysqli_real_escape_string($conn, $user_id)."' LIMIT 1") or die(mysqli_error($conn));

mysqli_query($conn, "DELETE FROM `users_reset_tokens` WHERE `token_code`='".mysqli_real_escape_string($conn, $code)."' AND `token_userid`='".mysqli_real_escape_string($conn, $user_id)."' AND `token_email`='".mysqli_real_escape_string($conn, $email)."'") or die(mysqli_error($conn));

$message = "<div class=\"alert alert-success\">Your account's password has successfully been changed. You may now log into your account.</div>";

}
} 

?>

<div class="container">

<div class="row row-centered">
<div class="col-lg-5 col-centered">
      <form class="form-signin" role="form" method="post" action="">
        <h2 class="form-signin-heading text-center">Resetting Password</h2>

<?php if(isset($message)) echo $message; ?>

<div class="form-group">
        <label for="inputEmail" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Enter password" required autofocus>
        </div>
        
        <div class="form-group">
        <label for="inputUsername" class="sr-only">Confirm Password</label>
        <input type="password" name="password2" id="inputPassword2" class="form-control" placeholder="Confirm password" required autofocus>
        </div>
        
        
<div class="form-group">
        <button class="btn btn-lg btn-primary btn-block" name="reset_password" type="submit">Reset Password</button>
        <hr class="sep"></hr>
        <a class="btn btn-lg btn-success btn-block" href="<?php echo $config['base_url']; ?>login.php">Login</a>
        </div>
      </form>

    </div>
    </div>
    </div> <!-- /container -->

<?php } ?>

<?php include('footer.php'); ?>