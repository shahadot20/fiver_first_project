<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/
session_start();


include('header.php');

if(isset($_POST['forgot_username'])) {

$email = str_clean($_POST['email']);

$check_email = mysqli_query($conn, "SELECT * FROM `users` WHERE `email`='".mysqli_real_escape_string($conn, $email)."' LIMIT 1") or die(mysqli_error($conn));
$login = mysqli_fetch_array($check_email);
$check_exists = mysqli_num_rows($check_email);

if($login['banned'] > 0) {
$message = "<div class=\"alert alert-danger\">Your account is banned. <strong>Ban Reason: </strong>".$login['banned_reason']."</div>";
} else if($check_exists == 0) {
$message = "<div class=\"alert alert-danger\">No account exists with that email.</div>";
} else if(empty($email) || $email == "") {
$message = "<div class=\"alert alert-danger\">Username is empty, try again.</div>";
} else if($check_exists == 1) {

		mail($login['email'],"{$config['site_name']} - Username Request","
Hello {$login['username']},

You have recently requested your username on {$config['site_name']}, here are those details:

Username: {$login['username']}

You can log into your account via this URL:
{$config['site_url']}login

If you have also forgotten your password, please reset it here:
{$config['site_url']}forgot_password


Best Regards,
{$config['site_name']}
{$config['site_url']}
","From: {$config['site_name']} <{$config['site_email']}>");

$message = "<div class=\"alert alert-success\">We have sent an email containing your username. Please check your spam/bulk folders if you can not find our emails.</div>";
} else {
$message = "<div class=\"alert alert-danger\">Invalid email. Try again.</div>";
}
}

?>

<div class="container">

<div class="row row-centered">
<div class="col-lg-5 col-centered">
      <form class="form-signin" role="form" method="post" action="">
        <h2 class="form-signin-heading text-center">Forgot Username</h2>
        
        <div class="alert alert-info">Enter your email to retrieve your username.</div>

<?php if(isset($message)) echo $message; ?>
<div class="form-group">
        <label for="inputEmail" class="sr-only">Email</label>
        <input type="text" name="email" id="inputEmail" class="form-control" placeholder="you@domain.com" required autofocus>
        </div>
        
<div class="form-group">
        <button class="btn btn-lg btn-primary btn-block" name="forgot_username" type="submit">Send Username</button>
        <hr class="sep"></hr>
        <a class="btn btn-lg btn-success btn-block" href="<?php echo $config['base_url']; ?>login.php">Login</a>
        </div>
      </form>

    </div>
    </div>
    </div> <!-- /container -->


<?php include('footer.php'); ?>