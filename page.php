<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include_once("header.php");

$get_page = str_clean($_GET['page_seo_title']);

$page_sql = mysqli_query($conn, "SELECT * FROM `pages` WHERE `page_seo_name`='".mysqli_real_escape_string($conn, $get_page)."'") or die(mysqli_error($conn));
$row = mysqli_fetch_array($page_sql, MYSQLI_ASSOC);

$check_page = mysqli_query($conn, "SELECT * FROM `pages` WHERE `page_seo_name`='".mysqli_real_escape_string($conn, $get_page)."'") or die(mysqli_error($conn));
$page_exists = mysqli_num_rows($check_page);

?>

<?php if(!isset($user['username'])) { ?>
<div class="col-lg-12">
<?php } else { ?>
<div class="col-lg-9">
<?php } ?>

<?php if($page_exists == 0) { ?>
<h2 class="page-header">Error!</h2>
<div class="alert alert-danger">This page doesn't exist.</div>
<?php } else { ?>
<h2 class="page-header"><?php echo $row['page_name']; ?></h2>

<p><?php echo html_entity_decode($row['page_description']); ?></p>

<?php } ?>

</div>

<?php include_once("footer.php"); ?>