<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include('../includes/config.php');
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $config['site_name']; ?> - Chat</title>
<link href="<?php echo $config['site_url']; ?>assets/css/bootstrap.<?php if(empty($user['site_theme']) || $user['site_theme'] == "") { echo $config['site_theme']; } else { echo $user['site_theme']; } ?>.min.css" rel="stylesheet">
<style type="text/css">
<!--
body {
margin:0;
padding:0;
}
.shout_box {
	background: #FFFFFF;
	width: 100%;
	overflow: hidden;
	bottom: 0;
}

.shout_box .message_box {
	background: #FFFFFF;
	height: 350px;
	overflow:auto;
	border: 1px solid #CCC;
}
.shout_msg{
	margin-bottom: 10px;
	display: block;
	border-bottom: 1px solid #F3F3F3;
	padding: 0px 5px 5px 5px;
	color:#7C7C7C;
}
.message_box:last-child {
	border-bottom:none;
}
time{
	font: 11px 'lucida grande', tahoma, verdana, arial, sans-serif;
	font-weight: normal;
	float:right;
	color: #D5D5D5;
}
.shout_msg .username{
	margin-bottom: 10px;
	margin-top: 10px;
}
.user_info input {
	width: 100%;
	height: 25px;
	border: 1px solid #CCC;
	border-top: none;
	padding: 3px 0px 0px 3px;
}
.shout_msg .username{
	font-weight: bold;
	display: block;
}
-->
</style>

<script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {

	// load messages every 5000 milliseconds from server.
	load_data = {'fetch':1};
	window.setInterval(function(){
	    
	 $.post('ajax_chat.php', load_data,  function(data) {
		$('.message_box').html(data);
		var scrolltoh = $('.message_box')[0].scrollHeight;
		$('.message_box').scrollTop(scrolltoh);
		$('#shout_message').attr('','');

});
	}, 5000);
	
	//method to trigger when user hits enter key
	$("#shout_message").keypress(function(evt) {
	    
		if(evt.which == 13) {
				var iusername = $('#shout_username').val();
				var imessage = $('#shout_message').val();
				post_data = {'username':iusername, 'message':imessage};

			 	if(imessage == '' || imessage == ' ') {
					     alert('You need to add a message.');
					 } else if($('.shout_message').val() == ''){
					     alert('You need to add a message.');
					     } else if(imessage.length > 200) {
					     alert('You can only post maximum 200 characters.');
					     } else if(imessage.length < 2) {
					     alert('Try saying a little more..');
					     } else { 
				//send data to "shout.php" using jQuery $.post()
				
				
				$.post('ajax_chat.php', post_data, function(data) {

					//append data into messagebox with jQuery fade effect!
					$(data).hide().appendTo('.message_box').fadeIn();
	
					//keep scrolled to bottom of chat!
					var scrolltoh = $('.message_box')[0].scrollHeight;
					$('.message_box').scrollTop(scrolltoh);
					
					//reset value of message box
					$('#shout_message').val('');
					
				}).fail(function(err) { 
				
				//alert HTTP server error
				alert(err.statusText); 
				});
			}
		}	

	});
});

</script>
</head>

<body>
<div class="shout_box">
  <div class="message_box">
    </div>
    <div class="user_info">
   <input name="shout_message" id="shout_message" type="text" placeholder="Type Message Hit Enter" maxlength="200" /> 
    </div>
</div>
</body>
</html>
