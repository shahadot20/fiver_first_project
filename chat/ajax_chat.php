<?php
/**************************************************************************************************
| GPT Reward PHP Script
| https://www.scriptbucks.com
| support@scriptbucks.com
|
|**************************************************************************************************
|
| By using this software you agree that you have read and acknowledged our End-User License 
| Agreement available at https://www.scriptbucks.com/eula and to be bound by it.
|
| Copyright (c) 2017 ScriptBucks.com. All rights reserved.
|**************************************************************************************************/

session_start();

include('../includes/config.php');

if(isset($user['username'])){

$get_chatban_users = mysqli_query($conn, "SELECT * FROM chat_bans WHERE ban_username='{$user['username']}' LIMIT 1") or die(mysqli_error($conn));
if(mysqli_num_rows($get_chatban_users) == 0) { 
if($_POST)
{
	
	//check if its an ajax request, exit if not
    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
        die();
    } 
	
	if(isset($_POST["message"]) &&  strlen($_POST["message"])>0)
	{
		//sanitize user name and message received from chat box
		//You can replace username with registerd username, if only registered users are allowed.
		$username = filter_var(trim($user['username']),FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
		$message = filter_var(trim($_POST["message"]),FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
		$user_ip = filter_var(trim($_SERVER['REMOTE_ADDR']),FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);


		//insert new message in db
		if(mysqli_query($conn, "INSERT INTO chat(id, username, message, ip_address) value(NULL, '".mysqli_real_escape_string($conn, $username)."','".mysqli_real_escape_string($conn, $message)."','".mysqli_real_escape_string($conn, $user_ip)."')"))
		{
			$msg_time = date('h:i A M d',time()); // current time
			
			echo '<div class="shout_msg"><time>'.$msg_time.'</time><span class="username">'.$username.'</span><span class="message">'.$message.'</span></div>';
		}
		
		// delete all records except last 10, if you don't want to grow your db size!
		mysqli_query($conn,"DELETE FROM chat WHERE id NOT IN (SELECT * FROM (SELECT id FROM chat ORDER BY id DESC LIMIT 0, 10) as sb)");
	}
	elseif($_POST["fetch"]==1)
	{
		$results = mysqli_query($conn,"SELECT username, message, date_time FROM (select * from chat ORDER BY id DESC LIMIT 10) chat ORDER BY chat.id ASC");
		while($row = mysqli_fetch_array($results))
		{
			$msg_time = date('h:i A M d',strtotime($row["date_time"]));
			//message posted time
			
			if($chatuser['chat_role'] == 1) {
			    $user_role = '<span class="label label-danger label-role">mod</span>';
			} else if($chatuser['chat_role'] == 2) {
			    $user_role = '<span class="label label-danger label-role">admin</span>';
			}
			
			echo '<div class="shout_msg"><time>'.$msg_time.'</time><span class="username">'.$row["username"].'</span> <span class="message">'.$row["message"].'</span></div>';
		}
	}
	else
	{
		header('HTTP/1.1 500');
    	exit();
	}
}
}
}